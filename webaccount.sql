-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 23, 2020 at 11:26 PM
-- Server version: 5.6.49-cll-lve
-- PHP Version: 7.3.6
-- Kannna  
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webaccount`
--

-- --------------------------------------------------------

--
-- Table structure for table `fin_accountdefaults`
--

CREATE TABLE `fin_accountdefaults` (
  `adid` int(11) NOT NULL,
  `agid` int(11) NOT NULL,
  `aiid` int(11) NOT NULL,
  `systemaccount` tinyint(4) NOT NULL,
  `edit` tinyint(4) NOT NULL,
  `delete` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_accountdefaults`
--

INSERT INTO `fin_accountdefaults` (`adid`, `agid`, `aiid`, `systemaccount`, `edit`, `delete`) VALUES
(1, 1, 364, 1, 0, 0),
(2, 1, 366, 1, 0, 0),
(3, 1, 363, 1, 0, 0),
(4, 2, 362, 1, 0, 0),
(5, 2, 361, 1, 0, 0),
(6, 3, 360, 1, 0, 0),
(7, 3, 365, 0, 0, 1),
(8, 4, 335, 0, 0, 1),
(18, 4, 359, 1, 0, 0),
(20, 4, 346, 0, 0, 1),
(22, 4, 351, 0, 0, 1),
(24, 4, 162, 0, 0, 1),
(27, 4, 355, 0, 0, 1),
(30, 5, 91, 0, 1, 0),
(31, 5, 358, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fin_accountgroupcontents`
--

CREATE TABLE `fin_accountgroupcontents` (
  `acid` int(11) NOT NULL,
  `agid` int(11) NOT NULL COMMENT 'account group id',
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `lan` varchar(5) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_accountgroupcontents`
--

INSERT INTO `fin_accountgroupcontents` (`acid`, `agid`, `name`, `lan`) VALUES
(1, 1, 'Asset', 'en'),
(2, 2, 'Liability', 'en'),
(3, 3, 'Income', 'en'),
(4, 4, 'Expense', 'en'),
(5, 5, 'Equity', 'en'),
(6, 1, 'varlık', 'tr'),
(7, 2, 'sorumluluk', 'tr'),
(8, 3, 'gelir', 'tr'),
(9, 4, 'gider', 'tr'),
(10, 5, 'öz kaynak', 'tr');

-- --------------------------------------------------------

--
-- Table structure for table `fin_accountgroups`
--

CREATE TABLE `fin_accountgroups` (
  `agid` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_accountgroups`
--

INSERT INTO `fin_accountgroups` (`agid`, `name`) VALUES
(1, 'Asset'),
(2, 'Liability'),
(3, 'Income'),
(4, 'Expense'),
(5, 'Equity');

-- --------------------------------------------------------

--
-- Table structure for table `fin_adminusers`
--

CREATE TABLE `fin_adminusers` (
  `aid` int(11) NOT NULL,
  `adminname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `createddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive','Trash') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_adminusers`
--

INSERT INTO `fin_adminusers` (`aid`, `adminname`, `username`, `email`, `password`, `createddate`, `status`) VALUES
(1, 'Admin', 'admin', 'sasi@coderobotics.com', 'f865b53623b121fd34ee5426c792e5c33af8c227', '2013-09-30 13:37:56', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `fin_agcategories`
--

CREATE TABLE `fin_agcategories` (
  `agcid` int(11) NOT NULL,
  `agid` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_agcategories`
--

INSERT INTO `fin_agcategories` (`agcid`, `agid`, `name`) VALUES
(1, 1, 'Bank'),
(2, 2, 'Current Liability'),
(3, 1, 'Current Asset'),
(4, 1, 'Fixed Asset'),
(5, 2, 'Non-Current Liability'),
(6, 3, 'Income'),
(7, 4, 'Cost Of Goods'),
(8, 4, 'Expense'),
(9, 5, 'Equity');

-- --------------------------------------------------------

--
-- Table structure for table `fin_agcategorycontents`
--

CREATE TABLE `fin_agcategorycontents` (
  `agccid` int(11) NOT NULL,
  `agcid` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `lan` varchar(5) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_agcategorycontents`
--

INSERT INTO `fin_agcategorycontents` (`agccid`, `agcid`, `name`, `lan`) VALUES
(1, 1, 'Bank', 'en'),
(2, 2, 'Current Liability', 'en'),
(3, 2, 'kredi kartı', 'tr'),
(4, 1, 'Banka', 'tr'),
(5, 3, 'Current Asset', 'en'),
(6, 4, 'Fixed Asset', 'en'),
(7, 5, 'Non-Current Liability', 'en'),
(8, 6, 'Income', 'en'),
(9, 7, 'Cost Of Goods', 'en'),
(10, 8, 'Expense', 'en'),
(11, 9, 'Equity', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `fin_agitemcontents`
--

CREATE TABLE `fin_agitemcontents` (
  `agicid` int(11) NOT NULL,
  `agiid` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `lan` varchar(5) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_agitemcontents`
--

INSERT INTO `fin_agitemcontents` (`agicid`, `agiid`, `name`, `lan`) VALUES
(1, 1, 'Savings Account ', 'en'),
(2, 2, 'Checking Account ', 'en'),
(3, 3, 'Money Market Account ', 'en'),
(4, 4, 'Cash Management Account ', 'en'),
(5, 5, 'Other Bank Account ', 'en'),
(6, 6, 'GIC & Term Deposits ', 'en'),
(7, 7, 'Certificate of Deposit ', 'en'),
(8, 8, 'Other Asset Account ', 'en'),
(9, 9, 'Brokerage Account ', 'en'),
(10, 10, 'Mutual Fund Account ', 'en'),
(11, 11, 'Shares ', 'en'),
(12, 12, 'Bonds ', 'en'),
(13, 13, 'T-Bills ', 'en'),
(14, 14, 'Other Short-Term Investments ', 'en'),
(15, 15, 'Allowance for Bad Debt ', 'en'),
(16, 16, 'Sales Tax Receivable ', 'en'),
(17, 17, 'Notes & Loans Receivable ', 'en'),
(18, 18, 'Inventory ', 'en'),
(19, 19, 'Due From Related Party ', 'en'),
(20, 20, 'Prepaid Expenses ', 'en'),
(21, 21, 'Future (deferred) Income Taxes ', 'en'),
(22, 22, 'Accrued Investment Income ', 'en'),
(23, 23, 'Taxes Recoverable/Refundable ', 'en'),
(24, 24, 'Other Current Asset ', 'en'),
(25, 25, 'Bank Overdraft ', 'en'),
(26, 26, 'Line of Credit ', 'en'),
(27, 27, 'Billing Accounts ', 'en'),
(28, 28, 'Credit Card ', 'en'),
(29, 29, 'Other Current Bank Debt ', 'en'),
(30, 30, 'Other Short-Term Debt ', 'en'),
(31, 31, 'Shareholder Loan ', 'en'),
(32, 32, 'Due to Other Related Party ', 'en'),
(33, 33, 'Deposits Received ', 'en'),
(34, 34, 'Dividends Payable ', 'en'),
(35, 35, 'Taxes Payable ', 'en'),
(36, 36, 'Future (deferred) Income Taxes ', 'en'),
(37, 37, 'Other Current Debt ', 'en'),
(38, 38, 'Prepaid Income ', 'en'),
(39, 39, 'Sales Tax Payable ', 'en'),
(40, 40, 'Payroll Liabilities ', 'en'),
(41, 41, 'Mortgages ', 'en'),
(42, 42, 'Home Equity Loans ', 'en'),
(43, 43, 'Loans ', 'en'),
(44, 44, 'Other Long-Term Debt ', 'en'),
(45, 45, 'Agricultural Program Payments ', 'en'),
(46, 46, 'Commodity Credit Loans ', 'en'),
(47, 47, 'Cooperative Distributions ', 'en'),
(48, 48, 'Crop Insurance Proceeds ', 'en'),
(49, 49, 'Crop Sales ', 'en'),
(50, 50, 'Custom Hire Income ', 'en'),
(51, 51, 'Farmers Market Sales ', 'en'),
(52, 52, 'Livestock Sales ', 'en'),
(53, 53, 'Other Agriculture Revenue ', 'en'),
(54, 54, 'Commission Income ', 'en'),
(55, 55, 'Commission Adjustments ', 'en'),
(56, 56, 'Other Commission Income ', 'en'),
(57, 57, 'Finance Charge Income ', 'en'),
(58, 58, 'Administrative Fees ', 'en'),
(59, 59, 'Capitation Fees ', 'en'),
(60, 60, 'Fuel Surcharge ', 'en'),
(61, 61, 'Fuel Tax Credits ', 'en'),
(62, 62, 'Layover Fees ', 'en'),
(63, 63, 'Pallet Fees ', 'en'),
(64, 64, 'Stop Fees ', 'en'),
(65, 65, 'Tenant Fees ', 'en'),
(66, 66, 'Settlement Fees ', 'en'),
(67, 67, 'Unloading Fees ', 'en'),
(68, 68, 'Other Fees & Charges ', 'en'),
(69, 69, 'Proceeds from Sale of Assets ', 'en'),
(70, 70, 'Insurance Proceeds Received ', 'en'),
(71, 71, 'Interest Income ', 'en'),
(72, 72, 'Miscellaneous Revenue ', 'en'),
(73, 73, 'Refunds ', 'en'),
(74, 74, 'Sales Discounts ', 'en'),
(75, 75, 'Reimbursements ', 'en'),
(76, 76, 'Other Income ', 'en'),
(77, 77, 'Payroll - Employee Payments Received ', 'en'),
(78, 78, 'Investments – Dividends ', 'en'),
(79, 79, 'Investments – Interest ', 'en'),
(80, 80, 'Investments – Asset Sales ', 'en'),
(81, 81, 'Investments – Other Investment Revenue ', 'en'),
(82, 82, 'Other Investment Revenue ', 'en'),
(83, 83, 'Accounting Services ', 'en'),
(84, 84, 'Bookkeeping Services ', 'en'),
(85, 85, 'Legal Fees ', 'en'),
(86, 86, 'Non-Medical Income ', 'en'),
(87, 87, 'Other Medical Income ', 'en'),
(88, 88, 'Payroll Services ', 'en'),
(89, 89, 'Tax Preparation Services ', 'en'),
(90, 90, 'Other Professional Services ', 'en'),
(91, 91, 'Owner Investment / Drawings ', 'en'),
(92, 92, 'Common Shares ', 'en'),
(93, 93, 'Preferred Shares ', 'en'),
(94, 94, 'Share Capital ', 'en'),
(95, 95, 'Contributed and Other Surplus ', 'en'),
(96, 96, 'Contributed Surplus ', 'en'),
(97, 97, 'Retained Earnings/Deficit ', 'en'),
(98, 98, 'Dividends Declared ', 'en'),
(99, 99, 'Cash Dividends ', 'en'),
(100, 100, 'Patronage Dividends ', 'en'),
(101, 101, 'Prior Period Adjustments ', 'en'),
(102, 102, 'Share Redemptions ', 'en'),
(103, 103, 'Special Reserves ', 'en'),
(104, 104, 'Currency Adjustments ', 'en'),
(105, 105, 'Other Equity ', 'en'),
(106, 106, 'Merchant Account Fees ', 'en'),
(107, 107, 'Subcontracted Services ', 'en'),
(108, 108, 'Blueprints & Reproduction ', 'en'),
(109, 109, 'Bond Expense ', 'en'),
(110, 110, 'Equipment Rental for Jobs ', 'en'),
(111, 111, 'Fuel ', 'en'),
(112, 112, 'Freight & Shipping Costs ', 'en'),
(113, 113, 'Contracted Services ', 'en'),
(114, 114, 'Commissions Paid ', 'en'),
(115, 115, 'Construction Materials ', 'en'),
(116, 116, 'Equipment Rental ', 'en'),
(117, 117, 'Linens & Lodging Supplies ', 'en'),
(118, 118, 'Materials Cost ', 'en'),
(119, 119, 'Media Purchased for Clients ', 'en'),
(120, 120, 'Other Construction Costs ', 'en'),
(121, 121, 'Other Job Related Costs ', 'en'),
(122, 122, 'Outsourced Service Providers ', 'en'),
(123, 123, 'Product Samples ', 'en'),
(124, 124, 'Production & Supplies ', 'en'),
(125, 125, 'Purchases – Food & Beverage ', 'en'),
(126, 126, 'Purchases – Software for Resale ', 'en'),
(127, 127, 'Purchases – Hardware for Resale ', 'en'),
(128, 128, 'Purchases – Parts & Materials ', 'en'),
(129, 129, 'Purchases – Resale Items ', 'en'),
(130, 130, 'Restaurant Supplies ', 'en'),
(131, 131, 'Show and Exhibitor Fees ', 'en'),
(132, 132, 'Studio and Location Costs ', 'en'),
(133, 133, 'Subcontractors\' Expense ', 'en'),
(134, 134, 'Tools and Craft Supplies ', 'en'),
(135, 135, 'Other Cost of Goods ', 'en'),
(136, 136, 'Etsy Service Charges ', 'en'),
(137, 137, 'Chemicals Purchased ', 'en'),
(138, 138, 'Custom Hire & Contract Labor ', 'en'),
(139, 139, 'Feed Purchased ', 'en'),
(140, 140, 'Fertilizers & Lime ', 'en'),
(141, 141, 'Freight & Trucking ', 'en'),
(142, 142, 'Gasoline, Fuel & Oil ', 'en'),
(143, 143, 'Seed & Plants Purchased ', 'en'),
(144, 144, 'Vaccines & Medicines ', 'en'),
(145, 145, 'Veterinary, Breeding, Medicine ', 'en'),
(146, 146, 'Other Agriculture Expense ', 'en'),
(147, 147, 'Donations ', 'en'),
(148, 148, 'Miscellaneous Expense ', 'en'),
(149, 149, 'Political Contributions ', 'en'),
(150, 150, 'Amortization ', 'en'),
(151, 151, 'Conferences & Meetings ', 'en'),
(152, 152, 'Conservation Expense ', 'en'),
(153, 153, 'Catering and Facilities ', 'en'),
(154, 154, 'Other Expenses ', 'en'),
(155, 155, 'Equipment Lease or Rental ', 'en'),
(156, 156, 'Building & Property Security ', 'en'),
(157, 157, 'Property Management Fees ', 'en'),
(158, 158, 'Shop Expense ', 'en'),
(159, 159, 'Storage & Warehousing ', 'en'),
(160, 160, 'Custom Building & Equipment ', 'en'),
(161, 161, 'Building Maintenance ', 'en'),
(162, 162, 'Telephone – Land Line ', 'en'),
(163, 163, 'Other Computer & Communication ', 'en'),
(164, 164, 'Business Licenses & Permits ', 'en'),
(165, 165, 'Dues & Subscriptions ', 'en'),
(166, 166, 'Bad Debts ', 'en'),
(167, 167, 'Business Registration Fees ', 'en'),
(168, 168, 'Cash Over & Short ', 'en'),
(169, 169, 'Credit Card Discount Fees ', 'en'),
(170, 170, 'Fines, Penalties & Judgments ', 'en'),
(171, 171, 'Fundraising Fees ', 'en'),
(172, 172, 'Late Fees ', 'en'),
(173, 173, 'Memberships & Dues ', 'en'),
(174, 174, 'Trade Commissions ', 'en'),
(175, 175, 'Other Fees, Charges & Subscriptions ', 'en'),
(176, 176, 'Insurance – Life & Disability ', 'en'),
(177, 177, 'Insurance – Worker\'s Compensation ', 'en'),
(178, 178, 'Insurance – Professional Liability ', 'en'),
(179, 179, 'Insurance – General Liability ', 'en'),
(180, 180, 'Insurance – Health ', 'en'),
(181, 181, 'Insurance – Property ', 'en'),
(182, 182, 'Insurance – Other ', 'en'),
(183, 183, 'Awards & Grants ', 'en'),
(184, 184, 'Evangelism & Special Events ', 'en'),
(185, 185, 'Ministry Expenses ', 'en'),
(186, 186, 'Other Non-Profit Expenses ', 'en'),
(187, 187, 'Janitorial Expense ', 'en'),
(188, 188, 'Postage & Delivery ', 'en'),
(189, 189, 'Printing and Reproduction ', 'en'),
(190, 190, 'Furniture & Decoration ', 'en'),
(191, 191, 'Landscaping ', 'en'),
(192, 192, 'Medical Records Supplies ', 'en'),
(193, 193, 'Reference Materials ', 'en'),
(194, 194, 'Training Materials ', 'en'),
(195, 195, 'Other Office Expenses ', 'en'),
(196, 196, 'Management Fees ', 'en'),
(197, 197, 'Payroll – Commissions ', 'en'),
(198, 198, 'Payroll – Bonuses ', 'en'),
(199, 199, 'Other Payroll ', 'en'),
(200, 200, 'Payroll – Tax ', 'en'),
(201, 201, 'Payroll – Employee Expenses Paid ', 'en'),
(202, 202, 'Education & Training ', 'en'),
(203, 203, 'Business Services ', 'en'),
(204, 204, 'Contract Services ', 'en'),
(205, 205, 'Financial Advisor ', 'en'),
(206, 206, 'Laboratory Fees ', 'en'),
(207, 207, 'Laundry ', 'en'),
(208, 208, 'Legal Fees ', 'en'),
(209, 209, 'Marketing Expense ', 'en'),
(210, 210, 'Music & Entertainment ', 'en'),
(211, 211, 'Research Services ', 'en'),
(212, 212, 'Training ', 'en'),
(213, 213, 'Transcription Services ', 'en'),
(214, 214, 'Volunteer Services ', 'en'),
(215, 215, 'Assessment Costs ', 'en'),
(216, 216, 'Photography Processing & Supplies ', 'en'),
(217, 217, 'Other Services ', 'en'),
(218, 218, 'Taxes – Property Tax ', 'en'),
(219, 219, 'Taxes – Local Tax ', 'en'),
(220, 220, 'Taxes – Corporate Tax ', 'en'),
(221, 221, 'Other Taxes ', 'en'),
(222, 222, 'Linen Expense ', 'en'),
(223, 223, 'Medical Supplies ', 'en'),
(224, 224, 'Salon Supplies, Linens, Laundry ', 'en'),
(225, 225, 'Small Medical Equipment ', 'en'),
(226, 226, 'Small Tools & Equipment ', 'en'),
(227, 227, 'Uniforms ', 'en'),
(228, 228, 'Other Tools & Supplies ', 'en'),
(229, 229, 'Vehicle – Lease Payments ', 'en'),
(230, 230, 'Parking ', 'en'),
(231, 231, 'Public Transportation ', 'en'),
(232, 232, 'Rental Car & Taxi ', 'en'),
(233, 233, 'Other Vehicle Expenses ', 'en'),
(234, 234, 'Benevolence Offerings ', 'en'),
(235, 235, 'Building Fund ', 'en'),
(236, 236, 'Children\'s Church Offering ', 'en'),
(237, 237, 'General Fund ', 'en'),
(238, 238, 'Indirect Public Support ', 'en'),
(239, 239, 'Mission Offerings ', 'en'),
(240, 240, 'Program Income – Member Assessments ', 'en'),
(241, 241, 'Program Income – Membership Dues ', 'en'),
(242, 242, 'Program Income – Program Service Fees ', 'en'),
(243, 243, 'Pledges ', 'en'),
(244, 244, 'Youth Group ', 'en'),
(245, 245, 'Advertising Sales ', 'en'),
(246, 246, 'Banquets & Events ', 'en'),
(247, 247, 'Bar Sales ', 'en'),
(248, 248, 'Booth Rental Income ', 'en'),
(249, 249, 'Catering Sales ', 'en'),
(250, 250, 'Construction Income ', 'en'),
(251, 251, 'Consulting Income ', 'en'),
(252, 252, 'Design Income ', 'en'),
(253, 253, 'Facial Treatments ', 'en'),
(254, 254, 'Food & Beverage Sales ', 'en'),
(255, 255, 'Gift Shop & Vending Sales ', 'en'),
(256, 256, 'Gross Trucking Income ', 'en'),
(257, 257, 'Hair Coloring Services ', 'en'),
(258, 258, 'Installation Services ', 'en'),
(259, 259, 'Inventory Sales ', 'en'),
(260, 260, 'Job Income ', 'en'),
(261, 261, 'Lodging ', 'en'),
(262, 262, 'Maintenance Services ', 'en'),
(263, 263, 'Manicure/Pedicure ', 'en'),
(264, 264, 'Massage Services ', 'en'),
(265, 265, 'Rental Income ', 'en'),
(266, 266, 'Sales – Retail Products ', 'en'),
(267, 267, 'Security Sales ', 'en'),
(268, 268, 'Services ', 'en'),
(269, 269, 'Special Events Income ', 'en'),
(270, 270, 'Photo & Video Services Income ', 'en'),
(271, 271, 'Product Sales ', 'en'),
(272, 272, 'Royalties Received ', 'en'),
(273, 273, 'Seminars ', 'en'),
(274, 274, 'Special Services ', 'en'),
(275, 275, 'Tape & Book Sales ', 'en'),
(276, 276, 'Labor Income ', 'en'),
(277, 277, 'Parts & Materials Sales ', 'en'),
(278, 278, 'Sales – Hardware ', 'en'),
(279, 279, 'Sales – Software ', 'en'),
(280, 280, 'Sales – Software Subscription ', 'en'),
(281, 281, 'Sales – Support and Maintenance ', 'en'),
(282, 282, 'Shipping & Delivery Income ', 'en'),
(283, 283, 'Coaching ', 'en'),
(284, 284, 'Training ', 'en'),
(285, 285, 'Assessments ', 'en'),
(286, 286, 'Facilitation ', 'en'),
(287, 287, 'Other Products & Services ', 'en'),
(288, 288, 'Reimbursed Expenses ', 'en'),
(289, 289, 'Land ', 'en'),
(290, 290, 'Land Improvements ', 'en'),
(291, 291, 'Accumulated Amortization of Land Improvements ', 'en'),
(292, 292, 'Depletable Assets ', 'en'),
(293, 293, 'Accumulated Amortization of Depletable Assets ', 'en'),
(294, 294, 'Resource Properties ', 'en'),
(295, 295, 'Accumulated Amortization of Resource Properties ', 'en'),
(296, 296, 'Buildings ', 'en'),
(297, 297, 'Accumulated Amortization of Buildings ', 'en'),
(298, 298, 'Machinery, equipment, furniture & fixtures ', 'en'),
(299, 299, 'Accumulated Amortization of Machinery, Equipment, Furniture & Fixtures ', 'en'),
(300, 300, 'Other Tangible Capital Assets ', 'en'),
(301, 301, 'Accumulated Amortization of Other Tangible Assets ', 'en'),
(302, 302, 'Goodwill ', 'en'),
(303, 303, 'Accumulated Amortization of Goodwill ', 'en'),
(304, 304, 'Incorporation Costs ', 'en'),
(305, 305, 'Accumulated Amortization of Incorporation Costs ', 'en'),
(306, 306, 'Resource Rights ', 'en'),
(307, 307, 'Accumulated Amortization of Resource Rights ', 'en'),
(308, 308, 'Other Intangible Assets ', 'en'),
(309, 309, 'Other Long Term Assets ', 'en'),
(310, 310, 'Whole Life Insurance ', 'en'),
(311, 311, 'Universal Life Insurance ', 'en'),
(312, 312, 'Term Life Insurance ', 'en'),
(313, 313, 'Annuity ', 'en'),
(314, 314, 'Other Investments ', 'en'),
(315, 315, 'Direct Public Grants – Corporate and Business ', 'en'),
(316, 316, 'Direct Public Grants – Foundation and Trust Grants ', 'en'),
(317, 317, 'Direct Public Grants – Nonprofit Organization Grants ', 'en'),
(318, 318, 'Direct Public Support – Corporate Contributions ', 'en'),
(319, 319, 'Direct Public Support – Donated Art ', 'en'),
(320, 320, 'Direct Public Support – Donated Professional Fees or Facilities ', 'en'),
(321, 321, 'Direct Public Support – Gifts in Kind ', 'en'),
(322, 322, 'Direct Public Support – Business Contributions ', 'en'),
(323, 323, 'Direct Public Support – Individual Contributions ', 'en'),
(324, 324, 'Direct Public Support – Legacies and Bequests ', 'en'),
(325, 325, 'Direct Public Support – Uncollectible Pledges ', 'en'),
(326, 326, 'Direct Public Support – Volunteer Services ', 'en'),
(327, 327, 'Government Contracts – Agency ', 'en'),
(328, 328, 'Government Contracts – Federal ', 'en'),
(329, 329, 'Government Contracts – Local Government ', 'en'),
(330, 330, 'Government Contracts – State or Provincial ', 'en'),
(331, 331, 'Government Grants – Agency ', 'en'),
(332, 332, 'Government Grants – Federal ', 'en'),
(333, 333, 'Government Grants – Local Government ', 'en'),
(334, 334, 'Government Grants – State or Provincial ', 'en'),
(335, 335, 'Accounting Fees', 'en'),
(336, 336, 'Advertising & Promotion', 'en'),
(337, 337, 'Bank Service Charges', 'en'),
(338, 338, 'Computer – Hardware', 'en'),
(339, 339, 'Computer – Hosting', 'en'),
(340, 340, 'Computer – Internet', 'en'),
(341, 341, 'Computer – Software', 'en'),
(342, 342, 'Depreciation Expense', 'en'),
(343, 343, 'Insurance – Vehicles', 'en'),
(344, 344, 'Interest Expense', 'en'),
(345, 345, 'Meals and Entertainment', 'en'),
(346, 346, 'Office Supplies', 'en'),
(347, 347, 'Payroll – Employee Benefits', 'en'),
(348, 348, 'Payroll – Employer\'s Share of Benefits', 'en'),
(349, 349, 'Payroll – Salary & Wages', 'en'),
(350, 350, 'Professional Fees', 'en'),
(351, 351, 'Rent Expense', 'en'),
(352, 352, 'Repairs & Maintenance', 'en'),
(353, 353, 'Telephone – Wireless', 'en'),
(354, 354, 'Travel Expense', 'en'),
(355, 355, 'Utilities', 'en'),
(356, 356, 'Vehicle – Fuel', 'en'),
(357, 357, 'Vehicle – Repairs & Maintenance', 'en'),
(358, 358, 'Owner\'s Equity', 'en'),
(359, 359, 'Loss on Foreign Exchange', 'en'),
(360, 360, 'Gain on Foreign Exchange', 'en'),
(361, 361, 'Unrealized Loss on Foreign Currency', 'en'),
(362, 362, 'Accounts Payable', 'en'),
(363, 363, 'Unrealized Gain on Foreign Currency', 'en'),
(364, 364, 'Accounts Receivable', 'en'),
(365, 365, 'Sales', 'en'),
(366, 366, 'Cash on Hand', 'en'),
(367, 1, 'tasarruf Hesabı', 'tr'),
(368, 367, 'check account', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `fin_agitems`
--

CREATE TABLE `fin_agitems` (
  `agiid` int(11) NOT NULL,
  `agid` int(11) NOT NULL,
  `agcid` int(11) NOT NULL,
  `agsid` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_agitems`
--

INSERT INTO `fin_agitems` (`agiid`, `agid`, `agcid`, `agsid`, `name`) VALUES
(1, 1, 1, 1, 'Savings Account '),
(2, 1, 1, 1, 'Checking Account '),
(3, 1, 1, 1, 'Money Market Account '),
(4, 1, 1, 1, 'Cash Management Account '),
(5, 1, 1, 1, 'Other Bank Account '),
(6, 1, 3, 3, 'GIC & Term Deposits '),
(7, 1, 3, 3, 'Certificate of Deposit '),
(8, 1, 3, 3, 'Other Asset Account '),
(9, 1, 3, 3, 'Brokerage Account '),
(10, 1, 3, 3, 'Mutual Fund Account '),
(11, 1, 3, 3, 'Shares '),
(12, 1, 3, 3, 'Bonds '),
(13, 1, 3, 3, 'T-Bills '),
(14, 1, 3, 3, 'Other Short-Term Investments '),
(15, 1, 3, 4, 'Allowance for Bad Debt '),
(16, 1, 3, 4, 'Sales Tax Receivable '),
(17, 1, 3, 4, 'Notes & Loans Receivable '),
(18, 1, 3, 4, 'Inventory '),
(19, 1, 3, 4, 'Due From Related Party '),
(20, 1, 3, 4, 'Prepaid Expenses '),
(21, 1, 3, 4, 'Future (deferred) Income Taxes '),
(22, 1, 3, 4, 'Accrued Investment Income '),
(23, 1, 3, 4, 'Taxes Recoverable/Refundable '),
(24, 1, 3, 4, 'Other Current Asset '),
(25, 2, 2, 2, 'Bank Overdraft '),
(26, 2, 2, 2, 'Line of Credit '),
(27, 2, 2, 2, 'Billing Accounts '),
(28, 2, 2, 2, 'Credit Card '),
(29, 2, 2, 2, 'Other Current Bank Debt '),
(30, 2, 2, 7, 'Other Short-Term Debt '),
(31, 2, 2, 7, 'Shareholder Loan '),
(32, 2, 2, 7, 'Due to Other Related Party '),
(33, 2, 2, 7, 'Deposits Received '),
(34, 2, 2, 7, 'Dividends Payable '),
(35, 2, 2, 7, 'Taxes Payable '),
(36, 2, 2, 7, 'Future (deferred) Income Taxes '),
(37, 2, 2, 7, 'Other Current Debt '),
(38, 2, 2, 7, 'Prepaid Income '),
(39, 2, 2, 7, 'Sales Tax Payable '),
(40, 2, 2, 7, 'Payroll Liabilities '),
(41, 2, 5, 8, 'Mortgages '),
(42, 2, 5, 8, 'Home Equity Loans '),
(43, 2, 5, 8, 'Loans '),
(44, 2, 5, 8, 'Other Long-Term Debt '),
(45, 3, 6, 9, 'Agricultural Program Payments '),
(46, 3, 6, 9, 'Commodity Credit Loans '),
(47, 3, 6, 9, 'Cooperative Distributions '),
(48, 3, 6, 9, 'Crop Insurance Proceeds '),
(49, 3, 6, 9, 'Crop Sales '),
(50, 3, 6, 9, 'Custom Hire Income '),
(51, 3, 6, 9, 'Farmers Market Sales '),
(52, 3, 6, 9, 'Livestock Sales '),
(53, 3, 6, 9, 'Other Agriculture Revenue '),
(54, 3, 6, 10, 'Commission Income '),
(55, 3, 6, 10, 'Commission Adjustments '),
(56, 3, 6, 10, 'Other Commission Income '),
(57, 3, 6, 16, 'Finance Charge Income '),
(58, 3, 6, 16, 'Administrative Fees '),
(59, 3, 6, 16, 'Capitation Fees '),
(60, 3, 6, 16, 'Fuel Surcharge '),
(61, 3, 6, 16, 'Fuel Tax Credits '),
(62, 3, 6, 16, 'Layover Fees '),
(63, 3, 6, 16, 'Pallet Fees '),
(64, 3, 6, 16, 'Stop Fees '),
(65, 3, 6, 16, 'Tenant Fees '),
(66, 3, 6, 16, 'Settlement Fees '),
(67, 3, 6, 16, 'Unloading Fees '),
(68, 3, 6, 16, 'Other Fees & Charges '),
(69, 3, 6, 11, 'Proceeds from Sale of Assets '),
(70, 3, 6, 11, 'Insurance Proceeds Received '),
(71, 3, 6, 11, 'Interest Income '),
(72, 3, 6, 11, 'Miscellaneous Revenue '),
(73, 3, 6, 11, 'Refunds '),
(74, 3, 6, 11, 'Sales Discounts '),
(75, 3, 6, 11, 'Reimbursements '),
(76, 3, 6, 11, 'Other Income '),
(77, 3, 6, 11, 'Payroll - Employee Payments Received '),
(78, 3, 6, 17, 'Investments – Dividends '),
(79, 3, 6, 17, 'Investments – Interest '),
(80, 3, 6, 17, 'Investments – Asset Sales '),
(81, 3, 6, 17, 'Investments – Other Investment Revenue '),
(82, 3, 6, 17, 'Other Investment Revenue '),
(83, 3, 6, 19, 'Accounting Services '),
(84, 3, 6, 19, 'Bookkeeping Services '),
(85, 3, 6, 19, 'Legal Fees '),
(86, 3, 6, 19, 'Non-Medical Income '),
(87, 3, 6, 19, 'Other Medical Income '),
(88, 3, 6, 19, 'Payroll Services '),
(89, 3, 6, 19, 'Tax Preparation Services '),
(90, 3, 6, 19, 'Other Professional Services '),
(91, 5, 9, 15, 'Owner Investment / Drawings '),
(92, 5, 9, 15, 'Common Shares '),
(93, 5, 9, 15, 'Preferred Shares '),
(94, 5, 9, 15, 'Share Capital '),
(95, 5, 9, 15, 'Contributed and Other Surplus '),
(96, 5, 9, 15, 'Contributed Surplus '),
(97, 5, 9, 15, 'Retained Earnings/Deficit '),
(98, 5, 9, 15, 'Dividends Declared '),
(99, 5, 9, 15, 'Cash Dividends '),
(100, 5, 9, 15, 'Patronage Dividends '),
(101, 5, 9, 15, 'Prior Period Adjustments '),
(102, 5, 9, 15, 'Share Redemptions '),
(103, 5, 9, 15, 'Special Reserves '),
(104, 5, 9, 15, 'Currency Adjustments '),
(105, 5, 9, 15, 'Other Equity '),
(106, 4, 7, 12, 'Merchant Account Fees '),
(107, 4, 7, 12, 'Subcontracted Services '),
(108, 4, 7, 12, 'Blueprints & Reproduction '),
(109, 4, 7, 12, 'Bond Expense '),
(110, 4, 7, 12, 'Equipment Rental for Jobs '),
(111, 4, 7, 12, 'Fuel '),
(112, 4, 7, 12, 'Freight & Shipping Costs '),
(113, 4, 7, 12, 'Contracted Services '),
(114, 4, 7, 12, 'Commissions Paid '),
(115, 4, 7, 12, 'Construction Materials '),
(116, 4, 7, 12, 'Equipment Rental '),
(117, 4, 7, 12, 'Linens & Lodging Supplies '),
(118, 4, 7, 12, 'Materials Cost '),
(119, 4, 7, 12, 'Media Purchased for Clients '),
(120, 4, 7, 12, 'Other Construction Costs '),
(121, 4, 7, 12, 'Other Job Related Costs '),
(122, 4, 7, 12, 'Outsourced Service Providers '),
(123, 4, 7, 12, 'Product Samples '),
(124, 4, 7, 12, 'Production & Supplies '),
(125, 4, 7, 12, 'Purchases – Food & Beverage '),
(126, 4, 7, 12, 'Purchases – Software for Resale '),
(127, 4, 7, 12, 'Purchases – Hardware for Resale '),
(128, 4, 7, 12, 'Purchases – Parts & Materials '),
(129, 4, 7, 12, 'Purchases – Resale Items '),
(130, 4, 7, 12, 'Restaurant Supplies '),
(131, 4, 7, 12, 'Show and Exhibitor Fees '),
(132, 4, 7, 12, 'Studio and Location Costs '),
(133, 4, 7, 12, 'Subcontractors Expense '),
(134, 4, 7, 12, 'Tools and Craft Supplies '),
(135, 4, 7, 12, 'Other Cost of Goods '),
(136, 4, 7, 12, 'Etsy Service Charges '),
(137, 4, 8, 13, 'Chemicals Purchased '),
(138, 4, 8, 13, 'Custom Hire & Contract Labor '),
(139, 4, 8, 13, 'Feed Purchased '),
(140, 4, 8, 13, 'Fertilizers & Lime '),
(141, 4, 8, 13, 'Freight & Trucking '),
(142, 4, 8, 13, 'Gasoline, Fuel & Oil '),
(143, 4, 8, 13, 'Seed & Plants Purchased '),
(144, 4, 8, 13, 'Vaccines & Medicines '),
(145, 4, 8, 13, 'Veterinary, Breeding, Medicine '),
(146, 4, 8, 13, 'Other Agriculture Expense '),
(147, 4, 8, 14, 'Donations '),
(148, 4, 8, 14, 'Miscellaneous Expense '),
(149, 4, 8, 14, 'Political Contributions '),
(150, 4, 8, 14, 'Amortization '),
(151, 4, 8, 14, 'Conferences & Meetings '),
(152, 4, 8, 14, 'Conservation Expense '),
(153, 4, 8, 14, 'Catering and Facilities '),
(154, 4, 8, 14, 'Other Expenses '),
(155, 4, 8, 21, 'Equipment Lease or Rental '),
(156, 4, 8, 21, 'Building & Property Security '),
(157, 4, 8, 21, 'Property Management Fees '),
(158, 4, 8, 21, 'Shop Expense '),
(159, 4, 8, 21, 'Storage & Warehousing '),
(160, 4, 8, 21, 'Custom Building & Equipment '),
(161, 4, 8, 21, 'Building Maintenance '),
(162, 4, 8, 22, 'Telephone – Land Line '),
(163, 4, 8, 22, 'Other Computer & Communication '),
(164, 4, 8, 23, 'Business Licenses & Permits '),
(165, 4, 8, 23, 'Dues & Subscriptions '),
(166, 4, 8, 23, 'Bad Debts '),
(167, 4, 8, 23, 'Business Registration Fees '),
(168, 4, 8, 23, 'Cash Over & Short '),
(169, 4, 8, 23, 'Credit Card Discount Fees '),
(170, 4, 8, 23, 'Fines, Penalties & Judgments '),
(171, 4, 8, 23, 'Fundraising Fees '),
(172, 4, 8, 23, 'Late Fees '),
(173, 4, 8, 23, 'Memberships & Dues '),
(174, 4, 8, 23, 'Trade Commissions '),
(175, 4, 8, 23, 'Other Fees, Charges & Subscriptions '),
(176, 4, 8, 24, 'Insurance – Life & Disability '),
(177, 4, 8, 24, 'Insurance – Worker\'s Compensation '),
(178, 4, 8, 24, 'Insurance – Professional Liability '),
(179, 4, 8, 24, 'Insurance – General Liability '),
(180, 4, 8, 24, 'Insurance – Health '),
(181, 4, 8, 24, 'Insurance – Property '),
(182, 4, 8, 24, 'Insurance – Other '),
(183, 4, 8, 25, 'Awards & Grants '),
(184, 4, 8, 25, 'Evangelism & Special Events '),
(185, 4, 8, 25, 'Ministry Expenses '),
(186, 4, 8, 25, 'Other Non-Profit Expenses '),
(187, 4, 8, 26, 'Janitorial Expense '),
(188, 4, 8, 26, 'Postage & Delivery '),
(189, 4, 8, 26, 'Printing and Reproduction '),
(190, 4, 8, 26, 'Furniture & Decoration '),
(191, 4, 8, 26, 'Landscaping '),
(192, 4, 8, 26, 'Medical Records Supplies '),
(193, 4, 8, 26, 'Reference Materials '),
(194, 4, 8, 26, 'Training Materials '),
(195, 4, 8, 26, 'Other Office Expenses '),
(196, 4, 8, 27, 'Management Fees '),
(197, 4, 8, 27, 'Payroll – Commissions '),
(198, 4, 8, 27, 'Payroll – Bonuses '),
(199, 4, 8, 27, 'Other Payroll '),
(200, 4, 8, 27, 'Payroll – Tax '),
(201, 4, 8, 27, 'Payroll – Employee Expenses Paid '),
(202, 4, 8, 28, 'Education & Training '),
(203, 4, 8, 28, 'Business Services '),
(204, 4, 8, 28, 'Contract Services '),
(205, 4, 8, 28, 'Financial Advisor '),
(206, 4, 8, 28, 'Laboratory Fees '),
(207, 4, 8, 28, 'Laundry '),
(208, 4, 8, 28, 'Legal Fees '),
(209, 4, 8, 28, 'Marketing Expense '),
(210, 4, 8, 28, 'Music & Entertainment '),
(211, 4, 8, 28, 'Research Services '),
(212, 4, 8, 28, 'Training '),
(213, 4, 8, 28, 'Transcription Services '),
(214, 4, 8, 28, 'Volunteer Services '),
(215, 4, 8, 28, 'Assessment Costs '),
(216, 4, 8, 28, 'Photography Processing & Supplies '),
(217, 4, 8, 28, 'Other Services '),
(218, 4, 8, 29, 'Taxes – Property Tax '),
(219, 4, 8, 29, 'Taxes – Local Tax '),
(220, 4, 8, 29, 'Taxes – Corporate Tax '),
(221, 4, 8, 29, 'Other Taxes '),
(222, 4, 8, 30, 'Linen Expense '),
(223, 4, 8, 30, 'Medical Supplies '),
(224, 4, 8, 30, 'Salon Supplies, Linens, Laundry '),
(225, 4, 8, 30, 'Small Medical Equipment '),
(226, 4, 8, 30, 'Small Tools & Equipment '),
(227, 4, 8, 30, 'Uniforms '),
(228, 4, 8, 30, 'Other Tools & Supplies '),
(229, 4, 8, 31, 'Vehicle – Lease Payments '),
(230, 4, 8, 31, 'Parking '),
(231, 4, 8, 31, 'Public Transportation '),
(232, 4, 8, 31, 'Rental Car & Taxi '),
(233, 4, 8, 31, 'Other Vehicle Expenses '),
(234, 3, 6, 18, 'Benevolence Offerings '),
(235, 3, 6, 18, 'Building Fund '),
(236, 3, 6, 18, 'Children\'s Church Offering '),
(237, 3, 6, 18, 'General Fund '),
(238, 3, 6, 18, 'Indirect Public Support '),
(239, 3, 6, 18, 'Mission Offerings '),
(240, 3, 6, 18, 'Program Income – Member Assessments '),
(241, 3, 6, 18, 'Program Income – Membership Dues '),
(242, 3, 6, 18, 'Program Income – Program Service Fees '),
(243, 3, 6, 18, 'Pledges '),
(244, 3, 6, 18, 'Youth Group '),
(245, 3, 6, 20, 'Advertising Sales '),
(246, 3, 6, 20, 'Banquets & Events '),
(247, 3, 6, 20, 'Bar Sales '),
(248, 3, 6, 20, 'Booth Rental Income '),
(249, 3, 6, 20, 'Catering Sales '),
(250, 3, 6, 20, 'Construction Income '),
(251, 3, 6, 20, 'Consulting Income '),
(252, 3, 6, 20, 'Design Income '),
(253, 3, 6, 20, 'Facial Treatments '),
(254, 3, 6, 20, 'Food & Beverage Sales '),
(255, 3, 6, 20, 'Gift Shop & Vending Sales '),
(256, 3, 6, 20, 'Gross Trucking Income '),
(257, 3, 6, 20, 'Hair Coloring Services '),
(258, 3, 6, 20, 'Installation Services '),
(259, 3, 6, 20, 'Inventory Sales '),
(260, 3, 6, 20, 'Job Income '),
(261, 3, 6, 20, 'Lodging '),
(262, 3, 6, 20, 'Maintenance Services '),
(263, 3, 6, 20, 'Manicure/Pedicure '),
(264, 3, 6, 20, 'Massage Services '),
(265, 3, 6, 20, 'Rental Income '),
(266, 3, 6, 20, 'Sales – Retail Products '),
(267, 3, 6, 20, 'Security Sales '),
(268, 3, 6, 20, 'Services '),
(269, 3, 6, 20, 'Special Events Income '),
(270, 3, 6, 20, 'Photo & Video Services Income '),
(271, 3, 6, 20, 'Product Sales '),
(272, 3, 6, 20, 'Royalties Received '),
(273, 3, 6, 20, 'Seminars '),
(274, 3, 6, 20, 'Special Services '),
(275, 3, 6, 20, 'Tape & Book Sales '),
(276, 3, 6, 20, 'Labor Income '),
(277, 3, 6, 20, 'Parts & Materials Sales '),
(278, 3, 6, 20, 'Sales – Hardware '),
(279, 3, 6, 20, 'Sales – Software '),
(280, 3, 6, 20, 'Sales – Software Subscription '),
(281, 3, 6, 20, 'Sales – Support and Maintenance '),
(282, 3, 6, 20, 'Shipping & Delivery Income '),
(283, 3, 6, 20, 'Coaching '),
(284, 3, 6, 20, 'Training '),
(285, 3, 6, 20, 'Assessments '),
(286, 3, 6, 20, 'Facilitation '),
(287, 3, 6, 20, 'Other Products & Services '),
(288, 3, 6, 20, 'Reimbursed Expenses '),
(289, 1, 4, 5, 'Land '),
(290, 1, 4, 5, 'Land Improvements '),
(291, 1, 4, 5, 'Accumulated Amortization of Land Improvements '),
(292, 1, 4, 5, 'Depletable Assets '),
(293, 1, 4, 5, 'Accumulated Amortization of Depletable Assets '),
(294, 1, 4, 5, 'Resource Properties '),
(295, 1, 4, 5, 'Accumulated Amortization of Resource Properties '),
(296, 1, 4, 5, 'Buildings '),
(297, 1, 4, 5, 'Accumulated Amortization of Buildings '),
(298, 1, 4, 5, 'Machinery, equipment, furniture & fixtures '),
(299, 1, 4, 5, 'Accumulated Amortization of Machinery, Equipment, Furniture & Fixtures '),
(300, 1, 4, 5, 'Other Tangible Capital Assets '),
(301, 1, 4, 5, 'Accumulated Amortization of Other Tangible Assets '),
(302, 1, 4, 5, 'Goodwill '),
(303, 1, 4, 5, 'Accumulated Amortization of Goodwill '),
(304, 1, 4, 5, 'Incorporation Costs '),
(305, 1, 4, 5, 'Accumulated Amortization of Incorporation Costs '),
(306, 1, 4, 5, 'Resource Rights '),
(307, 1, 4, 5, 'Accumulated Amortization of Resource Rights '),
(308, 1, 4, 5, 'Other Intangible Assets '),
(309, 1, 4, 5, 'Other Long Term Assets '),
(310, 1, 4, 6, 'Whole Life Insurance '),
(311, 1, 4, 6, 'Universal Life Insurance '),
(312, 1, 4, 6, 'Term Life Insurance '),
(313, 1, 4, 6, 'Annuity '),
(314, 1, 4, 6, 'Other Investments '),
(315, 3, 6, 18, 'Direct Public Grants – Corporate and Business '),
(316, 3, 6, 18, 'Direct Public Grants – Foundation and Trust Grants '),
(317, 3, 6, 18, 'Direct Public Grants – Nonprofit Organization Grants '),
(318, 3, 6, 18, 'Direct Public Support – Corporate Contributions '),
(319, 3, 6, 18, 'Direct Public Support – Donated Art '),
(320, 3, 6, 18, 'Direct Public Support – Donated Professional Fees or Facilities '),
(321, 3, 6, 18, 'Direct Public Support – Gifts in Kind '),
(322, 3, 6, 18, 'Direct Public Support – Business Contributions '),
(323, 3, 6, 18, 'Direct Public Support – Individual Contributions '),
(324, 3, 6, 18, 'Direct Public Support – Legacies and Bequests '),
(325, 3, 6, 18, 'Direct Public Support – Uncollectible Pledges '),
(326, 3, 6, 18, 'Direct Public Support – Volunteer Services '),
(327, 3, 6, 18, 'Government Contracts – Agency '),
(328, 3, 6, 18, 'Government Contracts – Federal '),
(329, 3, 6, 18, 'Government Contracts – Local Government '),
(330, 3, 6, 18, 'Government Contracts – State or Provincial '),
(331, 3, 6, 18, 'Government Grants – Agency '),
(332, 3, 6, 18, 'Government Grants – Federal '),
(333, 3, 6, 18, 'Government Grants – Local Government '),
(334, 3, 6, 18, 'Government Grants – State or Provincial '),
(335, 4, 8, 28, 'Accounting Fees'),
(336, 4, 8, 28, 'Advertising & Promotion'),
(337, 4, 8, 23, 'Bank Service Charges'),
(338, 4, 8, 22, 'Computer – Hardware'),
(339, 4, 8, 22, 'Computer – Hosting'),
(340, 4, 8, 22, 'Computer – Internet'),
(341, 4, 8, 22, 'Computer – Software'),
(342, 4, 8, 14, 'Depreciation Expense'),
(343, 4, 8, 24, 'Insurance – Vehicles'),
(344, 4, 8, 23, 'Interest Expense'),
(345, 4, 8, 14, 'Meals and Entertainment'),
(346, 4, 8, 26, 'Office Supplies'),
(347, 4, 8, 27, 'Payroll – Employee Benefits'),
(348, 4, 8, 27, 'Payroll – Employer\'s Share of Benefits'),
(349, 4, 8, 27, 'Payroll – Salary & Wages'),
(350, 4, 8, 28, 'Professional Fees'),
(351, 4, 8, 21, 'Rent Expense'),
(352, 4, 8, 21, 'Repairs & Maintenance'),
(353, 4, 8, 22, 'Telephone – Wireless'),
(354, 4, 8, 14, 'Travel Expense'),
(355, 4, 8, 26, 'Utilities'),
(356, 4, 8, 31, 'Vehicle – Fuel'),
(357, 4, 8, 31, 'Vehicle – Repairs & Maintenance'),
(358, 5, 9, 15, 'Owner\'s Equity'),
(359, 4, 8, 14, 'Loss on Foreign Exchange'),
(360, 3, 6, 11, 'Gain on Foreign Exchange'),
(361, 2, 2, 7, 'Unrealized Loss on Foreign Currency'),
(362, 2, 2, 2, 'Accounts Payable'),
(363, 1, 1, 1, 'Unrealized Gain on Foreign Currency'),
(364, 1, 1, 1, 'Accounts Receivable'),
(365, 3, 6, 20, 'Sales'),
(366, 1, 1, 1, 'Cash on Hand'),
(367, 1, 1, 1, 'check account');

-- --------------------------------------------------------

--
-- Table structure for table `fin_agsubcategories`
--

CREATE TABLE `fin_agsubcategories` (
  `agsid` int(11) NOT NULL,
  `agid` int(11) NOT NULL,
  `agcid` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_agsubcategories`
--

INSERT INTO `fin_agsubcategories` (`agsid`, `agid`, `agcid`, `name`) VALUES
(1, 1, 1, 'Bank & Cash'),
(2, 2, 2, 'Current Bank Debt'),
(3, 1, 3, 'Investment'),
(4, 1, 3, 'Other Current Assets'),
(5, 1, 4, 'Long-Term Assets'),
(6, 1, 4, 'Long-Term Investments'),
(7, 2, 2, 'Current Debt'),
(8, 2, 5, 'Long-Term Debt'),
(9, 3, 6, 'Agriculture'),
(10, 3, 6, 'Commissions'),
(11, 3, 6, 'Other Income'),
(12, 4, 7, 'Cost Of Goods'),
(13, 4, 8, 'Agriculture'),
(14, 4, 8, 'Other Expenses'),
(15, 5, 9, 'Equity'),
(16, 3, 6, 'Fees & Charges'),
(17, 3, 6, 'Investments'),
(18, 3, 6, 'Non-Profit'),
(19, 3, 6, 'Professional Services'),
(20, 3, 6, 'Sales Products & Services'),
(21, 4, 8, 'Buildings & Equipment'),
(22, 4, 8, 'Computers/Communication'),
(23, 4, 8, 'Fees, Charges & Subscriptions'),
(24, 4, 8, 'Insurance'),
(25, 4, 8, 'Non-Profit'),
(26, 4, 8, 'Office'),
(27, 4, 8, 'Payroll'),
(28, 4, 8, 'Services'),
(29, 4, 8, 'Taxes'),
(30, 4, 8, 'Tools & Supplies'),
(31, 4, 8, 'Vehicle Expenses');

-- --------------------------------------------------------

--
-- Table structure for table `fin_agsubcategorycontents`
--

CREATE TABLE `fin_agsubcategorycontents` (
  `agscid` int(11) NOT NULL,
  `agsid` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `lan` varchar(5) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_agsubcategorycontents`
--

INSERT INTO `fin_agsubcategorycontents` (`agscid`, `agsid`, `name`, `lan`) VALUES
(1, 1, 'Bank & Cash', 'en'),
(12, 2, 'Current Bank Debt', 'en'),
(23, 2, 'Güncel Bankası Borç', 'tr'),
(24, 3, 'Investment', 'en'),
(25, 4, 'Other Current Assets', 'en'),
(26, 5, 'Long-Term Assets', 'en'),
(27, 6, 'Long-Term Investments', 'en'),
(28, 7, 'Current Debt', 'en'),
(29, 8, 'Long-Term Debt', 'en'),
(30, 9, 'Agriculture', 'en'),
(31, 10, 'Commissions', 'en'),
(32, 11, 'Other Income', 'en'),
(33, 12, 'Cost Of Goods', 'en'),
(34, 13, 'Agriculture', 'en'),
(35, 14, 'Other Expenses', 'en'),
(36, 15, 'Equity', 'en'),
(37, 16, 'Fees & Charges', 'en'),
(38, 17, 'Investments', 'en'),
(39, 18, 'Non-Profit', 'en'),
(40, 19, 'Professional Services', 'en'),
(41, 20, 'Sales Products & Services', 'en'),
(42, 21, 'Buildings & Equipment', 'en'),
(43, 22, 'Computers/Communication', 'en'),
(44, 23, 'Fees, Charges & Subscriptions', 'en'),
(45, 24, 'Insurance', 'en'),
(46, 25, 'Non-Profit', 'en'),
(47, 26, 'Office', 'en'),
(48, 27, 'Payroll', 'en'),
(49, 28, 'Services', 'en'),
(50, 29, 'Taxes', 'en'),
(51, 30, 'Tools & Supplies', 'en'),
(52, 31, 'Vehicle Expenses', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `fin_banks`
--

CREATE TABLE `fin_banks` (
  `bankid` int(11) NOT NULL,
  `uid` int(11) NOT NULL COMMENT 'userid',
  `bid` int(11) NOT NULL COMMENT 'business id',
  `agcid` int(11) NOT NULL COMMENT 'Account category id',
  `bankname` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `openbalance` double NOT NULL COMMENT 'Opening balance',
  `accountno` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `createddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive','Trash') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_banks`
--

INSERT INTO `fin_banks` (`bankid`, `uid`, `bid`, `agcid`, `bankname`, `openbalance`, `accountno`, `createddate`, `status`) VALUES
(2, 9, 3, 1, 'ICICI BANK', 0, '1234567', '2013-10-29 08:09:48', 'Active'),
(4, 9, 3, 1, 'CANRA BANK', 0, '3253252352', '2013-10-29 10:09:50', 'Active'),
(5, 8, 4, 1, 'ICICI', 0, '601601510869', '2013-10-29 11:01:03', 'Active'),
(6, 10, 5, 1, 'ICICI Bank', 0, '600601010003', '2013-11-08 10:44:40', 'Active'),
(7, 10, 5, 2, 'Canara Bank', 0, '47509', '2013-11-08 10:45:03', 'Active'),
(8, 8, 4, 1, 'SBI', 0, '12356', '2013-11-20 11:15:41', 'Active'),
(9, 8, 4, 1, 'Credit card', 0, '123564', '2013-11-25 11:44:14', 'Active'),
(10, 9, 3, 2, 'Check account', 0, '123456', '2013-11-29 12:41:36', 'Active'),
(11, 9, 3, 2, 'ICICI check', 0, 'fdsfsdfs', '2013-11-29 12:56:28', 'Active'),
(12, 12, 7, 1, 'icici', 0, '601601515013', '2014-03-04 06:48:23', 'Active'),
(13, 12, 7, 2, 'icici', 0, '60160151501365', '2014-03-04 07:18:46', 'Active'),
(14, 13, 8, 2, 'CRDB Plc', 0, '0150200818000', '2014-03-06 06:51:20', 'Active'),
(15, 16, 10, 1, 'icici', 0, '60160151501365', '2014-03-06 12:21:28', 'Active'),
(17, 19, 12, 1, 'icici', 0, '4579845646', '2014-03-24 12:23:09', 'Active'),
(25, 13, 8, 2, 'AKIBA Commercial Bank', 0, '101010780109', '2019-06-03 21:45:35', 'Active'),
(26, 26, 18, 2, 'ABSA', 0, '9283058491', '2019-06-04 13:49:40', 'Active'),
(27, 27, 19, 2, 'ABSA', 0, '9283058491', '2019-06-11 20:02:53', 'Active'),
(28, 29, 21, 1, 'yedy', 0, '23234234', '2020-01-06 16:23:06', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `fin_banktransfers`
--

CREATE TABLE `fin_banktransfers` (
  `btid` int(11) NOT NULL,
  `uid` int(11) NOT NULL COMMENT 'user id',
  `bid` int(11) NOT NULL COMMENT 'business id',
  `frombankid` int(11) NOT NULL COMMENT 'From bank id',
  `tobankid` int(11) NOT NULL COMMENT 'Transfer to bank id',
  `description` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `credit` double(8,2) NOT NULL,
  `debit` double(8,2) NOT NULL,
  `transferdate` date NOT NULL COMMENT 'Amount transfer date',
  `createddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive','Trash') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fin_billproducts`
--

CREATE TABLE `fin_billproducts` (
  `bpid` int(11) NOT NULL,
  `billid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `exp_cat` int(11) NOT NULL,
  `description` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double NOT NULL,
  `taxamount` double NOT NULL,
  `amount` double NOT NULL,
  `total` double NOT NULL,
  `etotal` double NOT NULL,
  `discount` float NOT NULL,
  `discounttype` enum('Percentage','Amount') COLLATE utf8_unicode_ci NOT NULL,
  `discountamt` double(8,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_billproducts`
--

INSERT INTO `fin_billproducts` (`bpid`, `billid`, `pid`, `exp_cat`, `description`, `qty`, `price`, `taxamount`, `amount`, `total`, `etotal`, `discount`, `discounttype`, `discountamt`) VALUES
(93, 29, 97, 70, '', 1, 20, 0, 0, 19.8, 0, 1, 'Percentage', 0.20),
(94, 30, 96, 65, '', 1, 10, 0, 0, 9.8, 0, 2, 'Percentage', 0.20),
(95, 30, 97, 70, '', 1, 20, 0, 0, 19.8, 0, 1, 'Percentage', 0.20),
(697, 28, 93, 11, '', 1, 500, 0, 0, 500, 1000, 0, 'Percentage', 0.00),
(698, 28, 69, 12, '', 1, 50, 0, 0, 50, 100, 0, 'Percentage', 0.00),
(699, 28, 69, 55, '', 1, 50, 0, 0, 50, 100, 0, 'Percentage', 0.00),
(700, 28, 93, 10, '', 1, 500, 0, 0, 500, 1000, 0, 'Percentage', 0.00),
(701, 27, 93, 11, '', 1, 500, 0, 0, 500, 500, 0, 'Percentage', 0.00),
(702, 27, 93, 55, '', 1, 500, 0, 0, 500, 500, 0, 'Percentage', 0.00),
(703, 27, 69, 8, '', 1, 50, 0, 0, 50, 50, 0, 'Percentage', 0.00),
(746, 43, 94, 56, '', 1, 200, 0, 0, 200, 200, 0, 'Percentage', 0.00),
(747, 43, 90, 28, '', 1, 10, 0, 0, 10, 10, 0, 'Percentage', 0.00),
(748, 43, 72, 87, 'testing', 1, 10, 0, 0, 10, 10, 0, 'Percentage', 0.00),
(749, 43, 94, 56, '', 1, 20, 0, 0, 20, 20, 0, 'Percentage', 0.00),
(750, 43, 83, 29, 'dsfasdf', 1, 32, 0, 0, 32, 32, 0, 'Percentage', 0.00),
(751, 44, 67, 31, 'mouse product', 1, 32, 0, 0, 32, 123.61792, 0, 'Percentage', 0.00),
(752, 45, 108, 104, '', 1, 45000, 0, 0, 45000, 45000, 0, 'Percentage', 0.00),
(753, 46, 112, 122, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ', 1, 1000, 0, 0, 1000, 1000, 0, 'Percentage', 0.00),
(754, 47, 115, 122, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ', 1, 10000, 0, 0, 10000, 10000, 0, 'Percentage', 0.00),
(759, 50, 123, 175, 'he digital world needs an adaptive User Experience We create itWe Prefer Long-Term App Development Relationships', 1, 100, 0, 0, 100, 100, 0, 'Percentage', 0.00),
(760, 50, 123, 175, 'he digital world needs an adaptive User Experience We create itWe Prefer Long-Term App Development Relationships', 1, 100, 0, 0, 100, 100, 0, 'Percentage', 0.00),
(761, 51, 123, 175, 'he digital world needs an adaptive User Experience We create itWe Prefer Long-Term App Development Relationships', 1, 100, 0, 0, 100, 100, 0, 'Percentage', 0.00),
(763, 52, 127, 213, 'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing softwar', 1, 350, 0, 0, 350, 156.3128, 0, 'Percentage', 0.00),
(764, 53, 130, 294, '', 1, 2000, 0, 0, 2000, 2000, 0, 'Percentage', 0.00),
(773, 58, 134, 330, 'Black label case', 1000, 300, 0, 0, 300000, 300000, 0, 'Percentage', 0.00),
(774, 59, 135, 330, 'Castle lite case', 100, 350, 0, 0, 35000, 35000, 0, 'Percentage', 0.00),
(777, 60, 137, 354, 'Black label Cases', 1000, 200, 0, 0, 200000, 200000, 0, 'Percentage', 0.00),
(778, 61, 138, 310, 'fafddaf', 1, 44, 0, 0, 44, 44, 0, 'Percentage', 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `fin_bills`
--

CREATE TABLE `fin_bills` (
  `billid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `bid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `bdate` date NOT NULL,
  `duedate` date NOT NULL,
  `psno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `billno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `currency` int(11) NOT NULL,
  `primary_currency` int(11) NOT NULL,
  `exchange_rate` float NOT NULL,
  `total` double NOT NULL,
  `subtotal` double NOT NULL,
  `exchangetotal` double NOT NULL,
  `sendpayment` float NOT NULL COMMENT 'Bill payment to vendor',
  `balance` float NOT NULL COMMENT 'Balance payment ',
  `excessamount` float NOT NULL COMMENT 'Extra payment of bill',
  `createddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `billstatus` enum('Save','Partial','Overdue','Paid') COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Active','Inactive','Trash') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_bills`
--

INSERT INTO `fin_bills` (`billid`, `uid`, `bid`, `cid`, `bdate`, `duedate`, `psno`, `billno`, `notes`, `currency`, `primary_currency`, `exchange_rate`, `total`, `subtotal`, `exchangetotal`, `sendpayment`, `balance`, `excessamount`, `createddate`, `billstatus`, `key`, `status`) VALUES
(27, 8, 4, 102, '2013-10-31', '2013-11-20', '', 'b-2', '', 41, 41, 1, 1101.15, 1050, 1101.15, -101.15, 0, 0, '2013-10-31 11:43:37', 'Overdue', 'ew09qifb', 'Active'),
(28, 8, 4, 5, '2013-10-31', '2013-11-17', '', 'b-3', '', 21, 21, 2, 1146.5, 1100, 2293, 1146.5, 0, 0, '2013-10-31 11:52:29', 'Paid', '2fa5fzv7', 'Active'),
(29, 10, 5, 115, '2013-11-08', '2013-11-08', '', 'b-1', '', 41, 41, 1, 21.78, 19.8, 21.78, 0, 0, 0, '2013-11-08 10:41:46', 'Overdue', 'hni0lu6z', 'Active'),
(30, 10, 5, 116, '2013-10-01', '2013-10-15', '', 'b-2', '', 41, 41, 1, 31.57, 29.6, 31.57, 0, 0, 0, '2013-11-08 10:42:57', 'Overdue', 'gkbhokdm', 'Active'),
(43, 9, 3, 110, '2013-11-16', '2013-11-19', '', 'b-2', '', 4, 4, 1, 315.56, 272, 315.56, 315.56, 0, 0, '2013-11-16 06:08:44', 'Paid', 'aeeygimd', 'Active'),
(44, 9, 3, 109, '2013-11-19', '2013-11-21', '', 'b-21', '', 51, 51, 3.86306, 32.96, 32, 127.3264576, 32.96, 0, 0, '2013-11-19 06:01:32', 'Paid', 'tma2g0f7', 'Active'),
(45, 11, 6, 137, '2014-01-13', '2014-01-13', '', 'b-1', 'description', 41, 41, 1, 45000, 45000, 45000, 45000, 0, 0, '2014-01-13 12:04:42', 'Paid', '9je26pgw', 'Active'),
(46, 12, 7, 140, '2014-03-04', '2014-03-19', '', 'b-1', '', 41, 41, 1, 1000, 1000, 1000, 0, 0, 0, '2014-03-04 07:15:14', 'Overdue', 'ma63keem', 'Active'),
(47, 12, 7, 140, '2014-03-04', '2014-03-19', '', 'b-2', '', 41, 41, 1, 10000, 10000, 10000, 0, 0, 0, '2014-03-04 07:15:24', 'Overdue', 'toe9jbwz', 'Active'),
(50, 16, 10, 145, '2014-03-06', '2014-03-06', '', 'b-1', 'he digital world needs an adaptive User Experience We create it<br />\r\nWe Prefer Long-Term App Development Relationships<br />\r\n', 108, 108, 1, 220, 200, 220, 220, 0, 0, '2014-03-06 12:19:09', 'Paid', 'kild8hqq', 'Active'),
(51, 16, 10, 145, '2014-03-07', '2014-03-07', '', 'b-2', '', 103, 108, 1, 100, 100, 100, 80, 0, 0, '2014-03-07 05:44:07', 'Overdue', 'a6tmpmeg', 'Active'),
(52, 19, 12, 151, '2014-03-24', '2014-03-24', '', 'b-1', '', 102, 102, 0.446608, 385, 350, 171.94408, 0, 0, 0, '2014-03-24 12:20:19', 'Overdue', 'rcsqlpii', 'Active'),
(53, 24, 17, 154, '2016-08-20', '2016-08-20', '', 'b-1', '<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 41, 41, 1, 2000, 2000, 2000, 0, 0, 0, '2016-08-20 10:14:08', 'Overdue', 'bq695uo3', 'Active'),
(58, 26, 18, 161, '2019-06-04', '2019-06-19', '', 'b-1', '', 117, 117, 1, 345000, 300000, 345000, 345000, 0, 0, '2019-06-04 18:01:26', 'Paid', 'zla3t296', 'Active'),
(59, 26, 18, 161, '2019-06-04', '2019-06-19', '', 'b-2', '', 117, 117, 1, 40250, 35000, 40250, 0, 0, 0, '2019-06-04 18:04:36', 'Overdue', 'mjmsmnsh', 'Active'),
(60, 27, 19, 162, '2019-06-11', '2019-06-11', '', 'b-1', '', 117, 117, 1, 230000, 200000, 230000, 230000, 0, 0, '2019-06-11 20:05:07', 'Overdue', 'gr8yh96o', 'Active'),
(61, 13, 8, 157, '2020-04-03', '2020-04-18', '', 'b-1', '', 105, 108, 1, 44, 44, 44, 44, 0, 0, '2020-04-03 10:15:04', 'Paid', 'jfez3m68', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `fin_billtaxes`
--

CREATE TABLE `fin_billtaxes` (
  `btid` int(11) NOT NULL,
  `billid` int(11) NOT NULL,
  `bpid` int(11) NOT NULL,
  `exp_cat` int(11) NOT NULL,
  `tamt` double NOT NULL COMMENT 'percentage or amount',
  `taxid` int(11) NOT NULL,
  `taxamount` double NOT NULL,
  `taxtype` enum('Percentage','Amount') COLLATE utf8_unicode_ci NOT NULL,
  `currency` int(11) NOT NULL,
  `primarycurrency` int(11) NOT NULL,
  `exchange_rate` double NOT NULL,
  `totalamount` double NOT NULL,
  `amount` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_billtaxes`
--

INSERT INTO `fin_billtaxes` (`btid`, `billid`, `bpid`, `exp_cat`, `tamt`, `taxid`, `taxamount`, `taxtype`, `currency`, `primarycurrency`, `exchange_rate`, `totalamount`, `amount`) VALUES
(144, 28, 700, 10, 5, 16, 25, 'Percentage', 21, 41, 2, 50, 0),
(145, 28, 700, 10, 4.3, 20, 21.5, 'Percentage', 21, 41, 2, 43, 0),
(146, 27, 702, 55, 5, 16, 25, 'Percentage', 41, 41, 1, 25, 0),
(147, 27, 702, 55, 4.3, 20, 21.5, 'Percentage', 41, 41, 1, 21.5, 0),
(148, 27, 703, 8, 5, 16, 2.5, 'Percentage', 41, 41, 1, 2.5, 0),
(149, 27, 703, 8, 4.3, 20, 2.15, 'Percentage', 41, 41, 1, 2.15, 0),
(234, 43, 746, 56, 12, 13, 24, 'Percentage', 4, 4, 1, 24, 0),
(235, 43, 746, 56, 3, 19, 6, 'Percentage', 4, 4, 1, 6, 0),
(236, 43, 747, 28, 12, 13, 1.2, 'Percentage', 4, 4, 1, 1.2, 0),
(237, 43, 747, 28, 10, 14, 1, 'Percentage', 4, 4, 1, 1, 0),
(238, 43, 748, 87, 12, 13, 1.2, 'Percentage', 4, 4, 1, 1.2, 0),
(239, 43, 748, 87, 10, 14, 1, 'Percentage', 4, 4, 1, 1, 0),
(240, 43, 749, 56, 12, 13, 2.4, 'Percentage', 4, 4, 1, 2.4, 0),
(241, 43, 749, 56, 10, 14, 2, 'Percentage', 4, 4, 1, 2, 0),
(242, 43, 749, 56, 3, 19, 0.6, 'Percentage', 4, 4, 1, 0.6, 0),
(243, 43, 750, 29, 10, 14, 3.2, 'Percentage', 4, 4, 1, 3.2, 0),
(244, 43, 750, 29, 3, 19, 0.96, 'Percentage', 4, 4, 1, 0.96, 0),
(245, 44, 751, 31, 3, 19, 0.96, 'Percentage', 51, 4, 3.86306, 3.7085376, 0),
(249, 50, 759, 175, 10, 23, 10, 'Percentage', 108, 108, 1, 10, 0),
(250, 50, 760, 175, 10, 23, 10, 'Percentage', 108, 108, 1, 10, 0),
(252, 52, 763, 213, 10, 25, 35, 'Percentage', 102, 108, 0.446608, 15.63128, 0),
(254, 58, 773, 330, 15, 27, 45000, 'Percentage', 117, 117, 1, 45000, 0),
(255, 59, 774, 330, 15, 27, 5250, 'Percentage', 117, 117, 1, 5250, 0),
(258, 60, 777, 354, 15, 28, 30000, 'Percentage', 117, 117, 1, 30000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fin_blogcates`
--

CREATE TABLE `fin_blogcates` (
  `cid` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `createdate` date NOT NULL,
  `modifydate` date NOT NULL,
  `status` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fin_blogs`
--

CREATE TABLE `fin_blogs` (
  `bid` int(11) NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `cid` int(11) NOT NULL COMMENT 'category id',
  `author` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `video_url` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `createdate` date NOT NULL,
  `modifydate` date NOT NULL,
  `status` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fin_businesses`
--

CREATE TABLE `fin_businesses` (
  `bid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `company` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `businesstype` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `country` int(11) NOT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `currency` int(11) NOT NULL,
  `dateformat` int(11) NOT NULL,
  `financialyear` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `bus_startdate` date NOT NULL,
  `createddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive','Trash') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_businesses`
--

INSERT INTO `fin_businesses` (`bid`, `uid`, `company`, `businesstype`, `description`, `country`, `city`, `address`, `currency`, `dateformat`, `financialyear`, `bus_startdate`, `createddate`, `status`) VALUES
(3, 9, 'KD & Co.,', 1, 'test', 97, 'Madurai', 'test', 4, 1, '12-31', '2004-02-29', '2013-10-02 12:26:07', 'Active'),
(4, 8, 'Muthu Veera  Pandi', 1, 'My company in india...........', 97, 'Madurai', 'Madurai\r\nTamilnadu', 41, 1, '11-30', '2013-06-01', '2013-10-02 12:35:43', 'Active'),
(5, 10, 'Shunmugam', 1, 'My company in india...........', 1, 'Madurai', 'sfd asf dfd saffsdfsf sf', 41, 2, '12-31', '2013-10-02', '2013-10-03 05:07:23', 'Active'),
(6, 11, 'business name', 5, 'test', 97, 'madurai', 'Give your address here', 41, 1, '12-31', '2014-01-13', '2014-01-13 11:32:49', 'Active'),
(7, 12, 'super', 1, 'Bangalore Karnataka india\r\n', 97, 'Bangalore', 'Bangalore Karnataka india\r\n\r\n', 41, 1, '12-31', '2014-03-04', '2014-03-04 06:22:19', 'Active'),
(8, 13, 'Motorspartz.com', 3, 'Butwal - 12, Rupandhi', 143, 'Butwal', 'sample adress', 108, 1, '12-19', '2019-07-17', '2014-03-06 04:30:50', 'Active'),
(9, 15, 'fd', 5, 'sdfgsdfg', 3, 'dfsg', 'sdfgsfddsfgdfs sd sdfgsdf', 108, 1, '12-31', '2014-02-04', '2014-03-06 10:59:28', 'Active'),
(10, 16, 'Web Design & development', 3, 'he digital world needs an adaptive User Experience We create it\r\nWe Prefer Long-Term App Development Relationships\r\n', 1, 'California', '3951 Westerre Parkway, Suite 350\r\nRichmond, VA 23233\r\nUSA', 108, 2, '12-31', '2013-01-06', '2014-03-06 12:07:47', 'Active'),
(11, 17, 'Web services', 1, 'MARY ROE\r\n   MEGASYSTEMS INC\r\n   SUITE 5A-1204\r\n   799 E DRAGRAM\r\n   TUCSON AZ 85705\r\n   USA\r\n   ', 1, 'TUCSON ', 'JOHN \"GULLIBLE\" DOE\r\n   CENTER FOR FINANCIAL ASSISTANCE TO DEPOSED NIGERIAN ROYALTY\r\n   421 E DRACHMAN\r\n   TUCSON AZ 85705-7598\r\n   USA\r\n   ', 108, 1, '12-31', '2014-03-01', '2014-03-24 06:49:35', 'Active'),
(12, 19, 'Web services', 1, 'MARY ROE\r\n   MEGASYSTEMS INC\r\n   SUITE 5A-1204\r\n   799 E DRAGRAM\r\n   TUCSON AZ 85705\r\n   USA', 1, 'TUCSON', 'JOHN \"GULLIBLE\" DOECENTER FOR FINANCIAL ASSISTANCE TO DEPOSED \r\n\r\nNIGERIAN ROYALTY\r\n   421 E DRACHMAN\r\n   TUCSON AZ 85705-7598\r\n   USA', 108, 1, '12-31', '2014-03-01', '2014-03-24 09:36:40', 'Active'),
(13, 20, 'Lynchpin Technologies', 3, 'Lynchpin is an IT Company', 97, 'Madurai', '2, Karthick Raja Complex,\r\nIDBI Bank Upstairs,\r\nMadurai - 625009', 41, 7, '12-31', '2013-12-06', '2015-05-22 08:22:12', 'Active'),
(14, 21, 'COTTON MILL', 3, 'COTTON MILL which purchases cotton from farmers and process cotton.', 97, 'Bangalore', 'F 302, Punyaboomi layout, kalkere.', 41, 1, '12-31', '2014-07-16', '2015-06-28 09:16:20', 'Active'),
(15, 22, 'sai srinivasa ginning mill', 5, 'It is a Cotton Ginning mill', 97, 'hyderabad', '4-1-77/1, 503M, Venkatswara Heights, Nacharam. Hyderabad', 41, 1, '12-31', '2010-12-25', '2015-06-30 05:12:07', 'Active'),
(16, 23, 'Cotton Ginning', 5, '', 97, 'warangal', 'jangaon,warangal.', 41, 1, '12-31', '2015-04-01', '2015-07-04 03:54:05', 'Active'),
(17, 24, 'Naga solution', 1, 'Creative company', 97, 'Paramakudi', 'Test Address', 41, 1, '12-31', '2016-08-20', '2016-08-20 06:38:13', 'Active'),
(18, 26, 'think mybiz', 3, 'We are the best', 188, 'mokopane', 'No 48 Rabe street\r\nMokopane\r\n0600', 117, 7, '12-31', '2019-03-01', '2019-06-04 13:42:07', 'Active'),
(19, 27, 'My biz 34', 3, 'My owner', 188, 'mokopane', '48 Rabe Street', 117, 7, '02-29', '2019-03-01', '2019-06-11 06:52:22', 'Active'),
(20, 28, 'Medicotantra', 3, '', 97, 'Surat', 'capitalist, surat', 41, 1, '12-31', '2018-12-02', '2019-12-27 06:30:07', 'Active'),
(21, 29, 'Test Company', 1, 'test', 156, 'Lhr', 'lhtasdasasdasd', 80, 5, '12-31', '2020-01-01', '2020-01-06 16:21:33', 'Active'),
(22, 31, 'Testing', 5, 'wewew wewe wewe', 1, 'Nelka', 'laaa wewe wewe ', 1, 1, '12-31', '2020-02-13', '2020-02-09 05:35:03', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `fin_businesstypecontents`
--

CREATE TABLE `fin_businesstypecontents` (
  `btcid` int(11) NOT NULL,
  `btid` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `lan` varchar(5) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_businesstypecontents`
--

INSERT INTO `fin_businesstypecontents` (`btcid`, `btid`, `name`, `lan`) VALUES
(1, 1, 'Artists, Photographers & Creative Types', 'en'),
(2, 1, 'Sanatçılar, Fotoğrafçılar ve Yaratıcı Türleri', 'tr'),
(3, 2, 'Consultants & Professionals', 'en'),
(4, 2, 'Danışmanlar ve Uzmanları', 'tr'),
(5, 3, 'Test1', 'en'),
(6, 4, 'Test2', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `fin_businesstypes`
--

CREATE TABLE `fin_businesstypes` (
  `btid` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Active','Inactive','Trash') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_businesstypes`
--

INSERT INTO `fin_businesstypes` (`btid`, `name`, `status`) VALUES
(1, 'Artists, Photographers & Creative Types', 'Active'),
(2, 'Consultants & Professionals', 'Active'),
(3, 'Test1', 'Active'),
(4, 'Test2', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `fin_checks`
--

CREATE TABLE `fin_checks` (
  `cid` int(11) NOT NULL,
  `chk_no` int(11) NOT NULL,
  `due_date` date NOT NULL,
  `recorddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bank` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `currency` int(11) NOT NULL,
  `city` int(11) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fin_collaberaters`
--

CREATE TABLE `fin_collaberaters` (
  `cid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `bid` int(11) NOT NULL,
  `ctype` enum('Admin','User') COLLATE utf8_unicode_ci NOT NULL,
  `view` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL,
  `edit` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL,
  `createddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive','Trash') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_collaberaters`
--

INSERT INTO `fin_collaberaters` (`cid`, `uid`, `bid`, `ctype`, `view`, `edit`, `createddate`, `status`) VALUES
(2, 9, 4, 'User', 'Yes', 'No', '2013-10-24 12:52:33', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `fin_countries`
--

CREATE TABLE `fin_countries` (
  `cid` int(11) NOT NULL,
  `country` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isocountry` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_countries`
--

INSERT INTO `fin_countries` (`cid`, `country`, `isocountry`) VALUES
(1, 'United States ', 'US'),
(2, 'Canada ', 'CA'),
(3, 'United Kingdom ', 'UK'),
(5, 'Afghanistan ', 'AF'),
(6, 'Albania ', 'AL'),
(7, 'Algeria ', 'AG'),
(8, 'Andorra ', 'AN'),
(9, 'Angola ', 'AO'),
(10, 'Anguilla ', 'AV'),
(11, 'Antigua and Barbuda ', 'AC'),
(12, 'Argentina ', 'AR'),
(13, 'Armenia ', 'AM'),
(14, 'Aruba ', 'AA'),
(15, 'Australia ', 'AS'),
(16, 'Austria ', 'AU'),
(17, 'Azerbaijan ', 'AJ'),
(18, 'Bahamas ', 'BF'),
(19, 'Bahrain ', 'BA'),
(20, 'Bangladesh ', 'BG'),
(21, 'Barbados ', 'BB'),
(22, 'Belarus ', 'BO'),
(23, 'Belgium ', 'BE'),
(24, 'Belize ', 'BH'),
(25, 'Benin ', 'BN'),
(26, 'Bermuda ', 'BD'),
(27, 'Bhutan ', 'BT'),
(28, 'Bolivia ', 'BL'),
(29, 'Bosnia and Herzegovina ', 'BK'),
(30, 'Botswana ', 'BC'),
(31, 'Brazil ', 'BR'),
(32, 'British Virgin Islands ', 'VI'),
(33, 'Brunei ', 'BX'),
(34, 'Bulgaria ', 'BU'),
(35, 'Burkina Faso ', 'UV'),
(36, 'Burundi ', 'BY'),
(37, 'Cambodia ', 'CB'),
(38, 'Cameroon ', 'CM'),
(39, 'Cape Verde ', 'CV'),
(40, 'Cayman Islands ', 'CJ'),
(41, 'Central African Republic ', 'CT'),
(42, 'Chad ', 'CD'),
(43, 'Chile ', 'CI'),
(44, 'China ', 'CH'),
(45, 'Christmas Island ', 'KT'),
(46, 'Cocos (Keeling) Islands ', 'CK'),
(47, 'Colombia ', 'CO'),
(48, 'Comoros ', 'CN'),
(49, 'Congo ', 'CF'),
(50, 'Congo, Democratic Republic ', 'CG'),
(51, 'Cook Islands ', 'CW'),
(52, 'Costa Rica ', 'CS'),
(53, 'Cote D\'Ivoire ', 'IV'),
(54, 'Croatia ', 'HR'),
(55, 'Cuba ', 'CU'),
(56, 'Cyprus ', 'CY'),
(57, 'Czech Republic ', 'EZ'),
(58, 'Denmark ', 'DA'),
(59, 'Djibouti ', 'DJ'),
(60, 'Dominica ', 'DO'),
(61, 'Dominican Republic ', 'DR'),
(62, 'Ecuador ', 'EC'),
(63, 'Egypt ', 'EG'),
(64, 'El Salvador ', 'ES'),
(65, 'Equatorial Guinea ', 'EK'),
(66, 'Eritrea ', 'ER'),
(67, 'Estonia ', 'EN'),
(68, 'Ethiopia ', 'ET'),
(69, 'Falkland Islands ', 'FK'),
(70, 'Faroe Islands ', 'FO'),
(71, 'Fiji ', 'FJ'),
(72, 'Finland ', 'FI'),
(73, 'France ', 'FR'),
(74, 'French Guiana ', 'FG'),
(75, 'French Polynesia ', 'FP'),
(76, 'French S. & Antarctic Lands ', 'FS'),
(77, 'Gabon ', 'GB'),
(78, 'Gambia, The ', 'GA'),
(79, 'Gaza Strip ', 'GZ'),
(80, 'Georgia ', 'GG'),
(81, 'Germany ', 'GM'),
(82, 'Ghana ', 'GH'),
(83, 'Gibraltar ', 'GI'),
(84, 'Greece ', 'GR'),
(85, 'Greenland ', 'GL'),
(86, 'Grenada ', 'GJ'),
(87, 'Guadeloupe ', 'GP'),
(88, 'Guatemala ', 'GT'),
(89, 'Guinea ', 'GV'),
(90, 'Guinea-Bissau ', 'PU'),
(91, 'Guyana ', 'GY'),
(92, 'Haiti ', 'HA'),
(93, 'Honduras ', 'HO'),
(94, 'Hong Kong ', 'HK'),
(95, 'Hungary ', 'HU'),
(96, 'Iceland ', 'IC'),
(97, 'India ', 'IN'),
(98, 'Indonesia ', 'ID'),
(99, 'Iran ', 'IR'),
(100, 'Iraq ', 'IZ'),
(101, 'Ireland ', 'EI'),
(102, 'Israel ', 'IS'),
(103, 'Italy ', 'IT'),
(104, 'Jamaica ', 'JM'),
(105, 'Japan ', 'JA'),
(106, 'Jordan ', 'JO'),
(107, 'Kazakhstan ', 'KZ'),
(108, 'Kenya ', 'KE'),
(109, 'Kiribati ', 'KR'),
(110, 'Kuwait ', 'KU'),
(111, 'Kyrgyzstan ', 'KG'),
(112, 'Laos ', 'LA'),
(113, 'Latvia ', 'LG'),
(114, 'Lebanon ', 'LE'),
(115, 'Lesotho ', 'LT'),
(116, 'Liberia ', 'LI'),
(117, 'Liechtenstein ', 'LS'),
(118, 'Lithuania ', 'LH'),
(119, 'Luxembourg ', 'LU'),
(120, 'Macau ', 'MC'),
(121, 'Macedonia ', 'MK'),
(122, 'Madagascar ', 'MA'),
(123, 'Malawi ', 'MI'),
(124, 'Malaysia ', 'MY'),
(125, 'Maldives ', 'MV'),
(126, 'Mali ', 'ML'),
(127, 'Malta ', 'MT'),
(128, 'Marshall Islands ', 'RM'),
(129, 'Martinique ', 'MB'),
(130, 'Mauritania ', 'MR'),
(131, 'Mauritius ', 'MP'),
(132, 'Mayotte ', 'MF'),
(133, 'Mexico ', 'MX'),
(134, 'Micronesia, Fed. States ', 'FM'),
(135, 'Moldova ', 'MD'),
(136, 'Monaco ', 'MN'),
(137, 'Mongolia ', 'MG'),
(138, 'Montserrat ', 'MH'),
(139, 'Morocco ', 'MO'),
(140, 'Mozambique ', 'MZ'),
(141, 'Namibia ', 'WA'),
(142, 'Nauru ', 'NR'),
(143, 'Nepal ', 'NP'),
(144, 'Netherlands ', 'NL'),
(145, 'Netherlands Antilles ', 'NT'),
(146, 'New Caledonia ', 'NC'),
(147, 'New Zealand ', 'NZ'),
(148, 'Nicaragua ', 'NU'),
(149, 'Niger ', 'NG'),
(150, 'Nigeria ', 'NI'),
(151, 'Niue ', 'NE'),
(152, 'Norfolk Island ', 'NF'),
(153, 'North Korea ', 'KN'),
(154, 'Norway ', 'NO'),
(155, 'Oman ', 'MU'),
(156, 'Pakistan ', 'PK'),
(157, 'Palau ', 'PS'),
(158, 'Panama ', 'PM'),
(159, 'Papua New Guinea ', 'PP'),
(160, 'Paraguay ', 'PA'),
(161, 'Peru ', 'PE'),
(162, 'Philippines ', 'RP'),
(163, 'Pitcairn Islands ', 'PC'),
(164, 'Poland ', 'PL'),
(165, 'Portugal ', 'PO'),
(166, 'Puerto Rico ', 'PR'),
(167, 'Qatar ', 'QA'),
(168, 'Reunion ', 'RE'),
(169, 'Romania ', 'RO'),
(170, 'Russia ', 'RS'),
(171, 'Rwanda ', 'RW'),
(172, 'S. Georgia & Sandwich Islands ', 'SX'),
(173, 'Saint Kitts and Nevis ', 'SC'),
(174, 'Saint Lucia ', 'ST'),
(175, 'Samoa ', 'WS'),
(176, 'San Marino ', 'SM'),
(177, 'Sao Tome and Principe ', 'TP'),
(178, 'Saudi Arabia ', 'SA'),
(179, 'Senegal ', 'SG'),
(180, 'Serbia ', 'SR'),
(181, 'Seychelles ', 'SE'),
(182, 'Sierra Leone ', 'SL'),
(183, 'Singapore ', 'SN'),
(184, 'Slovakia ', 'LO'),
(185, 'Slovenia ', 'SI'),
(186, 'Solomon Islands ', 'BP'),
(187, 'Somalia ', 'SO'),
(188, 'South Africa ', 'SF'),
(189, 'South Korea ', 'KS'),
(190, 'Spain ', 'SP'),
(191, 'Sri Lanka ', 'CE'),
(192, 'St. Vincent & The Grenadines ', 'VC'),
(193, 'Sudan ', 'SU'),
(194, 'Suriname ', 'NS'),
(195, 'Svalbard ', 'SV'),
(196, 'Swaziland ', 'WZ'),
(197, 'Sweden ', 'SW'),
(198, 'Switzerland ', 'SZ'),
(199, 'Syria ', 'SY'),
(200, 'Taiwan ', 'TW'),
(201, 'Tajikistan ', 'TI'),
(202, 'Tanzania ', 'TZ'),
(203, 'Thailand ', 'TH'),
(204, 'Togo ', 'TO'),
(205, 'Tokelau ', 'TL'),
(206, 'Tonga ', 'TN'),
(207, 'Trinidad and Tobago ', 'TD'),
(208, 'Tunisia ', 'TS'),
(209, 'Turkey ', 'TU'),
(210, 'Turkmenistan ', 'TX'),
(211, 'Turks and Caicos Islands ', 'TK'),
(212, 'Tuvalu ', 'TV'),
(213, 'Uganda ', 'UG'),
(214, 'Ukraine ', 'UP'),
(215, 'United Arab Emirates ', 'AE'),
(216, 'Uruguay ', 'UY'),
(217, 'Uzbekistan ', 'UZ'),
(218, 'Vanuatu ', 'NH'),
(219, 'Vatican City ', 'VT'),
(220, 'Venezuela ', 'VE'),
(221, 'Vietnam ', 'VM'),
(222, 'Virgin Islands ', 'VQ'),
(223, 'Wallis and Futuna ', 'WF'),
(224, 'Western Sahara ', 'WI'),
(225, 'Yemen ', 'YM'),
(226, 'Zambia ', 'ZA'),
(227, 'Zimbabwe ', 'ZI'),
(228, 'Montenegro ', 'MO');

-- --------------------------------------------------------

--
-- Table structure for table `fin_currencies`
--

CREATE TABLE `fin_currencies` (
  `cid` int(11) NOT NULL,
  `currency` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `symbol` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Active','Inactive','Trash') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_currencies`
--

INSERT INTO `fin_currencies` (`cid`, `currency`, `code`, `symbol`, `status`) VALUES
(1, 'UAE dirham', 'AED', 'AED', 'Active'),
(2, 'Netherlands Antillian Guilder', 'ANG', '&fnof;', 'Active'),
(3, 'Argentine peso', 'ARS', '$', 'Active'),
(4, 'Australian dollar', 'AUD', '$', 'Active'),
(9, 'Bulgarian lev', 'BGN', 'BGN', 'Active'),
(10, 'Bahraini dinar', 'BHD', 'BHD', 'Active'),
(11, 'Bolivian boliviano', 'BOB', 'BOB', 'Active'),
(12, 'Brazilian real', 'BRL', 'R$', 'Active'),
(14, 'Botswana pula', 'BWP', 'P', 'Active'),
(16, 'Belize dollar', 'BZD', 'BZ$', 'Active'),
(17, 'Canadian dollar', 'CAD', '$', 'Active'),
(18, 'Swiss franc', 'CHF', 'CHF', 'Active'),
(19, 'Chilean peso', 'CLP', '$', 'Active'),
(20, 'Chinese yuan', 'CNY', '&yen;', 'Active'),
(21, 'Colombian peso', 'COP', '$', 'Active'),
(22, 'Costa Rican colon', 'CRC', 'CRC', 'Active'),
(23, 'Czech koruna', 'CZK', 'CZK', 'Active'),
(24, 'Danish Krone', 'DKK', 'kr', 'Active'),
(25, 'Dominican peso', 'DOP', 'RD$', 'Active'),
(26, 'Algerian dinar', 'DZD', 'د.ج', 'Active'),
(27, 'Estonian kroon', 'EEK', 'kr', 'Active'),
(28, 'Egyptian pound', 'EGP', '&euro;', 'Active'),
(29, 'Euro', 'EUR', '&euro;', 'Active'),
(30, 'Fiji dollar', 'FJD', '$', 'Active'),
(31, 'British pound', 'GBP', '&euro;', 'Active'),
(35, 'Hong Kong dollar', 'HKD', '$', 'Active'),
(36, 'Honduras lempira', 'HNL', 'L', 'Active'),
(37, 'Croatian kuna', 'HRK', 'kn', 'Active'),
(38, 'Hungarian forint', 'HUF', 'Ft', 'Active'),
(39, 'Indonesian rupiah', 'IDR', 'Rp', 'Active'),
(40, 'New Israeli shekel', 'ILS', 'ILS', 'Active'),
(41, 'Indian rupee', 'INR', 'र', 'Active'),
(45, 'Jamaican dollar', 'JMD', 'J$', 'Active'),
(46, 'Jordanian dinar', 'JOD', 'JOD', 'Active'),
(47, 'Japanese yen', 'JPY', '&yen;', 'Active'),
(48, 'Kenyan shilling', 'KES', 'KES', 'Active'),
(50, 'South Korean won', 'KRW', 'KRW', 'Active'),
(51, 'Kuwaiti dinar', 'KWD', 'KWD', 'Active'),
(52, 'Kazakh tenge', 'KZT', 'KZT', 'Active'),
(53, 'Lebanese pound', 'LBP', '&euro;', 'Active'),
(54, 'Sri Lanka rupee', 'LKR', 'LKR', 'Active'),
(56, 'Lithuanian litas', 'LTL', 'LTL', 'Active'),
(57, 'Latvian lats', 'LVL', 'Ls', 'Active'),
(58, 'Moroccan dirham', 'MAD', 'MAD', 'Active'),
(59, 'Moldovan leu', 'MDL', 'MDL', 'Active'),
(61, 'Macedonian denar', 'MKD', 'MKD', 'Active'),
(64, 'Mauritius rupee', 'MUR', 'Rs', 'Active'),
(67, 'Mexican peso', 'MXN', '$', 'Active'),
(68, 'Malaysian ringgit', 'MYR', 'RM', 'Active'),
(70, 'Namibian dollar', 'NAD', '$', 'Active'),
(71, 'Nigerian naira', 'NGN', 'NGN', 'Active'),
(72, 'Nicaragua cordoba', 'NIO', 'C$kr', 'Active'),
(73, 'Norwegian krone', 'NOK', 'kr', 'Active'),
(74, 'Nepalese rupee', 'NPR', 'Rs', 'Active'),
(75, 'New Zealand dollar', 'NZD', '$', 'Active'),
(76, 'Omani rial', 'OMR', 'OMR', 'Active'),
(77, 'Peruvian nuevo sol', 'PEN', 'PEN', 'Active'),
(78, 'Papua New Guinean kina', 'PGK', 'PGK', 'Active'),
(79, 'Philippine peso', 'PHP', 'PHP', 'Active'),
(80, 'Pakistan rupee', 'PKR', 'Rs', 'Active'),
(81, 'Polish zloty', 'PLN', 'PLN', 'Active'),
(82, 'Paraguayan guarani', 'PYG', 'Gs', 'Active'),
(83, 'Qatari rial', 'QAR', 'QAR', 'Active'),
(84, 'Romanian lei', 'RON', 'RON', 'Active'),
(85, 'Serbian dinar', 'RSD', 'RSD', 'Active'),
(86, 'Russian ruble', 'RUB', 'RUB', 'Active'),
(88, 'Saudi riyal', 'SAR', 'SAR', 'Active'),
(90, 'Seychelles rupee', 'SCR', 'Rs', 'Active'),
(91, 'Swedish krona', 'SEK', 'kr', 'Active'),
(92, 'Singapore dollar', 'SGD', '$', 'Active'),
(94, 'Slovak koruna', 'SKK', 'SKK', 'Active'),
(95, 'Sierra Leonean leone', 'SLL', 'SLL', 'Active'),
(97, 'Salvadoran colon', 'SVC', '$', 'Active'),
(99, 'Thai baht', 'THB', 'THB', 'Active'),
(100, 'Tunisian dinar', 'TND', 'TND', 'Active'),
(102, 'Turkish lira', 'TRY', 'TL', 'Active'),
(103, 'Trinidad dollar', 'TTD', 'TT$', 'Active'),
(104, 'Taiwan dollar', 'TWD', 'NT$', 'Active'),
(105, 'Tanzanian shilling', 'TZS', 'TZS', 'Active'),
(106, 'Ukrainian grivna', 'UAH', 'UAH', 'Active'),
(107, 'Uganda shilling', 'UGX', 'UGX', 'Active'),
(108, 'United States dollar', 'USD', '$', 'Active'),
(109, 'Uruguayo peso', 'UYU', '$U', 'Active'),
(110, 'Vietnamese dong', 'VND', 'VND', 'Active'),
(116, 'Yemeni rial', 'YER', 'YER', 'Active'),
(117, 'South African rand', 'ZAR', 'ZAR', 'Active'),
(118, 'Zambia kwacha', 'ZMK', 'ZMK', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `fin_customers`
--

CREATE TABLE `fin_customers` (
  `cid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `bid` int(11) NOT NULL,
  `ctype` enum('Customer','Vendor') COLLATE utf8_unicode_ci NOT NULL COMMENT 'customer type',
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `currency` int(11) NOT NULL,
  `phone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `address1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `zip` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive','Trash') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_customers`
--

INSERT INTO `fin_customers` (`cid`, `uid`, `bid`, `ctype`, `name`, `email`, `firstname`, `lastname`, `currency`, `phone`, `mobile`, `address1`, `address2`, `city`, `state`, `country`, `zip`, `createdate`, `status`) VALUES
(1, 8, 4, 'Customer', 'mani', '', 'Mani', 'kandan', 108, '', '', '', '', '', '', '97', '', '2013-10-03 10:55:18', 'Active'),
(2, 8, 4, 'Customer', 'sheik', '', '', '', 41, '', '', '', '', '', '', '97', '', '2013-10-03 11:42:11', 'Trash'),
(3, 9, 3, 'Customer', 'sdfasdfs', '', '', '', 41, '', '', '', '', '', '', '97', '', '2013-10-03 11:58:52', 'Trash'),
(5, 8, 4, 'Vendor', 'RS Water', '', '', '', 41, '', '', '', '', '', '', '97', '', '2013-10-03 12:32:36', 'Active'),
(6, 9, 3, 'Customer', 'test', '', '', '', 92, '', '', '', '', '', '', '97', '', '2013-10-04 09:49:20', 'Trash'),
(7, 9, 3, 'Customer', 'tertererer', '', '', '', 108, '', '', '', '', '', '', '97', '', '2013-10-04 09:49:29', 'Trash'),
(8, 8, 4, 'Customer', 'Ganesh', '', '', '', 41, '', '', '', '', '', '', '97', '', '2013-10-04 13:27:31', 'Active'),
(9, 9, 3, 'Customer', 'shun', '', 'shumugam', 'mugam', 41, '', '', '', '', '', '', '', '', '2013-10-07 09:31:12', 'Trash'),
(10, 9, 3, 'Customer', 'shun', '', 'shumugam', 'mugam', 108, '', '', '', '', '', '', '1', '', '2013-10-07 09:32:56', 'Active'),
(73, 9, 3, 'Customer', 'prabh', '', 'prabha', 'karan', 41, '98764533225', '3252353253252355', 'test', 'test', 'madurai', 'tamilnadu', '97', '', '2013-10-07 11:33:04', 'Active'),
(97, 9, 3, 'Customer', 'prabhakaran', '', 'prabha', 'karan', 108, '', '', '', '', '', '', '1', '', '2013-10-07 13:03:28', 'Active'),
(98, 9, 3, 'Customer', 'test', '', '', '', 26, '', '', '', '', '', '', '1', '', '2013-10-07 13:04:52', 'Active'),
(99, 9, 3, 'Customer', 'shun', '', '', '', 1, '', '', '', '', '', '', '1', '', '2013-10-07 13:08:04', 'Trash'),
(100, 9, 3, 'Customer', 'Google', '', '', '', 108, '', '', '', '', '', '', '1', '', '2013-10-07 13:10:39', 'Trash'),
(101, 9, 3, 'Customer', 'test', '', '', '', 1, '', '', '', '', '', '', '1', '', '2013-10-08 07:40:04', 'Trash'),
(102, 8, 4, 'Vendor', 'Pcworld', '', '', '', 108, '', '', '', '', '', '', '97', '', '2013-10-09 10:53:46', 'Active'),
(103, 8, 4, 'Customer', 'Pcworld', '', '', '', 1, '', '', '', '', '', '', '1', '', '2013-10-09 11:21:43', 'Active'),
(104, 8, 4, 'Vendor', 'Priya computers', '', '', '', 1, '', '', '', '', '', '', '1', '', '2013-10-09 11:24:44', 'Active'),
(105, 9, 3, 'Vendor', 'shun', '', '', '', 1, '', '', '', '', '', '', '1', '', '2013-10-10 05:31:18', 'Trash'),
(106, 9, 3, 'Customer', 'musthafa', '', '', '', 1, '', '', '', '', '', '', '1', '', '2013-10-10 05:38:58', 'Trash'),
(107, 9, 3, 'Customer', 'demo', '', '', '', 4, '', '', '', '', '', '', '1', '', '2013-10-10 05:42:55', 'Trash'),
(108, 9, 3, 'Customer', 'shunds fds fds fs', '', '', '', 1, '', '', '', '', '', '', '1', '', '2013-10-10 05:44:32', 'Trash'),
(109, 9, 3, 'Vendor', 'test', '', '', '', 41, '', '', '', '', '', '', '1', '', '2013-10-10 05:54:26', 'Active'),
(110, 9, 3, 'Vendor', 'shun', '', '', '', 1, '', '', '', '', '', '', '1', '', '2013-10-12 05:55:56', 'Active'),
(111, 8, 4, 'Customer', 'Balaji', '', 'Balaji', 'loganathan', 41, '', '', '', '', '', '', '1', '', '2013-10-22 07:43:50', 'Active'),
(112, 9, 3, 'Customer', 'krish', '', '', '', 4, '', '', '', '', '', '', '1', '', '2013-10-24 09:54:42', 'Trash'),
(113, 10, 5, 'Customer', 'sundar', '', '', '', 41, '', '', '', '', '', '', '1', '', '2013-11-08 10:22:12', 'Active'),
(114, 10, 5, 'Customer', 'krish', '', '', '', 41, '', '', '', '', '', '', '1', '', '2013-11-08 10:23:08', 'Active'),
(115, 10, 5, 'Vendor', 'Ganesan', '', '', '', 41, '', '', '', '', '', '', '1', '', '2013-11-08 10:28:50', 'Active'),
(116, 10, 5, 'Vendor', 'sheik', '', '', '', 41, '', '', '', '', '', '', '1', '', '2013-11-08 10:42:36', 'Active'),
(129, 9, 3, 'Vendor', 'shun', '', '', '', 4, '', '', '', '', '', '', '97', '', '2013-11-26 08:27:11', 'Trash'),
(130, 9, 3, 'Vendor', 'musthafa', '', '', '', 4, '', '', '', '', '', '', '97', '', '2013-11-26 08:27:32', 'Active'),
(132, 8, 4, 'Vendor', 'test', '', '', '', 41, '', '', '', '', '', '', '97', '', '2013-11-26 12:50:56', 'Trash'),
(133, 8, 4, 'Vendor', 'Test Customer', '', '', '', 41, '', '', '', '', '', '', '97', '', '2013-11-26 12:52:34', 'Trash'),
(134, 8, 4, 'Customer', 'invoice customer', '', '', '', 41, '', '', '', '', '', '', '97', '', '2013-11-26 13:17:41', 'Trash'),
(135, 8, 4, 'Customer', 'set customer2', '', '', '', 41, '', '', '', '', '', '', '97', '', '2013-11-26 13:18:04', 'Trash'),
(136, 11, 6, 'Customer', 'super', 'testk9789@gmail.com', 'vignesh', 'waran', 41, '', '', '', '', '', '', '1', '', '2014-01-13 11:38:28', 'Active'),
(137, 11, 6, 'Vendor', 'krish', 'krish@lynchpintechnologies.com', 'krishna', 'kumar', 41, '', '', '', '', '', '', '1', '', '2014-01-13 12:03:19', 'Active'),
(138, 12, 7, 'Customer', 'krish', 'testk9789@gmail.com', 'krishna', 'waran', 41, '', '', '', '', '', '', '97', '', '2014-03-04 06:30:35', 'Active'),
(139, 12, 7, 'Vendor', 'krish', 'testk9789@gmail.com', 'krishna', 'waran', 41, '', '', '', '', '', '', '97', '', '2014-03-04 06:30:35', 'Active'),
(140, 12, 7, 'Vendor', 'vikki', 'testk9789@gmail.com', 'vignesh', 'waran', 41, '', '', '', '', '', '', '97', '', '2014-03-04 06:30:58', 'Active'),
(141, 12, 7, 'Customer', 'vikki', 'testk9789@gmail.com', 'vignesh', 'waran', 41, '', '', '', '', '', '', '97', '', '2014-03-04 06:30:58', 'Active'),
(142, 13, 8, 'Customer', 'TMS Consulting Ltd', 'stewart.kingu@tms.co.tz', '', '', 105, '', '', '', '', '', '', '1', '', '2014-03-06 04:38:11', 'Active'),
(143, 13, 8, 'Vendor', 'Exact Software Ltd', 'info@vision.co.tz', '', '', 105, '', '', '', '', '', '', '1', '', '2014-03-06 04:38:11', 'Active'),
(144, 16, 10, 'Customer', 'Jackhad', 'testk9789@gmail.com', 'Jack', 'had', 108, '', '', '', '', '', '', '1', '', '2014-03-06 12:12:22', 'Active'),
(145, 16, 10, 'Vendor', 'David', 'testk9789@gmail.com', 'David', 'John', 108, '', '', '', '', '', '', '1', '', '2014-03-06 12:13:23', 'Active'),
(146, 16, 10, 'Customer', 'senthil', 'zenthilengineer.1990@gmail.com', 'senthil', 'kumar', 41, '', '', '', '', '', '', '1', '', '2014-03-07 05:58:30', 'Trash'),
(147, 19, 12, 'Customer', 'David', 'David@gmail.com', 'David', 'jon', 108, '', '', '', '', '', '', '1', '', '2014-03-24 12:12:48', 'Trash'),
(148, 19, 12, 'Vendor', 'jon', 'jon123@gmail.com', 'jon', 'david', 108, '', '', '', '', '', '', '1', '', '2014-03-24 12:13:43', 'Trash'),
(149, 19, 12, 'Customer', 'David', 'David@gmail.com', 'David', 'jon', 108, '', '', '', '', '', '', '1', '', '2014-03-24 12:15:56', 'Active'),
(150, 19, 12, 'Vendor', 'David', 'David@gmail.com', 'David', 'jon', 108, '', '', '', '', '', '', '1', '', '2014-03-24 12:15:56', 'Active'),
(151, 19, 12, 'Vendor', 'jon', 'jon123@gmail.com', 'jon', 'jon', 108, '', '', '', '', '', '', '1', '', '2014-03-24 12:16:15', 'Active'),
(152, 19, 12, 'Customer', 'jon', 'jon123@gmail.com', 'jon', 'jon', 108, '', '', '', '', '', '', '1', '', '2014-03-24 12:16:15', 'Active'),
(153, 24, 17, 'Customer', 'Karthick', 'testuserlt1@gmail.com', 'Karthick', 'Raja', 41, '', '', '', '', '', '', '1', '', '2016-08-20 06:39:46', 'Active'),
(154, 24, 17, 'Vendor', 'Balamurugan', 'bala.lynchpin@gmail.com', 'Bala', 'Murugan', 41, '', '', '', '', '', '', '1', '', '2016-08-20 10:13:35', 'Active'),
(155, 13, 8, 'Customer', 'Balamurugan', 'bala.lynchpin@gmail.com', 'Bala', 'Murugan', 41, '', '', '', '', '', '', '1', '', '2016-08-22 11:23:04', 'Trash'),
(156, 13, 8, 'Customer', 'sudhakaran', 'hg@gmail.com', 'sddfghjk', 'sadfghjkl', 1, '', '', '', '', '', '', '1', '', '2018-09-18 16:50:52', 'Trash'),
(157, 13, 8, 'Vendor', 'ZAK Auto Works Ltd', '', '', '', 105, '', '', '', '', '', '', '1', '', '2019-04-29 03:32:15', 'Active'),
(158, 13, 8, 'Customer', 'Mlandizi Technologies', '', '', '', 105, '', '', '', '', '', '', '1', '', '2019-06-03 21:41:25', 'Active'),
(159, 13, 8, 'Vendor', 'Kilimanjaro Hotel', 'guest@kilimanjaro.co.tz', '', '', 117, '', '', '', '', '', '', '188', '', '2019-06-03 21:41:55', 'Active'),
(160, 26, 18, 'Customer', 'banda mf', '', '', '', 117, '', '', '', '', '', '', '188', '', '2019-06-04 14:07:06', 'Active'),
(161, 26, 18, 'Vendor', 'SAB', '', '', '', 117, '', '', '', '', '', '', '188', '', '2019-06-04 14:07:45', 'Active'),
(162, 27, 19, 'Vendor', 'SAB', '', '', '', 117, '', '', '', '', '', '', '188', '', '2019-06-11 07:00:56', 'Active'),
(163, 27, 19, 'Customer', 'M', '', '', '', 117, '', '', '', '', '', '', '188', '', '2019-06-11 07:03:42', 'Active'),
(164, 27, 19, 'Customer', 'M Banda', '', '', '', 117, '', '', '', '', '', '', '1', '', '2019-06-11 19:58:19', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `fin_dformates`
--

CREATE TABLE `fin_dformates` (
  `did` int(11) NOT NULL,
  `php` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `script` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Active','Inactive','Trash') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_dformates`
--

INSERT INTO `fin_dformates` (`did`, `php`, `script`, `status`) VALUES
(1, 'd.m.Y', 'dd.mm.yy', 'Active'),
(2, 'Y-m-d ', 'yy-mm-dd', 'Active'),
(3, 'Y/m/d', 'yy/mm/dd', 'Active'),
(4, 'y/m/d', 'y/mm/dd', 'Inactive'),
(5, 'm/d/Y', 'mm/dd/yy', 'Active'),
(6, 'm/d/y', 'mm/dd/y', 'Inactive'),
(7, 'd M, Y', 'dd M, yy', 'Active'),
(8, 'd M Y', 'dd M yy', 'Inactive'),
(9, 'd M y', 'dd M y', 'Inactive'),
(10, 'M d Y', 'M dd yy', 'Inactive'),
(11, 'M d y', 'M dd y', 'Inactive'),
(12, 'M d, Y', 'M dd, yy', 'Inactive'),
(13, 'M d, y', 'M dd, y', 'Inactive'),
(14, 'd F, Y', 'dd MM, yy', 'Inactive'),
(15, 'd F Y', 'dd MM yy', 'Inactive'),
(16, 'd F y', 'dd MM y', 'Inactive'),
(17, 'F d, Y', 'MM dd, yy', 'Inactive'),
(18, 'F d, y', 'MM dd, y', 'Inactive'),
(19, 'F d Y', 'MM dd yy', 'Inactive'),
(20, 'F d y', 'MM dd y', 'Inactive'),
(21, 'y-m-d', 'y-mm-dd', 'Inactive');

-- --------------------------------------------------------

--
-- Table structure for table `fin_emailcontents`
--

CREATE TABLE `fin_emailcontents` (
  `ecid` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `subject` text COLLATE utf8_unicode_ci NOT NULL,
  `emailcontent` text COLLATE utf8_unicode_ci NOT NULL,
  `lan` enum('en','tr') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_emailcontents`
--

INSERT INTO `fin_emailcontents` (`ecid`, `eid`, `title`, `subject`, `emailcontent`, `lan`) VALUES
(1, 1, 'Admin - Forgot Password', 'Forgot Password - Web Account', 'Your Login Credentials is below.<br />\r\n=========================<br />\r\n<br />\r\nUsername : {username}<br />\r\nPassword : {password}<br />\r\n<br />\r\nAdmin URL : {link}<br />\r\n<br />\r\n<br />\r\n<strong>Regards,<br />\r\nSupport Team,<br />\r\nWeb Account</strong><br />\r\n<br />\r\n<br />\r\n', 'en'),
(2, 2, 'Admin - Credentials', 'Web Account - Admin credentials', 'Your Admin Login Credentials is below.<br />\r\n===============================<br />\r\n<br />\r\nUsername : {username}<br />\r\nPassword : {password}<br />\r\n<br />\r\nAdmin URL : {link}<br />\r\n<br />\r\n<br />\r\n<strong>Regards,<br />\r\nSupport Team,<br />\r\nWeb Account</strong><br />\r\n<br />\r\n<br />\r\n', 'en'),
(3, 3, 'Admin Password - Request', 'Admin Password - Request', 'Your Admin Login New Credentials is below.<br />\r\n===================================<br />\r\n<br />\r\nUsername : {username}<br />\r\nPassword : {password}<br />\r\n<br />\r\nAdmin URL : {link}<br />\r\n<br />\r\n<br />\r\n<strong>Regards,<br />\r\nSupport Team,<br />\r\nWeb Account</strong><br />\r\n<br />\r\n<br />\r\n', 'en'),
(4, 4, 'User Login', 'Web Account - Login credentials', 'Dear {firstname},<br />\r\n<br />\r\nYour Web Account Login Credentials is below.<br />\r\n===============================<br />\r\n<br />\r\nUsername : {email}<br />\r\nPassword : {password}<br />\r\n<br />\r\nPlease click the below link to verify your account<br />\r\n{activationlink}<br />\r\n<br />\r\n<br />\r\n<strong>Regards,<br />\r\nSupport Team,<br />\r\nWeb Account</strong><br />\r\n<br />\r\n<br />\r\n', 'en'),
(5, 4, 'kullanıcı Girişi', 'Finansal - kimlik bilgileri giriş', 'Yazık! {name},<br />\r\n<br />\r\n<span class=\"short_text\" id=\"result_box\" lang=\"tr\"><span class=\"hps\">Sizin</span> <span class=\"hps\">Finansal</span> <span class=\"hps\">Giriş</span> <span class=\"hps\">Kimlik</span> <span class=\"hps\">altındadır.</span></span><br />\r\n===============================<br />\r\n<br />\r\n<span class=\"short_text\" id=\"result_box\" lang=\"tr\"><span class=\"hps\">Kullanıcı Adı</span></span> : {username}<br />\r\n<span class=\"short_text\" id=\"result_box\" lang=\"tr\"><span class=\"hps\">parola</span></span> : {password}<br />\r\n<br />\r\nURL : {link}<br />\r\n<br />\r\n<span id=\"result_box\" lang=\"tr\"><span class=\"hps\">Hesabınızı doğrulamak</span> <span class=\"hps\">i&ccedil;in</span> <span class=\"hps\">aşağıdaki linke</span> <span class=\"hps\">tıklayınız</span></span><br />\r\n{activationlink}<br />\r\n<br />\r\n<br />\r\n<strong>Regards,<br />\r\nSupport Team,<br />\r\nFinansal</strong><br />\r\n<br />\r\n<br />\r\n', 'tr'),
(6, 5, 'User - Forgot Password', 'Forgot Password', '<p>\r\n	Hello {username}</p>\r\n<p>\r\n	A request was received, hopefully from you, to reset your Accounts site password. Please go to this page to reset your password and be able to access the site.</p>\r\n<p>\r\n	Click to change your password {link}</p>\r\n<p>\r\n	Your forget key is : {key}</p>\r\n<br />\r\n<strong>Regards,<br />\r\nSupport Team,<br />\r\nWeb Account</strong>', 'en'),
(7, 6, 'Estimate', 'Estimate from {company}', '<p style=\"font-size:20px\">\r\n	Estimate {no}<br />\r\n	issued on {fdate} from {company}<br />\r\n	==============================</p>\r\n{message}<br />\r\nClick to view your estimate {link}<br />\r\n<br />\r\n<strong>Regards,<br />\r\nSupport Team,<br />\r\nWeb Account</strong><br />\r\n<br />\r\n<br />\r\n', 'en'),
(8, 7, 'Send Invoice', 'Invoice from {company}', '<p style=\"font-size:20px\">\r\n	Invoice {no}<br />\r\n	issued on {fdate} from {company}<br />\r\n	==============================</p>\r\n{message}<br />\r\nClick to view your invoice {link}<br />\r\n<br />\r\n<strong>Regards,<br />\r\nSupport Team,<br />\r\nWeb Account</strong><br />\r\n<br />\r\n<br />\r\n', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `fin_emaillists`
--

CREATE TABLE `fin_emaillists` (
  `eid` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `label` text COLLATE utf8_unicode_ci NOT NULL,
  `fromname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fromemail` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_emaillists`
--

INSERT INTO `fin_emaillists` (`eid`, `title`, `label`, `fromname`, `fromemail`) VALUES
(1, 'Admin - Forgot Password', 'Username : {username}, Password : {password}, Link : {link}', 'Web Account', 'support@lynchpintechnologies.com'),
(2, 'Admin - Credentials', 'Username : {username}, Password : {password}, Link : {link}', 'Web Account', 'support@lynchpintechnologies.com'),
(3, 'Admin Password - Request', 'Username : {username}, Password : {password}, Link : {link}', 'Web Account', 'support@lynchpintechnologies.com'),
(4, 'User Login', 'Username : {email}, Password :{password},Activation Link : {activationlink},Link:{link},Name : {firstname}', 'Web Account', 'support@lynchpintechnologies.com'),
(5, 'User - Forgot Password', 'Username : {username}, Key : {key}, Link : {link}', 'Web Account', 'support@lynchpintechnologies.com'),
(6, 'Estimate', 'Company:{company},User Message:{message},Estimate Number:{no},Estimate Date:{fdate}, Preview Link:{link},Total:{total}', 'Estimate', 'support@lynchpintechnologies.com'),
(7, 'Send Invoice', 'Company:{company},User Message:{message},Invoice Number:{no},Invoice Date:{fdate}, Preview Link:{link},Total:{total}', 'Web Account', 'support@lynchpintechnologies.com');

-- --------------------------------------------------------

--
-- Table structure for table `fin_enquiries`
--

CREATE TABLE `fin_enquiries` (
  `cid` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `view` int(11) NOT NULL,
  `reply` int(11) NOT NULL,
  `replysubject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `replymessage` text COLLATE utf8_unicode_ci NOT NULL,
  `createddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `replydate` datetime NOT NULL,
  `lan` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fin_estimateproducts`
--

CREATE TABLE `fin_estimateproducts` (
  `epid` int(11) NOT NULL,
  `esid` int(11) NOT NULL COMMENT 'estimateid',
  `pid` int(11) NOT NULL COMMENT 'product id',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `price` double NOT NULL,
  `discount` double NOT NULL,
  `discounttype` enum('Percentage','Amount') COLLATE utf8_unicode_ci NOT NULL,
  `discountamt` double NOT NULL,
  `subtotal` double NOT NULL,
  `taxamount` double NOT NULL,
  `total` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_estimateproducts`
--

INSERT INTO `fin_estimateproducts` (`epid`, `esid`, `pid`, `description`, `qty`, `price`, `discount`, `discounttype`, `discountamt`, `subtotal`, `taxamount`, `total`) VALUES
(8, 7, 64, '', 1, 32, 0, 'Percentage', 0, 0, 0, 32),
(9, 7, 91, '', 1, 10, 0, 'Percentage', 0, 0, 0, 10),
(10, 7, 67, '', 1, 12, 0, 'Percentage', 0, 0, 0, 12),
(11, 8, 64, '', 1, 32, 0, 'Percentage', 0, 0, 0, 32),
(12, 8, 73, '', 1, 10, 0, 'Percentage', 0, 0, 0, 10),
(13, 9, 64, '', 1, 32, 1, 'Percentage', 0.32, 0, 0, 31.68),
(14, 9, 67, '', 1, 32, 2, 'Percentage', 0.64, 0, 0, 31.36),
(15, 9, 70, '', 1, 10, 1, 'Percentage', 0.1, 0, 0, 9.9),
(16, 9, 91, '', 1, 8, 10, 'Percentage', 0.8, 0, 0, 7.2),
(17, 10, 59, '', 1, 2500, 0, 'Percentage', 0, 0, 0, 2500),
(18, 10, 65, '', 10, 100, 0, 'Percentage', 0, 0, 0, 1000),
(19, 11, 64, '', 1, 32, 12, 'Percentage', 3.84, 0, 0, 28.16),
(20, 11, 67, '', 1, 32, 12, 'Percentage', 3.84, 0, 0, 28.16),
(21, 11, 73, '', 1, 10, 10, 'Percentage', 1, 0, 0, 9),
(22, 11, 91, '', 1, 10, 5, 'Percentage', 0.5, 0, 0, 9.5),
(23, 12, 107, 'new mobile', 1, 10000, 0, 'Percentage', 0, 0, 0, 10000),
(24, 13, 110, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, co', 1, 2000, 0, 'Percentage', 0, 0, 0, 2000),
(25, 13, 110, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, co', 1, 2000, 0, 'Percentage', 0, 0, 0, 2000),
(29, 15, 124, 'he digital world needs an adaptive User Experience We create itWe Prefer Long-Term App Development Relationships', 1, 3000, 0, 'Percentage', 0, 0, 0, 3000),
(30, 15, 123, 'he digital world needs an adaptive User Experience We create itWe Prefer Long-Term App Development Relationships', 1, 100, 0, 'Percentage', 0, 0, 0, 100),
(31, 16, 124, 'he digital world needs an adaptive User Experience We create itWe Prefer Long-Term App Development Relationships', 1, 3000, 0, 'Percentage', 0, 0, 0, 3000),
(32, 17, 110, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, co', 1, 2000, 0, 'Percentage', 0, 0, 0, 2000),
(33, 18, 126, 'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 500, 0, 'Percentage', 0, 0, 0, 500),
(34, 19, 128, 'Product', 1, 4000, 0, 'Percentage', 0, 0, 0, 4000);

-- --------------------------------------------------------

--
-- Table structure for table `fin_estimates`
--

CREATE TABLE `fin_estimates` (
  `eid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `bid` int(11) NOT NULL,
  `cid` int(11) NOT NULL COMMENT 'customer id',
  `estimatetitle` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `estimateno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `estimate_date` date NOT NULL,
  `estimate_expires` date NOT NULL,
  `currency` int(11) NOT NULL,
  `primary_currency` int(11) NOT NULL,
  `exchange_rate` double DEFAULT NULL,
  `posono` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'purchase no and shipping order no',
  `subheading` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `memo` text COLLATE utf8_unicode_ci,
  `total` double NOT NULL,
  `subtotal` double NOT NULL,
  `exchangetotal` double DEFAULT NULL,
  `key` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `createddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estimatestatus` enum('Saved','Sent') COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Active','Inactive','Trash') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_estimates`
--

INSERT INTO `fin_estimates` (`eid`, `uid`, `bid`, `cid`, `estimatetitle`, `estimateno`, `estimate_date`, `estimate_expires`, `currency`, `primary_currency`, `exchange_rate`, `posono`, `subheading`, `footer`, `memo`, `total`, `subtotal`, `exchangetotal`, `key`, `createddate`, `estimatestatus`, `status`) VALUES
(7, 9, 3, 10, 'Estimate', 'e-1', '2013-10-22', '2013-10-22', 108, 4, 1.0358400344848633, '', 'estimate subheading', 'estimate footer', 'Estimate memo', 60.20000076293945, 54, 62.36, '3a1gg6yb', '2013-10-22 10:08:28', 'Saved', 'Active'),
(8, 9, 3, 98, 'Estimate', 'e-2', '2013-10-22', '2013-10-22', 4, 4, 1, '', 'estimate subheading', 'estimate footer', 'Estimate memo', 46, 42, 46, 'vfbvr84e', '2013-10-22 10:24:34', 'Saved', 'Active'),
(9, 9, 3, 10, 'Estimate', 'e-3', '2013-10-23', '2013-10-23', 108, 4, 1.035949945449829, '', 'estimate subheading', 'estimate footer', 'Estimate memo', 88.93160247802734, 80.13999938964844, 92.13, 'co1aypam', '2013-10-23 11:44:05', 'Sent', 'Active'),
(10, 8, 4, 1, 'Estimate', 'e-1', '2013-10-25', '2013-11-09', 108, 41, 61.4514841, '', 'estimate subheading', 'estimate footer', 'Memo', 3675, 3500, 225834.2040675, 'zzo02mse', '2013-10-25 05:43:11', 'Saved', 'Active'),
(11, 9, 3, 10, 'Estimate', 'e-4', '2013-10-29', '2013-10-29', 108, 4, 1.04755919, '', 'estimate subheading', 'estimate footer', 'Estimate memo', 83.6083984375, 74.81999969482422, 87.58, 'sge1cdho', '2013-10-29 06:03:10', 'Saved', 'Active'),
(12, 11, 6, 136, 'Estimate', 'b123', '2014-01-13', '2014-01-13', 41, 41, 1, '', 'subheading', 'footercontent', 'descrption', 10000, 10000, 10000, 'enjbqn0c', '2014-01-13 11:40:40', 'Saved', 'Active'),
(13, 12, 7, 141, 'Estimate', 'e-1', '2014-03-04', '2014-03-19', 41, 41, 1, '', 'Bangalore Karnataka india', 'Bangalore Karnataka india', 'Bangalore Karnataka india ', 4400, 4000, 4400, 'jofhgdwk', '2014-03-04 06:37:16', 'Saved', 'Active'),
(15, 16, 10, 144, 'Estimate', 'e-1', '2014-03-06', '2014-03-06', 108, 108, 1, '', 'Thank you ', 'Thank you for your business', 'If you have question about this invoice , Please contact E-mail', 3410, 3100, 3410, '48qmmgzq', '2014-03-06 12:16:35', 'Saved', 'Active'),
(16, 16, 10, 144, 'Estimate', 'e-2', '2014-03-07', '2014-03-07', 108, 108, 1, '', 'Thank you ', 'Thank you for your business', 'If you have question about this invoice , Please contact E-mail', 3300, 3000, 3300, 't481fv0k', '2014-03-07 04:53:02', 'Saved', 'Active'),
(17, 12, 7, 138, 'Estimate', 'e-2', '2014-03-24', '2014-04-08', 41, 41, 1, '', 'Bangalore Karnataka india', 'Bangalore Karnataka india', 'Bangalore Karnataka india ', 2000, 2000, 2000, 'ijjtg2kv', '2014-03-24 06:32:41', 'Sent', 'Active'),
(18, 19, 12, 152, 'Estimate', 'e-1', '2014-03-24', '2014-03-24', 100, 108, 0.632831, '', 'Thank you for your business', 'Thank you for your business', 'Thank you for your business', 550, 500, 348.05705, '3kvlq9rj', '2014-03-24 12:18:42', 'Saved', 'Active'),
(19, 24, 17, 153, 'Estimate', 'e-1', '2016-08-20', '2016-08-20', 41, 41, 1, '', '', '', '', 4000, 4000, 4000, 'u428a3cm', '2016-08-20 06:41:23', 'Saved', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `fin_estimatetaxes`
--

CREATE TABLE `fin_estimatetaxes` (
  `estid` int(11) NOT NULL,
  `esid` int(11) NOT NULL COMMENT 'estimateid',
  `espid` int(11) NOT NULL COMMENT 'estimate product id',
  `taxid` int(11) NOT NULL COMMENT 'Tax id',
  `tamt` double NOT NULL COMMENT 'percentage or amount',
  `taxtype` enum('Percentage','Amount') COLLATE utf8_unicode_ci NOT NULL,
  `taxamount` double NOT NULL,
  `currency` int(11) NOT NULL,
  `primarycurrency` int(11) NOT NULL,
  `exchange_rate` double NOT NULL,
  `totalamount` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_estimatetaxes`
--

INSERT INTO `fin_estimatetaxes` (`estid`, `esid`, `espid`, `taxid`, `tamt`, `taxtype`, `taxamount`, `currency`, `primarycurrency`, `exchange_rate`, `totalamount`) VALUES
(23, 7, 8, 13, 3, 'Amount', 3, 108, 4, 1.0358400344848633, 3.11),
(24, 7, 9, 14, 2, 'Percentage', 0.2, 108, 4, 1.0358400344848633, 0.21),
(25, 7, 10, 13, 3, 'Amount', 3, 108, 4, 1.0358400344848633, 3.11),
(26, 8, 11, 13, 3, 'Amount', 3, 4, 4, 1, 3),
(27, 8, 12, 14, 10, 'Percentage', 1, 4, 4, 1, 1),
(31, 9, 13, 13, 12, 'Percentage', 3.8, 108, 4, 1.035949945449829, 3.94),
(32, 9, 14, 14, 10, 'Percentage', 3.14, 108, 4, 1.035949945449829, 3.25),
(33, 9, 15, 14, 10, 'Percentage', 0.99, 108, 4, 1.035949945449829, 1.03),
(34, 9, 16, 13, 12, 'Percentage', 0.86, 108, 4, 1.035949945449829, 0.9),
(39, 11, 19, 13, 12, 'Percentage', 3.38, 108, 4, 1.047559142112732, 3.54),
(40, 11, 20, 13, 12, 'Percentage', 3.38, 108, 4, 1.047559142112732, 3.54),
(41, 11, 21, 13, 12, 'Percentage', 1.08, 108, 4, 1.047559142112732, 1.13),
(42, 11, 22, 14, 10, 'Percentage', 0.95, 108, 4, 1.047559142112732, 1),
(43, 10, 17, 16, 5, 'Percentage', 125, 108, 41, 61.4514841, 7681.4355125),
(44, 10, 18, 16, 5, 'Percentage', 50, 108, 41, 61.4514841, 3072.574205),
(50, 15, 29, 23, 10, 'Percentage', 300, 108, 108, 1, 300),
(51, 15, 30, 23, 10, 'Percentage', 10, 108, 108, 1, 10),
(52, 16, 31, 23, 10, 'Percentage', 300, 108, 108, 1, 300),
(53, 13, 24, 21, 10, 'Percentage', 200, 41, 41, 1, 200),
(54, 13, 25, 21, 10, 'Percentage', 200, 41, 41, 1, 200),
(55, 18, 33, 25, 10, 'Percentage', 50, 100, 108, 0.632831, 31.64155);

-- --------------------------------------------------------

--
-- Table structure for table `fin_featurecontents`
--

CREATE TABLE `fin_featurecontents` (
  `fcid` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `lan` varchar(5) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_featurecontents`
--

INSERT INTO `fin_featurecontents` (`fcid`, `fid`, `title`, `description`, `lan`) VALUES
(4, 6, 'Dashboard', 'Lorem Ipsum is simply dummy text of the printing and typesetting.', 'en'),
(5, 6, 'Turkey ', 'Turkey description', 'tr'),
(6, 8, 'Collaboration', 'Lorem Ipsum is simply dummy text of the printing and typesetting.', 'en'),
(7, 9, 'Invoice', 'Lorem Ipsum is simply dummy text of the printing and typesetting.', 'en'),
(8, 10, 'Security', 'Lorem Ipsum is simply dummy text of the printing and typesetting.', 'en'),
(9, 11, 'Reports', 'Lorem Ipsum is simply dummy text of the printing and typesetting.', 'en'),
(10, 12, 'Income', 'Lorem Ipsum is simply dummy text of the printing and typesetting.', 'en'),
(12, 14, 'Accounting', 'Lorem Ipsum is simply dummy text of the printing and typesetting.', 'en'),
(13, 15, 'Transaction', 'Lorem Ipsum is simply dummy text of the printing and typesetting.', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `fin_features`
--

CREATE TABLE `fin_features` (
  `fid` int(11) NOT NULL,
  `image` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `display` enum('home','aboutus','both') COLLATE utf8_unicode_ci NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive','Trash') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_features`
--

INSERT INTO `fin_features` (`fid`, `image`, `display`, `created_date`, `status`) VALUES
(6, '52613726-c208-4f4f-a29b-0854c0a80228.png', 'both', '2013-10-18 13:27:02', 'Active'),
(8, '52613759-e358-4a11-b85e-0854c0a80228.png', 'home', '2013-10-21 12:58:07', 'Active'),
(9, '52613773-dcbc-4650-9b51-0854c0a80228.png', 'home', '2013-10-18 13:28:19', 'Active'),
(10, '5261378f-a4f4-4009-856f-0854c0a80228.png', 'home', '2013-10-18 13:28:47', 'Active'),
(11, '526137b1-b9c8-4165-9594-0854c0a80228.png', 'home', '2013-10-21 13:00:54', 'Active'),
(12, '526137c7-ad64-4e5f-9f50-0854c0a80228.png', 'home', '2013-10-18 13:29:43', 'Active'),
(14, '52652439-fdb4-40b4-8f41-0934c0a80228.png', 'aboutus', '2013-10-21 12:55:21', 'Active'),
(15, '526525b9-e0b4-4bd6-b10e-0934c0a80228.png', 'aboutus', '2013-10-21 13:01:45', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `fin_invoiceproducts`
--

CREATE TABLE `fin_invoiceproducts` (
  `ipid` int(11) NOT NULL,
  `ivid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `description` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `price` double NOT NULL,
  `taxamount` double NOT NULL,
  `amount` double NOT NULL,
  `total` double NOT NULL,
  `etotal` double NOT NULL,
  `discount` double NOT NULL,
  `discounttype` enum('Percentage','Amount') COLLATE utf8_unicode_ci NOT NULL,
  `discountamt` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_invoiceproducts`
--

INSERT INTO `fin_invoiceproducts` (`ipid`, `ivid`, `pid`, `description`, `qty`, `price`, `taxamount`, `amount`, `total`, `etotal`, `discount`, `discounttype`, `discountamt`) VALUES
(68, 14, 95, '', 1, 10, 0, 0, 9.9, 0, 1, 'Percentage', 0.1),
(83, 15, 2, 'dsfsdaff', 1, 32, 0, 0, 31.68, 0, 1, 'Percentage', 0.32),
(84, 15, 95, '', 1, 10, 0, 0, 9.8, 0, 2, 'Percentage', 0.2),
(248, 4, 59, '', 1, 2500, 0, 0, 2500, 153628.74984741, 0, 'Percentage', 0),
(249, 4, 65, '', 1, 100, 0, 0, 100, 6145.1499938965, 0, 'Percentage', 0),
(270, 16, 65, '', 1, 100, 0, 0, 90, 90, 10, 'Percentage', 10),
(271, 13, 59, '', 1, 2500, 0, 0, 2500, 2500, 0, 'Percentage', 0),
(272, 13, 65, '', 10, 100, 0, 0, 1000, 1000, 0, 'Percentage', 0),
(273, 13, 66, '', 2, 150, 0, 0, 300, 300, 0, 'Percentage', 0),
(376, 30, 67, 'mouse product', 11, 32, 0, 0, 352, 352, 0, 'Percentage', 0),
(377, 30, 73, '', 12, 10, 0, 0, 120, 120, 0, 'Percentage', 0),
(378, 30, 73, '', 13, 10, 0, 0, 130, 130, 0, 'Percentage', 0),
(379, 30, 91, 'New product', 13, 10, 0, 0, 130, 130, 0, 'Percentage', 0),
(380, 31, 67, 'mouse product', 5, 32, 0, 0, 160, 175.1456, 0, 'Percentage', 0),
(381, 31, 91, 'New product', 5, 10, 0, 0, 50, 54.733, 0, 'Percentage', 0),
(382, 31, 67, 'mouse product', 5, 32, 0, 0, 160, 175.1456, 0, 'Percentage', 0),
(412, 33, 67, 'mouse product', 1, 32, 0, 0, 32, 35.05632, 0, 'Percentage', 0),
(413, 34, 70, 'Nokia', 1, 1000, 0, 0, 1000, 1000, 0, 'Percentage', 0),
(414, 35, 67, 'mouse product', 1, 32, 0, 0, 32, 32, 0, 'Percentage', 0),
(422, 32, 100, 'austral', 30, 42666.29, 0, 0, 1279988.7, 1279988.7, 0, 'Percentage', 0),
(423, 32, 101, 'BV', 20, 47.17, 0, 0, 943.4, 943.4, 0, 'Percentage', 0),
(424, 32, 102, 'FCS', 50, 3.11, 0, 0, 155.5, 155.5, 0, 'Percentage', 0),
(425, 32, 103, 'CENT PLY', 20, 73.5, 0, 0, 1470, 1470, 0, 'Percentage', 0),
(426, 32, 104, 'GUR', 165, 47.84, 0, 0, 7893.6, 7893.6, 0, 'Percentage', 0),
(427, 32, 105, 'KFA', 15, 38.26, 0, 0, 573.9, 573.9, 0, 'Percentage', 0),
(428, 32, 106, 'PBA', 10, 97.47, 0, 0, 974.7, 974.7, 0, 'Percentage', 0),
(430, 36, 107, 'product description goes here', 1, 10000, 0, 0, 10000, 10000, 0, 'Percentage', 0),
(431, 37, 65, 'dfgdfgdfg', 1, 100, 0, 0, 100, 100, 0, 'Percentage', 0),
(432, 37, 66, 'dsfs', 1, 150, 0, 0, 150, 150, 0, 'Percentage', 0),
(433, 38, 109, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ', 1, 1000, 0, 0, 1000, 1000, 0, 'Percentage', 0),
(434, 38, 115, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ', 1, 10000, 0, 0, 10000, 10000, 0, 'Percentage', 0),
(435, 39, 110, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ', 1, 2000, 0, 0, 2000, 0, 0, 'Percentage', 0),
(436, 39, 110, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ', 1, 2000, 0, 0, 2000, 0, 0, 'Percentage', 0),
(437, 40, 110, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ', 1, 2000, 0, 0, 2000, 2000, 0, 'Percentage', 0),
(438, 40, 109, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ', 1, 1000, 0, 0, 1000, 1000, 0, 'Percentage', 0),
(439, 40, 111, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ', 1, 3000, 0, 0, 3000, 3000, 0, 'Percentage', 0),
(454, 43, 123, 'he digital world needs an adaptive User Experience We create itWe Prefer Long-Term App Development Relationships', 1, 100, 0, 0, 100, 100, 0, 'Percentage', 0),
(455, 43, 124, '', 1, 3000, 0, 0, 3000, 3000, 0, 'Percentage', 0),
(458, 44, 124, 'he digital world needs an adaptive User Experience We create itWe Prefer Long-Term App Development Relationships', 1, 3000, 0, 0, 3000, 3000, 0, 'Percentage', 0),
(459, 44, 123, 'he digital world needs an adaptive User Experience We create itWe Prefer Long-Term App Development Relationships', 1, 100, 0, 0, 100, 100, 0, 'Percentage', 0),
(460, 45, 124, 'he digital world needs an adaptive User Experience We create itWe Prefer Long-Term App Development Relationships', 1, 3000, 0, 0, 3000, 3000, 0, 'Percentage', 0),
(461, 45, 124, 'he digital world needs an adaptive User Experience We create itWe Prefer Long-Term App Development Relationships', 1, 3000, 0, 0, 3000, 3000, 0, 'Percentage', 0),
(465, 48, 110, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ', 1, 2000, 0, 0, 2000, 2000, 0, 'Percentage', 0),
(468, 49, 126, 'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing softwar', 1, 500, 0, 0, 500, 500, 0, 'Percentage', 0),
(469, 49, 127, 'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing softwar', 1, 350, 0, 0, 350, 350, 0, 'Percentage', 0),
(471, 50, 126, 'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing softwar', 1, 500, 0, 0, 500, 316.4155, 0, 'Percentage', 0),
(473, 51, 128, '', 1, 1000, 0, 0, 1000, 1000, 0, 'Percentage', 0),
(474, 51, 129, '', 1, 2000, 0, 0, 1800, 1800, 10, 'Percentage', 200),
(475, 52, 128, 'Product', 1, 1000, 0, 0, 1000, 1000, 0, 'Percentage', 0),
(490, 59, 134, 'Black label case', 450, 300, 0, 0, 135000, 135000, 0, 'Percentage', 0),
(491, 59, 135, 'Castle lite case', 190, 350, 0, 0, 66500, 66500, 0, 'Percentage', 0),
(492, 59, 136, '', 100, 400, 0, 0, 40000, 40000, 0, 'Percentage', 0),
(493, 60, 137, 'Black label Cases', 10, 200, 0, 0, 2000, 2000, 0, 'Percentage', 0),
(494, 61, 137, 'Black label Cases', 960, 200, 0, 0, 192000, 192000, 0, 'Percentage', 0),
(495, 62, 133, 'Black Label', 1, 200, 0, 0, 200, 200, 0, 'Percentage', 0),
(496, 63, 132, '', 1, 800, 0, 0, 800, 800, 0, 'Percentage', 0);

-- --------------------------------------------------------

--
-- Table structure for table `fin_invoices`
--

CREATE TABLE `fin_invoices` (
  `ivid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `bid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `invoicedate` date NOT NULL,
  `duedate` date NOT NULL,
  `invoiceno` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `psno` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'purchaseno or shipping order no',
  `invoicetitle` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `subheading` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `memo` text COLLATE utf8_unicode_ci,
  `currency` int(11) DEFAULT NULL,
  `primary_currency` int(11) NOT NULL,
  `exchange_rate` double DEFAULT NULL,
  `total` double NOT NULL,
  `subtotal` double NOT NULL,
  `exchangetotal` double DEFAULT NULL,
  `receivedpayment` double NOT NULL,
  `balance` double DEFAULT NULL COMMENT 'Balance Payment',
  `excessamount` double DEFAULT NULL COMMENT 'Extra payment from customer',
  `createddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `invoicestatus` enum('Draft','Save','Sent','Partial','Overdue','Paid') COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Active','Inactive','Trash') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_invoices`
--

INSERT INTO `fin_invoices` (`ivid`, `uid`, `bid`, `cid`, `eid`, `invoicedate`, `duedate`, `invoiceno`, `psno`, `invoicetitle`, `subheading`, `footer`, `memo`, `currency`, `primary_currency`, `exchange_rate`, `total`, `subtotal`, `exchangetotal`, `receivedpayment`, `balance`, `excessamount`, `createddate`, `key`, `invoicestatus`, `status`) VALUES
(4, 8, 4, 1, 0, '2013-10-25', '2013-10-07', '1', '', 'Invoice Title', 'Sub heading for invoice', 'Footer ivoice', 'Memo Invoice', 108, 41, 61.451499938964844, 2600, 2600, 159773.89984131, 2600, NULL, NULL, '2013-10-25 05:12:30', 'e0tglbv3', 'Paid', 'Active'),
(13, 8, 4, 1, 10, '2013-10-25', '2013-11-21', '3', '', 'Title', 'estimate subheading', 'estimate footer', 'Memo', 41, 41, 1, 3800, 3800, 3800, 3800, NULL, NULL, '2013-10-25 05:43:11', '0y1zggf3', 'Paid', 'Active'),
(14, 10, 5, 113, 0, '2013-11-08', '2013-11-15', '1', '', 'Invoice', '', '', '', 41, 41, 1, 9.9, 9.9, 9.9, 9.9, NULL, NULL, '2013-11-08 10:22:25', 'mfca0nn1', 'Paid', 'Active'),
(15, 10, 5, 114, 0, '2013-10-01', '2013-11-12', '2', '', 'Invoice', '', '', '', 41, 41, 1, 46.118, 41.48, 46.118, 0, NULL, NULL, '2013-11-08 10:24:57', '68vv69zu', 'Overdue', 'Active'),
(16, 8, 4, 111, 0, '2013-11-11', '2013-11-21', '4', '', 'Invoice Title', 'Sub heading for invoice', 'Footer ivoice', 'Memo Invoice', 41, 41, 1, 94.5, 90, 94.5, 94.5, NULL, NULL, '2013-11-11 08:36:37', 'yzbe8kms', 'Paid', 'Active'),
(30, 9, 3, 10, 0, '2013-11-25', '2013-11-01', '2', '', 'Invoice', 'Sub heading', 'Footer', 'Memo', 4, 4, 1, 732, 732, 732, 732, NULL, NULL, '2013-11-25 07:37:27', '94inbzc9', 'Paid', 'Active'),
(31, 9, 3, 10, 0, '2013-11-25', '2013-11-25', '3', '', 'Invoice', 'Sub heading', 'Footer', 'Memo', 108, 4, 1.09466, 370, 370, 0, 370, NULL, NULL, '2013-11-25 07:54:49', 'c9hdg4vy', 'Paid', 'Active'),
(32, 8, 4, 1, 0, '2013-11-25', '2013-12-10', '5', '', 'Invoice Title', 'Sub heading for invoice', 'Footer ivoice', 'Memo Invoice', 41, 41, 1, 1291999.8, 1291999.8, 1291999.8, 51500.8, NULL, NULL, '2013-11-25 10:24:57', 'mthhy5sa', 'Overdue', 'Active'),
(33, 9, 3, 10, 0, '2013-11-27', '2013-11-26', '4', '', 'Invoice', 'Sub heading', 'Footer', 'Memo', 108, 4, 1.09551, 32, 32, 35.05632, 32, NULL, NULL, '2013-11-27 11:15:39', '5a0v96yi', 'Paid', 'Active'),
(34, 9, 3, 73, 0, '2013-11-27', '2013-11-27', '5', '4', 'Invoice', 'Sub heading', 'Footer', 'Memo', 41, 4, 1, 1000, 1000, 1000, 1000, NULL, NULL, '2013-11-27 11:22:25', 'bcnvw29s', 'Paid', 'Active'),
(35, 9, 3, 73, 0, '2013-11-28', '2013-11-28', '51', '', 'Invoice', 'Sub heading', 'Footer', 'Memo', 4, 4, 1, 32, 32, 32, 0, NULL, NULL, '2013-11-28 12:40:15', '905gtlwc', 'Overdue', 'Active'),
(36, 11, 6, 136, 0, '2014-01-13', '2014-01-13', '1', '', 'Invoice', 'subheading', 'footer content', 'description', 41, 41, 1, 10000, 10000, 10000, 0, NULL, NULL, '2014-01-13 11:43:41', '24yp9mz2', 'Overdue', 'Active'),
(37, 8, 4, 8, 0, '2014-02-18', '2014-03-05', '6', '', 'Invoice Title', 'Sub heading for invoice', 'Footer ivoice', 'Memo Invoice', 41, 41, 1, 250, 250, 250, 0, NULL, NULL, '2014-02-18 05:26:25', 'nga6hwf7', 'Overdue', 'Active'),
(38, 12, 7, 138, 0, '2014-03-04', '2014-03-19', '1', '', 'Invoice', 'Bangalore Karnataka india', 'Bangalore Karnataka india', 'Bangalore Karnataka india ', 41, 41, 1, 12100, 11000, 12100, 10, NULL, NULL, '2014-03-04 06:33:45', 'bv78h4ss', 'Overdue', 'Active'),
(39, 12, 7, 141, 13, '2014-03-04', '2014-03-19', '2', NULL, '', 'Bangalore Karnataka india', 'Bangalore Karnataka india', 'Bangalore Karnataka india ', 41, 41, 1, 4400, 4000, 4400, 0, NULL, NULL, '2014-03-04 06:37:16', 'y2wtgq3p', 'Overdue', 'Active'),
(40, 12, 7, 138, 0, '2014-03-04', '2014-03-19', '3', '', 'Invoice', 'Bangalore Karnataka india', 'Bangalore Karnataka india', 'Bangalore Karnataka india ', 41, 41, 1, 6600, 6000, 6600, 0, NULL, NULL, '2014-03-04 11:22:08', 'chm8gs31', 'Overdue', 'Active'),
(43, 16, 10, 144, 0, '2014-03-06', '2014-03-06', '1', '', 'Invoice', 'Thank you ', 'Thank you for your business', 'If you have question about this invoice , Please contact E-mail', 108, 108, 1, 3410, 3100, 3410, 3400, NULL, NULL, '2014-03-06 12:15:11', 'lg8wn7k3', 'Overdue', 'Active'),
(44, 16, 10, 144, 15, '2014-03-06', '2014-03-06', '2', '', 'invoice', 'Thank you ', 'Thank you for your business', 'If you have question about this invoice , Please contact E-mail', 108, 108, 1, 3410, 3100, 3410, 1500, NULL, NULL, '2014-03-06 12:16:35', 'ztpo4z95', 'Overdue', 'Active'),
(45, 16, 10, 144, 0, '2014-03-07', '2014-03-07', '3', '', 'Invoice', 'Thank you ', 'Thank you for your business', 'If you have question about this invoice , Please contact E-mail', 108, 108, 1, 6350, 6000, 6350, 0, NULL, NULL, '2014-03-07 04:52:16', 'buat2qq2', 'Overdue', 'Active'),
(48, 12, 7, 138, 0, '2014-03-24', '2014-04-08', '445', '', 'Invoice', 'Bangalore Karnataka india', 'Bangalore Karnataka india', 'Bangalore Karnataka india ', 41, 41, 1, 2000, 2000, 2000, 0, NULL, NULL, '2014-03-24 06:33:31', '4s2ioqvy', 'Overdue', 'Active'),
(49, 19, 12, 149, 0, '2014-03-24', '2014-03-24', '1', '', 'Invoice', 'Thank you for your business', 'Thank you for your business', 'Thank you for your business. We do expect payment within 21 days, so please process this invoice within that time. There will be a 1.5% interest charg', 108, 108, 1, 935, 850, 935, 500, NULL, NULL, '2014-03-24 12:17:29', 'w7i6vvwv', 'Overdue', 'Active'),
(50, 19, 12, 152, 18, '2014-03-24', '2014-03-24', '2', '', 'invoice', 'Thank you for your business', 'Thank you for your business', 'Thank you for your business', 100, 108, 0.632831, 550, 500, 348.05705, 0, NULL, NULL, '2014-03-24 12:18:42', 'okt8vfwq', 'Overdue', 'Active'),
(51, 24, 17, 153, 0, '2016-08-20', '2016-08-20', '1', '', 'Invoice', '', '', 'Test memo', 41, 41, 1, 2800, 2800, 2800, 0, NULL, NULL, '2016-08-20 06:40:41', 't38ocilb', 'Overdue', 'Active'),
(52, 24, 17, 153, 0, '2016-08-20', '2016-08-20', '2', '', 'Invoice', '', '', '<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 41, 41, 1, 1000, 1000, 1000, 0, NULL, NULL, '2016-08-20 10:07:48', 'un3zarcj', 'Overdue', 'Active'),
(59, 26, 18, 160, 0, '2019-06-04', '2019-06-19', '1', '', 'Invoice', '', '', '', 117, 117, 1, 277725, 241500, 277725, 277725, NULL, NULL, '2019-06-04 18:07:20', 'q9b6goey', 'Paid', 'Active'),
(60, 27, 19, 164, 0, '2019-06-11', '2019-06-11', '1', '', 'Invoice', 'Tax Invoice', 'Thinkmybiz Limited', 'Welcome and Keep supporting', 117, 117, 1, 2300, 2000, 2300, 2300, NULL, NULL, '2019-06-11 19:58:54', 'g2y75cny', 'Paid', 'Active'),
(61, 27, 19, 164, 0, '2019-06-12', '2019-06-12', '2', '', 'Invoice', 'Tax Invoice', 'Thinkmybiz Limited', 'Welcome and Keep supporting', 117, 117, 1, 220800, 192000, 220800, 220800, NULL, NULL, '2019-06-12 03:46:26', 'ou5otupn', 'Paid', 'Active'),
(62, 13, 8, 158, 0, '2019-07-22', '2019-08-06', '1', '', 'Invoice', 'Sample Invoice', 'Sample Invoice', 'Sample Invoice', 105, 108, 1, 236, 200, 236, 0, NULL, NULL, '2019-07-22 20:00:55', 'd0qiyb8d', 'Overdue', 'Active'),
(63, 13, 8, 142, 0, '2020-02-03', '2020-02-18', '2', '', 'Invoice', 'Sample Invoice', 'Sample Invoice', 'Sample Invoice', 41, 108, 1, 944, 800, 944, 0, NULL, NULL, '2020-02-03 03:37:29', 'aqswhnqr', 'Overdue', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `fin_invoicetaxes`
--

CREATE TABLE `fin_invoicetaxes` (
  `itid` int(11) NOT NULL,
  `ivid` int(11) NOT NULL COMMENT 'invoiceid',
  `ipid` int(11) NOT NULL COMMENT 'invoice product id',
  `taxid` int(11) NOT NULL,
  `tamt` double NOT NULL COMMENT 'percentage or amount',
  `taxtype` enum('Percentage','Amount') COLLATE utf8_unicode_ci NOT NULL,
  `taxamount` double NOT NULL,
  `currency` int(11) NOT NULL,
  `primarycurrency` int(11) NOT NULL,
  `exchange_rate` double NOT NULL,
  `totalamount` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_invoicetaxes`
--

INSERT INTO `fin_invoicetaxes` (`itid`, `ivid`, `ipid`, `taxid`, `tamt`, `taxtype`, `taxamount`, `currency`, `primarycurrency`, `exchange_rate`, `totalamount`) VALUES
(74, 15, 83, 17, 10, 'Percentage', 3.168, 41, 41, 1, 3.168),
(75, 15, 84, 17, 10, 'Percentage', 0.98, 41, 41, 1, 0.98),
(76, 15, 84, 18, 5, 'Percentage', 0.49, 41, 41, 1, 0.49),
(153, 16, 270, 16, 5, 'Percentage', 4.5, 41, 41, 1, 4.5),
(154, 38, 433, 21, 10, 'Percentage', 100, 41, 41, 1, 100),
(155, 38, 434, 21, 10, 'Percentage', 1000, 41, 41, 1, 1000),
(156, 39, 435, 21, 10, 'Percentage', 200, 41, 41, 1, 200),
(157, 39, 436, 21, 10, 'Percentage', 200, 41, 41, 1, 200),
(158, 40, 437, 21, 10, 'Percentage', 200, 41, 41, 1, 200),
(159, 40, 438, 21, 10, 'Percentage', 100, 41, 41, 1, 100),
(160, 40, 439, 21, 10, 'Percentage', 300, 41, 41, 1, 300),
(175, 43, 454, 23, 10, 'Percentage', 10, 108, 108, 1, 10),
(176, 43, 455, 23, 10, 'Percentage', 300, 108, 108, 1, 300),
(179, 44, 458, 23, 10, 'Percentage', 300, 108, 108, 1, 300),
(180, 44, 459, 23, 10, 'Percentage', 10, 108, 108, 1, 10),
(181, 45, 460, 23, 10, 'Percentage', 300, 108, 108, 1, 300),
(182, 45, 461, 23, 50, 'Amount', 50, 108, 108, 1, 50),
(186, 49, 468, 25, 10, 'Percentage', 50, 108, 108, 1, 50),
(187, 49, 469, 25, 10, 'Percentage', 35, 108, 108, 1, 35),
(189, 50, 471, 25, 10, 'Percentage', 50, 100, 108, 0.632831, 31.64155),
(195, 59, 490, 27, 15, 'Percentage', 20250, 117, 117, 1, 20250),
(196, 59, 491, 27, 15, 'Percentage', 9975, 117, 117, 1, 9975),
(197, 59, 492, 27, 15, 'Percentage', 6000, 117, 117, 1, 6000),
(198, 60, 493, 28, 15, 'Percentage', 300, 117, 117, 1, 300),
(199, 61, 494, 28, 15, 'Percentage', 28800, 117, 117, 1, 28800),
(200, 62, 495, 26, 18, 'Percentage', 36, 105, 108, 1, 36),
(201, 63, 496, 26, 18, 'Percentage', 144, 41, 108, 1, 144);

-- --------------------------------------------------------

--
-- Table structure for table `fin_journalentries`
--

CREATE TABLE `fin_journalentries` (
  `jeid` int(11) NOT NULL,
  `uid` int(11) NOT NULL COMMENT 'userid',
  `bid` int(11) NOT NULL COMMENT 'business id',
  `jid` int(11) NOT NULL COMMENT 'journal id',
  `udid` int(11) NOT NULL COMMENT 'user account default id',
  `agid` int(11) DEFAULT NULL COMMENT 'Account group id',
  `credit` double DEFAULT NULL,
  `debit` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_journalentries`
--

INSERT INTO `fin_journalentries` (`jeid`, `uid`, `bid`, `jid`, `udid`, `agid`, `credit`, `debit`) VALUES
(73, 10, 5, 11, 58, 1, 100, NULL),
(74, 10, 5, 11, 59, 1, 50, NULL),
(75, 10, 5, 11, 61, 2, NULL, 100),
(76, 10, 5, 11, 65, 4, 50, NULL),
(77, 10, 5, 11, 71, 5, NULL, 100),
(104, 8, 4, 16, 52, NULL, 100, NULL),
(105, 8, 4, 16, 88, NULL, NULL, 100),
(556, 9, 3, 24, 25, NULL, NULL, 100),
(557, 9, 3, 24, 28, NULL, 100, NULL),
(558, 9, 3, 24, 28, NULL, NULL, 100),
(559, 9, 3, 24, 19, NULL, 100, NULL),
(560, 9, 3, 24, 22, NULL, NULL, 100),
(561, 9, 3, 24, 34, NULL, 100, NULL),
(562, 9, 3, 24, 35, NULL, NULL, 100),
(563, 9, 3, 24, 25, NULL, 100, NULL),
(564, 9, 3, 24, 85, NULL, NULL, 100),
(565, 9, 3, 24, 27, NULL, 100, NULL),
(566, 9, 3, 24, 30, NULL, NULL, 100),
(567, 9, 3, 24, 28, NULL, 100, NULL),
(568, 9, 3, 24, 56, NULL, NULL, 100),
(569, 9, 3, 24, 20, NULL, 100, NULL),
(570, 9, 3, 24, 30, NULL, NULL, 100),
(571, 9, 3, 24, 32, NULL, 100, NULL),
(572, 9, 3, 24, 33, NULL, NULL, 100),
(573, 9, 3, 24, 25, NULL, 100, NULL),
(574, 9, 3, 25, 19, NULL, NULL, 100),
(575, 9, 3, 25, 22, NULL, 100, NULL),
(576, 11, 6, 26, 92, 1, 1, 1),
(577, 11, 6, 26, 93, 1, 1, 1),
(578, 11, 6, 26, 94, 1, 1, 1),
(579, 11, 6, 26, 107, 1, 1, 1),
(580, 11, 6, 26, 95, 2, 1, 1),
(581, 11, 6, 26, 96, 2, 1, 1),
(582, 11, 6, 26, 97, 3, 1, 1),
(583, 11, 6, 26, 98, 3, 1, 1),
(584, 11, 6, 26, 108, 3, 1, 1),
(585, 11, 6, 26, 99, 4, 1, 1),
(586, 11, 6, 26, 100, 4, 1, 1),
(587, 11, 6, 26, 101, 4, 1, 1),
(588, 11, 6, 26, 102, 4, 1, 1),
(589, 11, 6, 26, 103, 4, 1, 1),
(590, 11, 6, 26, 104, 4, 1, 1),
(591, 11, 6, 26, 109, 4, 1, 1),
(592, 11, 6, 26, 105, 5, 1, 1),
(593, 11, 6, 26, 106, 5, 1, 1),
(594, 12, 7, 27, 111, NULL, 100, 200),
(595, 12, 7, 27, 113, NULL, 150, 100),
(596, 12, 7, 27, 110, NULL, 250, 200),
(597, 12, 7, 27, 110, NULL, 500, 500),
(608, 16, 10, 29, 163, NULL, 100, NULL),
(609, 16, 10, 29, 165, NULL, 200, NULL),
(610, 16, 10, 29, 169, NULL, NULL, 100),
(611, 16, 10, 29, 170, NULL, 500, NULL),
(612, 16, 10, 29, 177, NULL, NULL, 700),
(614, 19, 12, 31, 204, 1, 500, NULL),
(615, 19, 12, 31, 207, 2, NULL, 500),
(616, 19, 12, 31, 211, 4, 500, NULL),
(617, 19, 12, 31, 218, 5, NULL, 500),
(637, 27, 19, 35, 355, 1, 500000, NULL),
(638, 27, 19, 35, 342, 3, NULL, 500000),
(639, 26, 18, 36, 320, NULL, 500, NULL),
(640, 26, 18, 36, 321, NULL, NULL, 500);

-- --------------------------------------------------------

--
-- Table structure for table `fin_journals`
--

CREATE TABLE `fin_journals` (
  `jid` int(11) NOT NULL,
  `uid` int(11) NOT NULL COMMENT 'userid',
  `bid` int(11) NOT NULL COMMENT 'business id',
  `description` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `journaldate` date NOT NULL,
  `createddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Active','Inactive','Trash') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_journals`
--

INSERT INTO `fin_journals` (`jid`, `uid`, `bid`, `description`, `journaldate`, `createddate`, `key`, `status`) VALUES
(11, 10, 5, 'Open Balance', '2013-10-31', '2013-11-08 10:43:45', 'fi06hkfs', 'Active'),
(16, 8, 4, 'Test', '2013-11-26', '2013-11-27 10:31:16', 'pj6yjyl6', 'Active'),
(24, 9, 3, 'Journal', '2013-11-28', '2013-11-29 06:34:35', 'aphw3ddd', 'Active'),
(25, 9, 3, 'Journal 2', '2013-11-28', '2013-11-29 07:12:18', 'pe9ktnl3', 'Active'),
(26, 11, 6, 'ledger description', '2014-01-06', '2014-01-13 12:27:25', 'b1bbhvhe', 'Active'),
(27, 12, 7, 'ledger description', '2014-03-04', '2014-03-04 06:55:52', '26d7l6a0', 'Active'),
(29, 16, 10, 'ledger description', '2014-03-05', '2014-03-06 12:21:08', '6uf0j0lo', 'Active'),
(31, 19, 12, 'payment1', '2014-03-01', '2014-03-24 12:21:48', 'sdlc5vio', 'Active'),
(35, 27, 19, 'entries', '2019-06-11', '2019-06-11 20:07:40', 'h4t11w8q', 'Active'),
(36, 26, 18, 'To my bix', '2019-07-22', '2019-07-22 18:48:27', '6zu775fr', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `fin_languages`
--

CREATE TABLE `fin_languages` (
  `lid` int(11) NOT NULL,
  `language` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_languages`
--

INSERT INTO `fin_languages` (`lid`, `language`, `code`, `status`) VALUES
(1, 'English', 'en', 'Active'),
(2, 'Turkey', 'tr', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `fin_products`
--

CREATE TABLE `fin_products` (
  `pid` int(11) NOT NULL,
  `bid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `sell` int(11) NOT NULL COMMENT 'invoice products',
  `sell_cat` int(11) DEFAULT NULL,
  `buy` int(11) NOT NULL COMMENT 'billing products',
  `buy_cat` int(11) DEFAULT NULL,
  `createddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive','Trash') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_products`
--

INSERT INTO `fin_products` (`pid`, `bid`, `uid`, `name`, `description`, `price`, `sell`, `sell_cat`, `buy`, `buy_cat`, `createddate`, `status`) VALUES
(2, 5, 10, 'mobile1', 'dsfsdaff', 32, 1, 64, 0, 0, '2013-10-03 05:29:36', 'Active'),
(51, 4, 8, 'Water cane', '', 20, 0, NULL, 1, 195, '2013-10-08 08:02:05', 'Trash'),
(59, 4, 8, 'Mobile', 'THis is mobile', 2500, 1, 7, 0, NULL, '2013-10-08 08:17:15', 'Active'),
(63, 3, 9, 'testing', 'testing', 25, 1, 287, 0, NULL, '2013-10-08 08:24:30', 'Trash'),
(64, 3, 9, 'mobile', '', 32, 1, 85, 0, NULL, '2013-10-08 08:24:54', 'Active'),
(65, 4, 8, 'Keyboard', '', 100, 1, 7, 0, NULL, '2013-10-08 09:21:30', 'Active'),
(66, 4, 8, 'Chair', '', 150, 1, 7, 0, NULL, '2013-10-08 09:22:07', 'Active'),
(67, 3, 9, 'mouse', 'mouse product', 32, 1, 25, 1, 31, '2013-10-08 09:22:12', 'Active'),
(69, 4, 8, 'Ticket', '', 50, 0, NULL, 1, 10, '2013-10-09 11:38:06', 'Active'),
(70, 3, 9, 'mobile', 'fgdf g df dfg d', 10, 1, 85, 0, NULL, '2013-10-10 05:46:06', 'Active'),
(71, 3, 9, 'mobile recharge', 'mobile recharge', 100, 1, NULL, 0, 214, '2013-10-10 07:33:24', 'Trash'),
(72, 3, 9, 'testing', 'testing', 10, 0, NULL, 1, 87, '2013-10-10 07:39:58', 'Active'),
(73, 3, 9, 'dsfsdafsf', '', 10, 1, 25, 0, NULL, '2013-10-10 07:43:56', 'Active'),
(83, 3, 9, 'dsfasdf', 'dsfasdf', 32, 0, NULL, 1, 29, '2013-10-10 08:10:32', 'Active'),
(84, 3, 9, 'sdfsdfdsfa', '', 10, 0, NULL, 1, 230, '2013-10-10 08:12:16', 'Trash'),
(85, 3, 9, 'mobile service', 'mobile service', 235, 0, NULL, 1, 227, '2013-10-10 08:16:42', 'Trash'),
(86, 3, 9, 'fdgsd  ', 'fdg dfgdf g', 235, 0, NULL, 1, 111, '2013-10-10 08:17:42', 'Trash'),
(87, 3, 9, 'sdfdas fsf ', 'sdfsdf df', 10, 0, NULL, 1, 233, '2013-10-10 08:18:07', 'Trash'),
(88, 3, 9, 'mobile123', 'mobile123', 23, 0, NULL, 1, 28, '2013-10-10 09:36:52', 'Active'),
(89, 3, 9, 'dsfsdfdsfsd', 'fdsfsdfsf', 10, 1, 331, 0, NULL, '2013-10-10 13:45:58', 'Trash'),
(90, 3, 9, 'mobile', '', 10, 0, NULL, 1, 31, '2013-10-12 05:55:41', 'Active'),
(91, 3, 9, 'New product', 'New product', 10, 1, 24, 1, 30, '2013-10-17 06:24:22', 'Active'),
(92, 4, 8, 'Landing Page', '', 11050, 1, 7, 0, NULL, '2013-10-22 07:45:40', 'Active'),
(93, 4, 8, 'New Product', '', 500, 0, NULL, 1, 11, '2013-10-31 11:42:42', 'Active'),
(94, 3, 9, 'costgood', '', 20, 0, NULL, 1, 56, '2013-11-07 07:45:45', 'Active'),
(95, 5, 10, 'Keyboard', '', 10, 1, 64, 0, NULL, '2013-11-08 10:21:37', 'Active'),
(96, 5, 10, 'Cost of goods', '', 10, 0, NULL, 1, 81, '2013-11-08 10:29:30', 'Active'),
(97, 5, 10, 'Note Books', '', 20, 0, NULL, 1, 70, '2013-11-08 10:41:30', 'Active'),
(98, 3, 9, 'testererre', 'testererre', 10, 1, 25, 0, NULL, '2013-11-21 06:35:24', 'Active'),
(99, 3, 9, 'testing sf asdf fd sf', ' sdfa df df  sdf sfs fs sf ', 235, 1, 25, 0, NULL, '2013-11-21 06:36:06', 'Active'),
(100, 4, 8, 'Austral', '', 42.29, 1, 7, 0, NULL, '2013-11-25 12:32:33', 'Active'),
(101, 4, 8, 'BV', '', 47.17, 1, 7, 0, NULL, '2013-11-25 12:32:50', 'Active'),
(102, 4, 8, 'FCS', '', 3.11, 1, 7, 0, NULL, '2013-11-25 12:33:03', 'Active'),
(103, 4, 8, 'CENT PLY', '', 73.5, 1, 7, 0, NULL, '2013-11-25 12:39:15', 'Active'),
(104, 4, 8, 'GUR NRE', '', 47.84, 1, 7, 0, NULL, '2013-11-25 12:39:43', 'Active'),
(105, 4, 8, 'KFA', '', 38.26, 1, 7, 0, NULL, '2013-11-25 12:40:05', 'Active'),
(106, 4, 8, 'PBA', '', 97.47, 1, 7, 0, NULL, '2013-11-25 12:40:20', 'Active'),
(107, 6, 11, 'mobile', 'product description goes here', 10000, 1, 98, 0, NULL, '2014-01-13 11:39:30', 'Active'),
(108, 6, 11, 'iphone5s', 'iphone 5 is the latest of apple product', 45000, 0, NULL, 1, 104, '2014-01-13 12:03:58', 'Active'),
(109, 7, 12, 'nokia 720', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n ', 1000, 1, 116, 0, NULL, '2014-03-04 06:25:59', 'Active'),
(110, 7, 12, 'nokia 820', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n ', 2000, 1, 116, 0, NULL, '2014-03-04 06:26:19', 'Active'),
(111, 7, 12, 'nokia 920', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n ', 3000, 1, 116, 0, NULL, '2014-03-04 06:26:43', 'Active'),
(112, 7, 12, 'sony xpeira 1', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n ', 1000, 0, NULL, 1, 122, '2014-03-04 06:27:33', 'Active'),
(113, 7, 12, 'sony xpeira 2', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n ', 2000, 0, NULL, 1, 122, '2014-03-04 06:27:51', 'Active'),
(114, 7, 12, 'nokia 1020', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n ', 3000, 0, NULL, 1, 122, '2014-03-04 06:28:12', 'Active'),
(115, 7, 12, 'sony xpeira 3', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n ', 10000, 1, 116, 1, 122, '2014-03-04 06:29:42', 'Active'),
(116, 8, 13, 'nokia 1020', 'nokia 1020', 150, 1, 135, 0, NULL, '2014-03-06 04:33:58', 'Trash'),
(117, 8, 13, 'nokia 1020', 'nokia 1020', 100, 1, 135, 0, NULL, '2014-03-06 04:34:35', 'Trash'),
(118, 8, 13, 'nokia 720', 'nokia 1020', 1000, 1, 135, 0, NULL, '2014-03-06 04:34:51', 'Trash'),
(119, 8, 13, 'nokia 820', 'nokia 1020', 2000, 1, 135, 0, NULL, '2014-03-06 04:36:33', 'Trash'),
(120, 8, 13, 'sony xpeira 1', 'nokia 1020', 2000, 0, NULL, 1, 141, '2014-03-06 04:36:49', 'Trash'),
(121, 8, 13, 'sony xpeira 2', 'nokia 1020', 1000, 1, 135, 1, 141, '2014-03-06 04:37:14', 'Trash'),
(122, 8, 13, 'sony xpeira 3', 'nokia 1020', 3000, 1, 135, 1, 141, '2014-03-06 04:37:43', 'Trash'),
(123, 10, 16, 'Web Designs', 'he digital world needs an adaptive User Experience We create it\r\nWe Prefer Long-Term App Development Relationships\r\n', 100, 1, 169, 1, 175, '2014-03-06 12:11:31', 'Active'),
(124, 10, 16, 'Web devolpment', 'he digital world needs an adaptive User Experience We create it\r\nWe Prefer Long-Term App Development Relationships\r\n', 3000, 1, 169, 0, NULL, '2014-03-06 12:15:02', 'Active'),
(125, 12, 19, 'Cpu', 'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 400, 1, 210, 0, NULL, '2014-03-24 09:44:40', 'Active'),
(126, 12, 19, 'web designs', 'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 500, 1, 210, 1, 214, '2014-03-24 09:45:15', 'Active'),
(127, 12, 19, 'Seo', 'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 350, 1, 210, 1, 213, '2014-03-24 09:47:10', 'Active'),
(128, 17, 24, 'Test Product', 'Product', 1000, 1, 289, 0, NULL, '2016-08-20 06:40:34', 'Active'),
(129, 17, 24, 'test product2', 'test', 2000, 1, 288, 0, NULL, '2016-08-20 08:36:27', 'Active'),
(130, 17, 24, 'test', '', 2000, 0, NULL, 1, 294, '2016-08-20 10:14:05', 'Active'),
(131, 8, 13, 'bumpher alto', '', 2400, 1, 135, 0, NULL, '2019-04-29 03:29:18', 'Active'),
(132, 8, 13, 'wheel cover 12\"', '', 800, 1, 135, 0, NULL, '2019-04-29 03:30:50', 'Active'),
(133, 8, 13, 'BLK', 'Black Label', 200, 1, 311, 0, 138, '2019-06-03 21:41:06', 'Active'),
(134, 18, 26, 'BLK', 'Black label case', 300, 1, 320, 1, 330, '2019-06-04 13:53:56', 'Active'),
(135, 18, 26, 'clq', 'Castle lite case', 350, 1, 320, 1, 330, '2019-06-04 18:03:43', 'Active'),
(136, 18, 26, 'hdq', 'Hunters dry Q', 400, 1, 320, 0, NULL, '2019-06-04 18:06:55', 'Active'),
(137, 19, 27, 'BLK', 'Black label Cases', 200, 1, 342, 1, 354, '2019-06-11 19:57:35', 'Active'),
(138, 8, 13, 'dD', 'fafddaf', 44, 0, NULL, 1, 310, '2019-07-22 12:57:37', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `fin_slidercontents`
--

CREATE TABLE `fin_slidercontents` (
  `scid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `lan` varchar(5) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_slidercontents`
--

INSERT INTO `fin_slidercontents` (`scid`, `sid`, `title`, `description`, `lan`) VALUES
(2, 1, 'Unlimited invoicing, <span>100% free.</span>', 'Send customized, professional invoices, and get paid fast. From recurring to one-time invoicing, it&#39;s free, no matter how many customers you have, or how many invoices you send', 'en'),
(3, 2, 'Creative <span>Control</span>', 'Create unique sliders using CSS3 transitions', 'en'),
(4, 3, 'Cutting Edge', 'Supports modern browsers, old browsers (IE7+), touch devices and responsive designs', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `fin_sliders`
--

CREATE TABLE `fin_sliders` (
  `sid` int(11) NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive','Trash') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_sliders`
--

INSERT INTO `fin_sliders` (`sid`, `image`, `created_date`, `status`) VALUES
(1, '5264d057-b9c4-4911-ab89-0934c0a80228.png', '2013-10-21 06:57:28', 'Active'),
(2, '5264dd13-b520-418d-a660-0934c0a80228.png', '2013-10-21 07:51:49', 'Active'),
(3, '5264dd22-b29c-4fba-92a4-0934c0a80228.png', '2013-10-21 07:52:04', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `fin_staffs`
--

CREATE TABLE `fin_staffs` (
  `sid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `bid` int(11) NOT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `timings` enum('Hourly','Daily','Monthly','Weekly') COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `address1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `zip` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive','Trash') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_staffs`
--

INSERT INTO `fin_staffs` (`sid`, `uid`, `bid`, `firstname`, `lastname`, `email`, `price`, `timings`, `phone`, `mobile`, `address1`, `address2`, `city`, `state`, `country`, `zip`, `createdate`, `status`) VALUES
(1, 8, 4, 'Sheik ', 'Dawood', 'test002.sbs@gmail.com', 10.00, 'Hourly', '', '', '', '', '', '', '', '', '2013-10-03 13:42:41', 'Active'),
(2, 8, 4, 'Mani', 'Kandan', 'test003.sbs@gmail.com', 15.00, 'Hourly', '', '', '', '', '', '', '', '', '2013-10-04 05:09:55', 'Active'),
(3, 13, 8, 'sample1', 'sample1', 'testk9789@gmail.com', 150.00, 'Hourly', '', '', '', '', '', '', '', '', '2014-03-06 04:39:40', 'Trash'),
(4, 13, 8, 'Mohamed', 'Ndomoni', 'mohamed.ndomoni@gmail.com', 400000.00, 'Hourly', '', '', '', '', '', '', '', '', '2014-03-06 04:40:10', 'Active'),
(5, 16, 10, 'senthil', 'kumar', 'zenthilengineer.1990@gmail.com', 78.00, 'Hourly', '', '', '', '', '', '', '', '', '2014-03-07 06:00:18', 'Trash'),
(6, 13, 8, 'beeltha banda', 'Banda', 'f@gmail.com', 29000.00, 'Hourly', '', '', '', '', '', '', '', '', '2019-06-03 21:43:23', 'Trash'),
(7, 27, 19, 'beeltha banda', 'Banda', 'fgg@gmail.com', 5000.00, 'Hourly', '', '', '', '', '', '', '188', '', '2019-06-11 06:59:37', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `fin_staticpagecontents`
--

CREATE TABLE `fin_staticpagecontents` (
  `scid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `pagename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `metadescription` text COLLATE utf8_unicode_ci NOT NULL,
  `metakeyword` text COLLATE utf8_unicode_ci NOT NULL,
  `heading` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `lan` varchar(5) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_staticpagecontents`
--

INSERT INTO `fin_staticpagecontents` (`scid`, `sid`, `pagename`, `title`, `metadescription`, `metakeyword`, `heading`, `content`, `lan`) VALUES
(1, 1, 'Home', 'Home', 'Home', 'Home', 'Home', '<h2>\r\n	Everything you need in one place</h2>\r\n<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s <span>standard</span> dummy text ever since the<span> 1500s,</span></p>\r\n', 'en'),
(2, 2, 'About Us', 'About Us', 'About Us', 'About Us', 'About Us', '<h2 >We make</h2>\r\n              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s <span>standard</span> dummy text ever since the<span> 1500s,</span></p>', 'en'),
(3, 3, 'Terms and conditions', 'Terms and conditions', 'Terms and conditions', 'Terms and conditions', 'Terms and conditions', '<h1>\r\n	Privacy Policy Updates</h1>\r\n<p>\r\n	At Listemix we are constantly working to improve the user experience. These changes, along with the ever-changing nature of the Internet, may cause Listemix to update this Privacy Policy from time to time, and in Listemix&#39;s sole discretion. If we do, we will notify you by posting the amended policy on the Website or by emailing you of the nature of the modifications along with a link to the modified document so that you can review it. If any modification is unacceptable to you, you shall cease using this Website. If you do not cease using this Website, you will be deemed to have accepted the change(s). In all cases, use of information we collect now is subject to the Privacy Policy in effect at the time such information is collected.</p>\r\n<h1>\r\n	Listemix Visitors</h1>\r\n<p>\r\n	Like most website operators, Listemix also collects non-personally identifying information of the sort that web browsers and servers typically make available, such as the browser type, language preference, referring site, and the date and time of each visitor request. Listemix collects non-personally identifying information in order to better understand how Listemix&#39;s visitors use its Website. From time to time, Listemix may release non-personally-identifying information in the aggregate, e.g., by publishing a report on trends in the usage of its Website.<br />\r\n	Listemix also collects potentially personally identifying information like Internet Protocol (IP) addresses. Such information is not collected with the intent to identify Listemix visitors, however, and does not disclose such information, other than under the same circumstances that it uses and discloses personally identifying information, as described below. Listemix may collect and, on any page, display the total counts that the page has been viewed. Listemix allows for the RSS syndication of all of its public content within the Listemix website.</p>\r\n<h1>\r\n	User Submitted Information</h1>\r\n<p>\r\n	When visiting Listemix you may choose to submit information such as photos, videos, interests, social connections, and more. You may also be prompted to provide information to help you leverage your online social network through our third party sharing services, amongst other things.<br />\r\n	When you create a Listemix account, some information about your account and your account activity may be provided to other Listemix users. This may include the date you opened your account, the date you last logged into your account, your birthday (if you choose to make it public), and various other information</p>\r\n<p>\r\n	<br />\r\n	Your Listemix account name, not your email address, is displayed to other users when you engage in certain activities on Listemixr. Other users can contact you by leaving a message or comment on the Website.You may also choose to add personal information that may include your name, gender, profile picture or other details, that will be visible to other users on your Listemix account page</p>\r\n<h1>\r\n	Collection and Use of Personally Identifiable Information</h1>\r\n<p>\r\n	If you so choose, you can visit our Website without revealing any personally identifiable information about yourself. However, certain visitors to the Website choose to interact with Listemix in ways that require Listemix to gather personally identifying information. The amount and type of information that Listemix gathers depends on the nature of the interaction. For example, in certain sections of this Website we may invite you to contact us with questions or comments or feedback, participate in list creation and/or voting, questionnaires or contests, recommend or rate content, or complete a registration form. In each case, Listemix collects only such information as is necessary or appropriate to fulfill the purpose of the visitor&#39;s interaction with Listemix. Listemix does not disclose personally identifying information other than as described below. And visitors can always refuse to supply personally identifying information.<br />\r\n	<br />\r\n	We may use and disclose your information if we believe, in good faith, that it is appropriate or necessary: to take precautions against liability; to protect Listemix from fraudulent, abusive, or unlawful uses; to investigate and defend ourselves against third-party claims or allegations; to assist government enforcement agencies; to protect the security or integrity of the Website; or t; conduct data analysis and create reports; offer certain functionality and assist us in improving the Website and creating new features.</p>\r\n', 'en'),
(4, 4, 'Privacy Policy', 'Privacy Policy', '', '', 'Privacy Policy', '', 'en'),
(5, 2, 'hakkımızda', 'hakkımızda', 'hakkımızda', 'hakkımızda', 'hakkımızda', '<span id=\"result_box\" lang=\"tr\"><span class=\"hps\">Lorem Ipsum</span> <span class=\"hps\">sadece</span> <span class=\"hps\">baskı ve</span> <span class=\"hps\">dizgi</span> <span class=\"hps\">end&uuml;strisinin</span> <span class=\"hps\">kukla</span> <span class=\"hps\">metindir.</span> <span class=\"hps\">Lorem Ipsum</span> <span class=\"hps\">hi&ccedil;</span> <span class=\"hps\">bilinmeyen bir</span> <span class=\"hps\">yazıcı</span> <span class=\"hps\">tipi bir kadırga</span> <span class=\"hps\">aldı ve</span> <span class=\"hps\">bir t&uuml;r &ouml;rnek</span> <span class=\"hps\">kitap yapmak i&ccedil;in şifreli</span> <span class=\"hps\">1500&#39;lerden</span> <span class=\"hps\">bu yana</span> <span class=\"hps\">end&uuml;stri standart kukla</span> <span class=\"hps\">metin</span> <span class=\"hps\">olmuştur</span><span>.</span> <span class=\"hps\">Bu</span> <span class=\"hps\">aslında</span> <span class=\"hps\">değişmeden</span> <span class=\"hps\">kalan</span><span>,</span> <span class=\"hps\">elektronik</span> <span class=\"hps\">dizgi</span> <span class=\"hps\">i&ccedil;ine</span> <span class=\"hps\">beş y&uuml;zyıl</span><span>,</span> <span class=\"hps\">aynı zamanda</span> <span class=\"hps\">sı&ccedil;rama</span> <span class=\"hps\">sadece</span> <span class=\"hps\">yaşamıştır</span><span>.</span> <span class=\"hps\">Bu</span> <span class=\"hps\">Lorem Ipsum</span> <span class=\"hps\">pasajları</span> <span class=\"hps\">i&ccedil;eren</span> <span class=\"hps\">Letraset</span> <span class=\"hps\">yaprak</span> <span class=\"hps\">s&uuml;r&uuml;m&uuml; ile</span> <span class=\"hps\">1960&#39;lı yıllarda</span> <span class=\"hps\">pop&uuml;ler oldu</span> <span class=\"hps\">ve son zamanlarda</span> <span class=\"hps\">Lorem Ipsum</span> <span class=\"hps\">s&uuml;r&uuml;mleri</span> <span class=\"hps\">dahil olmak &uuml;zere</span> <span class=\"hps\">Aldus</span> <span class=\"hps\">PageMaker</span> <span class=\"hps\">gibi</span> <span class=\"hps\">masa&uuml;st&uuml; yayıncılık yazılımı</span> <span class=\"hps\">ile</span><span>.</span></span>', 'tr'),
(6, 1, 'yuva', 'yuva', 'yuva', 'yuva', '', '', 'tr'),
(7, 3, 'Şartlar ve koşullar', 'Şartlar ve koşullar', '', '', 'Şartlar ve koşullar', '<span id=\"result_box\" lang=\"tr\"><span class=\"hps\">Lorem Ipsum</span> <span class=\"hps\">sadece</span> <span class=\"hps\">baskı ve</span> <span class=\"hps\">dizgi</span> <span class=\"hps\">end&uuml;strisinin</span> <span class=\"hps\">kukla</span> <span class=\"hps\">metindir.</span> <span class=\"hps\">Lorem Ipsum</span> <span class=\"hps\">hi&ccedil;</span> <span class=\"hps\">bilinmeyen bir</span> <span class=\"hps\">yazıcı</span> <span class=\"hps\">tipi bir kadırga</span> <span class=\"hps\">aldı ve</span> <span class=\"hps\">bir t&uuml;r &ouml;rnek</span> <span class=\"hps\">kitap yapmak i&ccedil;in şifreli</span> <span class=\"hps\">1500&#39;lerden</span> <span class=\"hps\">bu yana</span> <span class=\"hps\">end&uuml;stri standart kukla</span> <span class=\"hps\">metin</span> <span class=\"hps\">olmuştur</span><span>.</span> <span class=\"hps\">Bu</span> <span class=\"hps\">aslında</span> <span class=\"hps\">değişmeden</span> <span class=\"hps\">kalan</span><span>,</span> <span class=\"hps\">elektronik</span> <span class=\"hps\">dizgi</span> <span class=\"hps\">i&ccedil;ine</span> <span class=\"hps\">beş y&uuml;zyıl</span><span>,</span> <span class=\"hps\">aynı zamanda</span> <span class=\"hps\">sı&ccedil;rama</span> <span class=\"hps\">sadece</span> <span class=\"hps\">yaşamıştır</span><span>.</span> <span class=\"hps\">Bu</span> <span class=\"hps\">Lorem Ipsum</span> <span class=\"hps\">pasajları</span> <span class=\"hps\">i&ccedil;eren</span> <span class=\"hps\">Letraset</span> <span class=\"hps\">yaprak</span> <span class=\"hps\">s&uuml;r&uuml;m&uuml; ile</span> <span class=\"hps\">1960&#39;lı yıllarda</span> <span class=\"hps\">pop&uuml;ler oldu</span> <span class=\"hps\">ve son zamanlarda</span> <span class=\"hps\">Lorem Ipsum</span> <span class=\"hps\">s&uuml;r&uuml;mleri</span> <span class=\"hps\">dahil olmak &uuml;zere</span> <span class=\"hps\">Aldus</span> <span class=\"hps\">PageMaker</span> <span class=\"hps\">gibi</span> <span class=\"hps\">masa&uuml;st&uuml; yayıncılık yazılımı</span> <span class=\"hps\">ile</span><span>.</span></span>', 'tr'),
(8, 4, 'Gizlilik Politikası', 'Gizlilik Politikası', '', '', 'Gizlilik Politikası', '<span id=\"result_box\" lang=\"tr\"><span class=\"hps\">Lorem Ipsum</span> <span class=\"hps\">sadece</span> <span class=\"hps\">baskı ve</span> <span class=\"hps\">dizgi</span> <span class=\"hps\">end&uuml;strisinin</span> <span class=\"hps\">kukla</span> <span class=\"hps\">metindir.</span> <span class=\"hps\">Lorem Ipsum</span> <span class=\"hps\">hi&ccedil;</span> <span class=\"hps\">bilinmeyen bir</span> <span class=\"hps\">yazıcı</span> <span class=\"hps\">tipi bir kadırga</span> <span class=\"hps\">aldı ve</span> <span class=\"hps\">bir t&uuml;r &ouml;rnek</span> <span class=\"hps\">kitap yapmak i&ccedil;in şifreli</span> <span class=\"hps\">1500&#39;lerden</span> <span class=\"hps\">bu yana</span> <span class=\"hps\">end&uuml;stri standart kukla</span> <span class=\"hps\">metin</span> <span class=\"hps\">olmuştur</span><span>.</span> <span class=\"hps\">Bu</span> <span class=\"hps\">aslında</span> <span class=\"hps\">değişmeden</span> <span class=\"hps\">kalan</span><span>,</span> <span class=\"hps\">elektronik</span> <span class=\"hps\">dizgi</span> <span class=\"hps\">i&ccedil;ine</span> <span class=\"hps\">beş y&uuml;zyıl</span><span>,</span> <span class=\"hps\">aynı zamanda</span> <span class=\"hps\">sı&ccedil;rama</span> <span class=\"hps\">sadece</span> <span class=\"hps\">yaşamıştır</span><span>.</span> <span class=\"hps\">Bu</span> <span class=\"hps\">Lorem Ipsum</span> <span class=\"hps\">pasajları</span> <span class=\"hps\">i&ccedil;eren</span> <span class=\"hps\">Letraset</span> <span class=\"hps\">yaprak</span> <span class=\"hps\">s&uuml;r&uuml;m&uuml; ile</span> <span class=\"hps\">1960&#39;lı yıllarda</span> <span class=\"hps\">pop&uuml;ler oldu</span> <span class=\"hps\">ve son zamanlarda</span> <span class=\"hps\">Lorem Ipsum</span> <span class=\"hps\">s&uuml;r&uuml;mleri</span> <span class=\"hps\">dahil olmak &uuml;zere</span> <span class=\"hps\">Aldus</span> <span class=\"hps\">PageMaker</span> <span class=\"hps\">gibi</span> <span class=\"hps\">masa&uuml;st&uuml; yayıncılık yazılımı</span> <span class=\"hps\">ile</span><span>.</span></span>', 'tr'),
(9, 5, 'faq', 'faq', '', '', 'faq', '', 'en'),
(10, 5, 'Sıkça sorulan soru', 'Sıkça sorulan soru', 'Sıkça sorulan soru', 'Sıkça sorulan soru', 'Sıkça sorulan soru', '<span id=\"result_box\" lang=\"tr\"><span title=\"Introduction\">Giriş</span><br />\r\n<span title=\"Welcome To Ranker!\">Erbaş Hoşgeldiniz!</span><br />\r\n<br />\r\n<span title=\"Ranker has lists on every topic you can think of, and features that make it easy and painless to make your own list and share it with the world!\">Erbaş aklınıza gelebilecek her konuda listeleri vardır ve kolay ve ağrısız kendi listesini yapmak ve d&uuml;nya ile paylaşmak i&ccedil;in yapmak &ouml;zellikler!</span><br />\r\n<br />\r\n<span title=\"Take Our Video Tours\">Bizim video Turlar alın</span><br />\r\n<span title=\"What is Ranker?\">Erbaş nedir?</span><br />\r\n<span title=\"How to Build and Re-Rank Lists on Ranker\">Erbaş &uuml;zerine inşa ve yeniden Rank Listesi Nasıl</span><br />\r\n<span title=\"The Ranker App\">Erbaş App</span><br />\r\n<span title=\"How Does Ranker Work?\">Erbaş nasıl &ccedil;alışır?</span><br />\r\n<br />\r\n<span title=\"Here\'s how it works:\">İşte nasıl &ccedil;alışır:<br />\r\n<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;</span><span title=\"Ranker has millions of items in its database - everything from universities to comic book heroes to candy brands to your favorite band\'s discography.\">&Ccedil;izgi roman kahramanları i&ccedil;in &uuml;niversitelerden en sevdiğiniz grubun diskografisi i&ccedil;in şeker marka i&ccedil;in her şeyi - erbaş kendi veritabanında &ouml;ğeleri milyonlarca var.<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;</span><span title=\"To make a list, all you have to do is tell us the topic and pick the items you want to include.\">Bir liste yapmak i&ccedil;in, yapmanız gereken tek şey bize konuyu anlatmak ve eklemek istediğiniz &ouml;ğeleri se&ccedil;in olduğunu. </span><span title=\"Ranker takes care of the rest.\">Erbaş gerisini halleder.<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;</span><span title=\"Once you\'ve built your own lists, we give you the tools to share them with your friends, or to open them up for voting and let the community decide the rankings!\">Kendi listeleri inşa ettik sonra, size arkadaşlarınızla paylaşmak i&ccedil;in ara&ccedil;ları sağlar, ya da oy i&ccedil;in onları a&ccedil;mak ve toplum sıralaması karar vermek!</span><br />\r\n<br />\r\n<span title=\"Why Should I Use Ranker?\">Neden erbaş kullanın mı?</span><br />\r\n<br />\r\n<span title=\"We thought this question might come up, so we made a list of the Reasons to Rank on Ranker\">Biz bu soruyu g&uuml;ndeme gelebileceğini d&uuml;ş&uuml;nd&uuml;m, bu y&uuml;zden erbaş Sıralaması i&ccedil;in nedenleri bir listesini yaptı</span><br />\r\n<span title=\"Does Ranker Charge?\">Erbaş Şarj mu?</span><br />\r\n<br />\r\n<span title=\"Nope, not a dime.\">Hayır, değil bir kuruş. </span><span title=\"We just want to help you make awesome lists.\">Biz sadece harika listeleri yapmak yardım etmek istiyorum.</span><br />\r\n<span title=\"So How Does Ranker Make Money?\">Yani nasıl erbaş Para Kazanmak mı?</span><br />\r\n<br />\r\n<span title=\"The international arms trade.\">Uluslararası silah ticareti.</span><br />\r\n<br />\r\n<span title=\"NO!\">HAYIR! </span><span title=\"I mean, we sell ads and we include links to where you buy things like music or movies that have been ranked on lists.\">Yani, reklam satabilecek ve m&uuml;zik veya listelerinde sıralanır olmuştur film gibi şeyler satın yere bağlantılar i&ccedil;erir.</span><br />\r\n<br />\r\n<span title=\"We\'d appreciate it if you disabled your ad blockers when on the site, and supported our great sponsors, to help us keep the lights on\">Sitede ve bizim i&ccedil;in b&uuml;y&uuml;k sponsorlar desteklenen yaparken reklamınızı blokerleri devre dışı eğer bize ışık tutmak i&ccedil;in, &ccedil;ok memnun olurum</span><br />\r\n<span title=\"Is There a Full List of Ranker\'s Rules of Conduct?\">Davranış erbaş en Kuralları Tam Listesi Var mı?</span><br />\r\n<br />\r\n<span title=\"Yes.\">Evet. </span><span title=\"Here\'s a link to Ranker\'s Terms and Conditions\">İşte erbaş en Şartları&#39;nı bir bağlantı</span><br />\r\n<span title=\"Browsing Ranker\">Erbaş Tarama</span><br />\r\n<span title=\"Do I Have to Make a Ranker Account?\">Ben bir erbaş Hesap yapmak zorunda mı?</span><br />\r\n<br />\r\n<span title=\"You can look around the site at all the lists and enjoy Ranker\'s great content without signing in or setting up an account.\">T&uuml;m listeleri site etrafına bakmak ve oturum ya da bir hesap a&ccedil;madan erbaş en b&uuml;y&uuml;k i&ccedil;erik keyfini &ccedil;ıkarabilirsiniz.</span><br />\r\n<br />\r\n<span title=\"However, if you want to vote, build lists or participate in any other ways, we ask that you please sign up and log in.\">Eğer, oy listeleri oluşturmak veya başka bir şekilde katılmak istiyorsanız Ancak, biz kaydolmak ve l&uuml;tfen giriş yapınız istiyoruz</span><br />\r\n<br />\r\n<span title=\"You can use Facebook, Twitter or Google to access the site if you don\'t want to create a new account.\">Yeni bir hesap oluşturmak istemiyorsanız siteye erişmek i&ccedil;in Facebook, Twitter veya Google kullanabilirsiniz. </span><span title=\"Just look for their icons next to the &quot;Signup&quot; button in the upper left hand corner of any Ranker page, or click Signup and you\'ll be given the option of which service to use.\">Sadece herhangi bir erbaş sayfanın sol &uuml;st k&ouml;şesindeki &quot;&Uuml;ye Ol&quot; d&uuml;ğmesinin yanındaki simgeleri aramak, ya da Kayıt tıklayın ve kullanmak i&ccedil;in hangi hizmet se&ccedil;eneği sunulur.</span><br />\r\n<span title=\"Can I Leave Comments on Lists?\">Ben Listesi &uuml;zerine yorum miyim?</span><br />\r\n<br />\r\n<span title=\"Yes!\">Evet! </span><span title=\"Please do!\">L&uuml;tfen!</span><br />\r\n<br />\r\n<span title=\"We use Disqus for our comments section, so if you are already registered with them, you can simply type your comment and then use that user/password to post it.\">Biz zaten onlarla kayıtlı eğer &ouml;yleyse, sadece yorumunuzu yazabilirsiniz, bizim yorum b&ouml;l&uuml;m&uuml;nde i&ccedil;in Disqus kullanın ve sonra kullanıcı / parola post it i&ccedil;in kullanabilirsiniz. </span><span title=\"If you don\'t feel like doing that, or registering with Disqus, you can easily just post a comment with whatever name you choose.\">Bu, ya da Disqus ile kayıt yapıyor gibi hissetmiyorum, kolayca sadece se&ccedil;tiğiniz adı ile bir yorum g&ouml;nderebilir. </span><span title=\"All you have to do is enter your email (which will not be seen on the page).\">T&uuml;m yapmanız gereken (sayfasında g&ouml;r&uuml;lebilir olmayacak olan) e-posta girmektir.</span><br />\r\n<br />\r\n<span title=\"If you don\'t register with Disqus, you won\'t know when someone replies to your comment, but you can just check back the old-school way.\">Eğer Disqus ile kayıt yoksa, birisi yoruma cevap zaman bilemezsiniz, ama sadece eski okul yolu tekrar kontrol edebilirsiniz.</span></span>', 'tr');

-- --------------------------------------------------------

--
-- Table structure for table `fin_staticpages`
--

CREATE TABLE `fin_staticpages` (
  `sid` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo` int(11) NOT NULL,
  `pagecontent` int(11) NOT NULL,
  `pagelink` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_staticpages`
--

INSERT INTO `fin_staticpages` (`sid`, `name`, `seo`, `pagecontent`, `pagelink`) VALUES
(1, 'Home', 1, 1, 'home'),
(2, 'About Us', 1, 1, 'about-us'),
(3, 'Terms and conditions', 1, 1, 'terms-and-conditions'),
(4, '', 1, 1, 'privacy-policy'),
(5, 'faq', 1, 1, 'faq');

-- --------------------------------------------------------

--
-- Table structure for table `fin_taxes`
--

CREATE TABLE `fin_taxes` (
  `tid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `bid` int(11) NOT NULL,
  `taxname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `abbrivation` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `taxnumber` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `show` enum('No','Yes') COLLATE utf8_unicode_ci NOT NULL,
  `recoverable` enum('No','Yes') COLLATE utf8_unicode_ci NOT NULL,
  `taxrate` double(8,2) NOT NULL,
  `compound` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `createddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive','Trash') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_taxes`
--

INSERT INTO `fin_taxes` (`tid`, `uid`, `bid`, `taxname`, `abbrivation`, `description`, `taxnumber`, `show`, `recoverable`, `taxrate`, `compound`, `createddate`, `status`) VALUES
(13, 9, 3, 'VAT', 'Value added Tax', 'Value added Tax', '432343', 'No', 'Yes', 12.00, 'No', '2013-10-15 10:28:47', 'Active'),
(14, 9, 3, 'SAT', 'Service tax', 'Service tax', '', 'Yes', 'Yes', 10.00, 'No', '2013-10-17 11:18:14', 'Active'),
(15, 9, 3, 'GTax', 'Ganesan tax', '', '', 'No', 'No', 12.00, 'No', '2013-10-17 11:19:19', 'Trash'),
(16, 8, 4, 'VAT', 'VAT', 'Value Added Tax', '', 'No', 'No', 5.00, 'No', '2013-10-25 05:03:06', 'Active'),
(17, 10, 5, 'VAT', 'Value added tax', '', '432343', 'Yes', 'Yes', 10.00, 'No', '2013-11-08 10:23:58', 'Active'),
(18, 10, 5, 'SAT', 'Service tax', '', '4323432', 'Yes', 'No', 5.00, 'No', '2013-11-08 10:24:18', 'Active'),
(19, 9, 3, 'CTAXt', 'Value Added', 'dfds afdsafds', '12', 'No', 'No', 3.00, 'No', '2013-11-08 10:53:39', 'Active'),
(20, 8, 4, 'VAT(R)', 'VAT(R)', 'VAT(R).............................', 'TAXVAT321', 'Yes', 'Yes', 4.30, 'No', '2013-11-09 05:12:18', 'Active'),
(21, 12, 7, 'Tax', 'Tax 1', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n ', '123', 'Yes', 'Yes', 10.00, 'No', '2014-03-04 06:31:57', 'Active'),
(22, 13, 8, 'Tax23', 'Tax 123', 'nokia 1020', '123', 'Yes', 'No', 10.00, 'No', '2014-03-06 04:38:50', 'Trash'),
(23, 16, 10, 'VAT', 'VAT TAX', 'he digital world needs an adaptive User Experience We create it\r\nWe Prefer Long-Term App Development Relationships\r\n', '123', 'Yes', 'Yes', 10.00, 'No', '2014-03-06 12:13:59', 'Active'),
(24, 16, 10, 'service', 'services', 'services', '111', 'Yes', 'Yes', 22.00, 'No', '2014-03-07 05:59:29', 'Trash'),
(25, 19, 12, 'VAT', 'VAT', 'to using \'Content here, content here\', making it look like readable English. Man', '1', 'Yes', 'Yes', 10.00, 'No', '2014-03-24 12:16:55', 'Active'),
(26, 13, 8, 'VAT', 'Value Add Tax', 'Value Add Tax', '123456789123456', 'Yes', 'No', 18.00, 'No', '2018-09-18 16:52:57', 'Active'),
(27, 26, 18, 'Standard VAT', 'VAT', '', '202090908', 'Yes', 'No', 15.00, 'No', '2019-06-04 14:08:41', 'Active'),
(28, 27, 19, 'Standard VAT', 'VAT', '', '123456789', 'No', 'No', 15.00, 'No', '2019-06-11 07:00:15', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `fin_testimonialcontents`
--

CREATE TABLE `fin_testimonialcontents` (
  `tcid` int(11) NOT NULL,
  `tid` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `lan` varchar(5) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_testimonialcontents`
--

INSERT INTO `fin_testimonialcontents` (`tcid`, `tid`, `description`, `lan`) VALUES
(1, 1, 'Cloud Account made our life so easy. In fact, it helped us to change the way we were working. Rather than sending and receiving email, we had now a \"central system\" to manage everything. ', 'en'),
(3, 3, 'This is my company..', 'en'),
(4, 3, 'This is turkey language for description....', 'tr');

-- --------------------------------------------------------

--
-- Table structure for table `fin_testimonials`
--

CREATE TABLE `fin_testimonials` (
  `tid` int(11) NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive','Trash') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_testimonials`
--

INSERT INTO `fin_testimonials` (`tid`, `image`, `name`, `company`, `created_date`, `status`) VALUES
(1, '5264fe42-72e8-42cd-99a6-0934c0a80228.jpg', 'jhonson', 'SK TECH', '2014-01-06 05:22:27', 'Active'),
(3, '52651000-f5f0-4384-bfcf-0934c0a80228.jpg', 'Amstrong', 'SBS & CO.,', '2014-01-06 05:22:56', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `fin_timetracks`
--

CREATE TABLE `fin_timetracks` (
  `trid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `prid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `task` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `duration` decimal(5,2) NOT NULL,
  `date` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Billed','Unbilled','Trash') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fin_transactions`
--

CREATE TABLE `fin_transactions` (
  `tid` int(11) NOT NULL,
  `uid` int(11) NOT NULL COMMENT 'user id',
  `bid` int(11) NOT NULL COMMENT 'business id',
  `bankid` int(11) DEFAULT NULL COMMENT 'bank id',
  `transdate` date NOT NULL,
  `description` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `category` int(11) DEFAULT NULL COMMENT 'Income and expense id from user default',
  `account` int(11) NOT NULL COMMENT 'Assest ,liabilites,equity from user default id',
  `amount` double NOT NULL COMMENT 'exclude tax amount',
  `currency` int(11) NOT NULL,
  `primarycurrency` int(11) NOT NULL,
  `exchange_rate` float NOT NULL,
  `total` double NOT NULL COMMENT 'include tax amount',
  `exchangetotal` double NOT NULL COMMENT 'include tax amount  multiply with exchange rate ',
  `exchangeamount` double NOT NULL COMMENT 'exclude tax with exchange amount',
  `trans_type` enum('Income','Expense') COLLATE utf8_unicode_ci NOT NULL COMMENT 'Transaction type',
  `customer` int(11) DEFAULT NULL COMMENT 'customer id',
  `vendor` int(11) DEFAULT NULL COMMENT 'vendor id',
  `invoice` int(11) DEFAULT NULL COMMENT 'Invoice id',
  `bill` int(11) DEFAULT NULL COMMENT 'Bill id',
  `tfrom` enum('Trans','Statement','Transfer') COLLATE utf8_unicode_ci NOT NULL,
  `chk_no` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `createddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_transactions`
--

INSERT INTO `fin_transactions` (`tid`, `uid`, `bid`, `bankid`, `transdate`, `description`, `category`, `account`, `amount`, `currency`, `primarycurrency`, `exchange_rate`, `total`, `exchangetotal`, `exchangeamount`, `trans_type`, `customer`, `vendor`, `invoice`, `bill`, `tfrom`, `chk_no`, `createddate`) VALUES
(477, 10, 5, NULL, '2013-11-08', '', 58, 59, 9.9, 41, 41, 1, 9.9, 0, 0, 'Income', 113, NULL, 14, NULL, 'Trans', '', '2013-11-08 10:22:40'),
(478, 10, 5, NULL, '2013-02-20', 'DCARDFEE3568MAR12-FEB13+ST', 70, 77, 111.24, 10, 10, 1, 111.24, 111.24, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(479, 10, 5, NULL, '2013-02-27', 'BIL/410053405/PAYTM MOBILE SOLUTIO/90554549_PAY', 67, 77, 50, 10, 10, 1, 50, 50, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(480, 10, 5, NULL, '2013-03-01', '601601513987:Int.Pd:01-09-2012 to 28-02-2013', 64, 77, 86, 10, 10, 1, 86, 86, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(481, 10, 5, NULL, '2013-03-04', 'VIN/PAYPAL *FAN/20130304233628/0', 65, 77, 157.41, 10, 10, 1, 157.41, 157.41, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(482, 10, 5, NULL, '2013-03-12', 'INF/000006364416/Sheik sal feb 2013                                             ', 64, 77, 8, 10, 10, 1, 8, 8, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(483, 10, 5, NULL, '2013-03-12', 'BIL/415922679/sheik/NSP                                                         ', 68, 77, 8, 10, 10, 1, 8, 8, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(484, 10, 5, NULL, '2013-03-21', 'BIL/418872055/Bill Desk/AIRTELPREPAI_MI                                         ', 69, 77, 55, 10, 10, 1, 55, 55, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(485, 10, 5, NULL, '2013-03-25', 'VPS/PAYPAL *FRE/20130325053948/0                                                ', 65, 77, 303.76, 10, 10, 1, 303.76, 303.76, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(486, 10, 5, NULL, '2013-03-31', 'BIL/422336044/Bill Desk/AIRTELPREPAI_MI                                         ', 65, 77, 55, 10, 10, 1, 55, 55, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(487, 10, 5, NULL, '2013-04-01', 'BIL/422677627/Bill Desk/AIRTELPREPAI_MI                                         ', 65, 77, 235, 10, 10, 1, 235, 235, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(488, 10, 5, NULL, '2013-04-03', 'BIL/423840649/C.C.AVENUES/SHOPCLUES.COM-I                                       ', 69, 77, 38, 10, 10, 1, 38, 38, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(489, 10, 5, NULL, '2013-04-03', 'BIL/423983693/TATA SKY/TATASKY_MICI296                                          ', 70, 77, 350, 10, 10, 1, 350, 350, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(490, 10, 5, NULL, '2013-04-04', 'BIL/424213904/EBAY-PAISAPAY/33429158205                                         ', 67, 77, 19, 10, 10, 1, 19, 19, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(491, 10, 5, NULL, '2013-04-05', 'INF/000006509644/March 2013 Salary                                              ', 64, 77, 5, 10, 10, 1, 5, 5, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(492, 10, 5, NULL, '2013-04-05', 'BIL/424840654/sheik/NSP                                                         ', 65, 77, 6, 10, 10, 1, 6, 6, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(493, 10, 5, NULL, '2013-04-09', 'BIL/426220390/REDIFF.COM/7934733                                                ', 67, 77, 185, 10, 10, 1, 185, 185, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(494, 10, 5, NULL, '2013-04-24', 'BIL/431284087/Bill Desk/AIRTELPREPAI_MI                                         ', 68, 77, 110, 10, 10, 1, 110, 110, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(495, 10, 5, NULL, '2013-04-26', 'DCARDFEE3568MAR13-FEB14ST12.24                                                  ', 67, 77, 111.24, 10, 10, 1, 111.24, 111.24, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(496, 10, 5, NULL, '2013-05-10', 'INF/000006720251/April Salary 2013                                              ', 64, 77, 8, 10, 10, 1, 8, 8, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(497, 10, 5, NULL, '2013-05-12', 'BIL/438631012/sheik/NSP                                                         ', 70, 77, 8, 10, 10, 1, 8, 8, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(498, 10, 5, NULL, '2013-05-31', 'BIL/445375091/0127/919894067178/919894067178                                    ', 68, 77, 50, 10, 10, 1, 50, 50, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(499, 10, 5, NULL, '2013-06-10', 'BIL/449630026/sudha/NSP                                                         ', 69, 77, 1, 10, 10, 1, 1, 1, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(500, 10, 5, NULL, '2013-06-11', 'BIL/449850495/sudha/NSP                                                         ', 65, 77, 2, 10, 10, 1, 2, 2, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(501, 10, 5, NULL, '2013-06-13', 'INF/000006934846/Shiek May salary                                               ', 64, 77, 8, 10, 10, 1, 8, 8, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(502, 10, 5, NULL, '2013-06-15', 'BIL/451511672/PayU India/29799661                                               ', 67, 77, 598, 10, 10, 1, 598, 598, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(503, 10, 5, NULL, '2013-06-17', 'BIL/452148003/sheik/NSP                                                         ', 68, 77, 9, 10, 10, 1, 9, 9, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(504, 10, 5, NULL, '2013-06-23', 'BIL/453990759/0127/919894067178/919894067178                                    ', 69, 77, 50, 10, 10, 1, 50, 50, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(505, 10, 5, NULL, '2013-06-23', 'BIL/AUTORECON TID.000453990759/PREPAID MOBILE RECH                              ', 65, 77, 50, 10, 10, 1, 50, 50, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(506, 10, 5, NULL, '2013-06-24', 'BIL/454411781/Bill Desk/AIRTELPREPAI_MI                                         ', 70, 77, 50, 10, 10, 1, 50, 50, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(507, 10, 5, NULL, '2013-06-24', 'BIL/AUTORECON TID.000454403358                                                  ', 67, 77, 50, 10, 10, 1, 50, 50, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(508, 10, 5, NULL, '2013-06-24', 'BIL/AUTORECON TID.000454403358/Bill Desk                                        ', 68, 77, 50, 10, 10, 1, 50, 50, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(509, 10, 5, NULL, '2013-06-26', 'REV OF PMT ID 454411781                                                         ', 64, 77, 50, 10, 10, 1, 50, 50, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(510, 10, 5, NULL, '2013-07-08', 'BIL/460444154/BILL JUNCTION PAYMEN/76581688                                     ', 65, 77, 827.95, 10, 10, 1, 827.95, 827.95, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(511, 10, 5, NULL, '2013-07-13', 'BIL/462381409/Bill Desk/AIRTELPREPAI_MI                                         ', 69, 77, 50, 10, 10, 1, 50, 50, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(512, 10, 5, NULL, '2013-07-16', 'INF/000007141623/Sheik jun 2013 sal                                             ', 64, 77, 8, 10, 10, 1, 8, 8, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(513, 10, 5, NULL, '2013-07-17', 'ATM/CASH WDL/17-07-13/19:13:17/0                                                ', 67, 77, 8, 10, 10, 1, 8, 8, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(514, 10, 5, NULL, '2013-08-03', 'REV OF PMT ID 451511672                                                         ', 65, 77, 139, 10, 10, 1, 139, 139, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(515, 10, 5, NULL, '2013-08-08', 'INF/000007307898/Sheik July 2013salary                                          ', 64, 77, 8, 10, 10, 1, 8, 8, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(516, 10, 5, NULL, '2013-08-13', 'ATM/CASH WDL/13-08-13/19:21:25/0                                                ', 67, 77, 8, 10, 10, 1, 8, 8, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(517, 10, 5, NULL, '2013-09-02', '601601513987:Int.Pd:01-03-2013 to 31-08-2013                                    ', 69, 77, 83, 10, 10, 1, 83, 83, 0, 'Expense', NULL, NULL, NULL, NULL, 'Statement', '', '2013-11-08 10:48:46'),
(518, 10, 5, NULL, '2013-11-08', '', 64, 59, 0, 10, 10, 1, 100, 100, 100, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2013-11-08 10:50:02'),
(540, 9, 3, NULL, '2013-11-16', 'test', 25, 20, 1000, 4, 4, 1, 1000, 1000, 1000, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2013-11-16 14:33:55'),
(542, 9, 3, NULL, '2013-11-16', '', 25, 20, 10000, 41, 4, 1, 10000, 10000, 10000, 'Income', 73, NULL, NULL, NULL, 'Trans', '', '2013-11-16 14:47:53'),
(543, 9, 3, NULL, '2013-11-16', 'edsfadsf', 25, 20, 5000, 4, 4, 1, 5000, 5000, 5000, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2013-11-16 14:48:53'),
(547, 9, 3, NULL, '2013-11-18', '', 25, 20, 800, 4, 4, 1, 1000, 800, 1000, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2013-11-18 07:00:19'),
(561, 9, 3, NULL, '2013-11-19', '', 29, 20, 1000, 4, 4, 1, 1000, 1000, 1000, 'Expense', NULL, NULL, NULL, NULL, 'Trans', '', '2013-11-19 06:02:20'),
(562, 9, 3, NULL, '2013-11-19', '', 87, 20, 10, 4, 4, 1, 10, 10, 10, 'Expense', NULL, NULL, NULL, NULL, 'Trans', '', '2013-11-19 07:24:38'),
(565, 9, 3, NULL, '2013-11-19', '', 28, 20, 500, 4, 4, 1, 500, 500, 500, 'Expense', NULL, NULL, NULL, NULL, 'Trans', '', '2013-11-19 14:01:26'),
(575, 8, 4, NULL, '2013-11-20', '', 7, 2, 100, 29, 41, 10, 100, 1000, 100, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2013-11-20 10:25:25'),
(579, 9, 3, NULL, '2013-11-20', '', 25, 49, 5000, 4, 4, 1, 5000, 5000, 5000, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2013-11-20 12:28:01'),
(580, 9, 3, NULL, '2013-11-20', '', 28, 51, 1000, 4, 4, 1, 1000, 1000, 1000, 'Expense', NULL, NULL, NULL, NULL, 'Trans', '', '2013-11-20 12:28:20'),
(590, 8, 4, NULL, '2013-11-21', '', 4, 52, 146.5, 21, 21, 0.0325835, 146.5, 4.77348275, 0, 'Expense', NULL, 5, NULL, 28, 'Trans', '', '2013-11-21 12:56:59'),
(591, 8, 4, NULL, '2013-11-21', '', 4, 52, 500, 21, 21, 0.0325835, 500, 16.29175, 0, 'Expense', NULL, 5, NULL, 28, 'Trans', '', '2013-11-21 12:57:16'),
(592, 8, 4, NULL, '2013-11-21', '', 4, 52, 500, 21, 21, 0.0325835, 500, 16.29175, 0, 'Expense', NULL, 5, NULL, 28, 'Trans', '', '2013-11-21 12:57:30'),
(593, 8, 4, NULL, '2013-11-21', '', 1, 52, 50, 41, 41, 1, 50, 50, 0, 'Income', 111, NULL, 16, NULL, 'Trans', '', '2013-11-21 13:00:54'),
(594, 8, 4, NULL, '2013-11-21', '', 1, 52, 44.5, 41, 41, 1, 44.5, 44.5, 0, 'Income', 111, NULL, 16, NULL, 'Trans', '', '2013-11-21 13:01:05'),
(644, 8, 4, NULL, '2013-11-25', 'invoice', 7, 52, 10000, 41, 41, 1, 10000, 10000, 10000, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2013-11-25 10:15:27'),
(646, 9, 3, NULL, '2013-11-25', '', 28, 20, 100, 4, 4, 1, 100, 100, 100, 'Expense', NULL, 130, NULL, NULL, 'Trans', '', '2013-11-25 10:32:33'),
(647, 9, 3, NULL, '2013-11-25', '', 25, 49, 86.96, 108, 4, 1.09594, 100, 95.3, 0, 'Income', 10, NULL, NULL, NULL, 'Trans', '', '2013-11-25 10:36:13'),
(648, 9, 3, NULL, '2013-11-25', '', 25, 20, 0, 4, 4, 1, 0, 0, 0, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2013-11-25 10:37:06'),
(649, 8, 4, NULL, '2013-11-25', '', 7, 89, 0, 41, 41, 1, 0, 0, 0, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2013-11-25 11:51:01'),
(651, 8, 4, NULL, '2013-11-25', '', 1, 2, 3000, 41, 41, 1, 3000, 3000, 0, 'Income', 1, NULL, 13, NULL, 'Trans', '', '2013-11-25 12:46:41'),
(652, 8, 4, NULL, '2013-11-25', '', 1, 52, 1600.45, 108, 41, 62.4177, 1600.45, 99896.407965, 0, 'Income', 1, NULL, 4, NULL, 'Trans', '', '2013-11-25 12:47:05'),
(654, 8, 4, NULL, '2013-11-25', '', 1, 2, 19.527594951984, 108, 41, 61.4515, 19.527594951984, 1200, 0, 'Income', 1, NULL, 4, NULL, 'Trans', '', '2013-11-25 12:49:33'),
(655, 8, 4, NULL, '2013-11-25', '', 1, 2, 3.6427101083348, 108, 41, 61.4515, 3.6427101083348, 223.85, 0, 'Income', 1, NULL, 4, NULL, 'Trans', '', '2013-11-25 12:50:10'),
(657, 8, 4, NULL, '2013-11-25', '', 1, 2, 500, 41, 41, 1, 500, 500, 0, 'Income', 1, NULL, 32, NULL, 'Trans', '', '2013-11-25 12:51:51'),
(658, 8, 4, NULL, '2013-11-25', '', 1, 2, 400, 41, 41, 1, 400, 400, 0, 'Income', 1, NULL, 13, NULL, 'Trans', '', '2013-11-25 12:52:33'),
(659, 8, 4, NULL, '2013-11-25', '', 1, 2, 200, 41, 41, 1, 200, 200, 0, 'Income', 1, NULL, 13, NULL, 'Trans', '', '2013-11-25 12:52:56'),
(660, 8, 4, NULL, '2013-11-25', '', 1, 2, 10, 41, 41, 1, 10, 10, 0, 'Income', 1, NULL, 13, NULL, 'Trans', '', '2013-11-25 12:54:30'),
(661, 8, 4, NULL, '2013-11-25', '', 1, 2, 100, 41, 41, 1, 100, 100, 0, 'Income', 1, NULL, 32, NULL, 'Trans', '', '2013-11-25 12:54:30'),
(662, 8, 4, NULL, '2013-11-25', '', 1, 2, 10, 41, 41, 1, 10, 10, 0, 'Income', 1, NULL, 13, NULL, 'Trans', '', '2013-11-25 12:55:05'),
(663, 8, 4, NULL, '2013-11-25', '', 1, 2, 100, 41, 41, 1, 100, 100, 0, 'Income', 1, NULL, 32, NULL, 'Trans', '', '2013-11-25 12:55:05'),
(664, 8, 4, NULL, '2013-11-25', '', 1, 2, 40, 41, 41, 1, 40, 40, 0, 'Income', 1, NULL, 13, NULL, 'Trans', '', '2013-11-25 12:56:22'),
(665, 8, 4, NULL, '2013-11-25', '', 1, 2, 260, 41, 41, 1, 260, 260, 0, 'Income', 1, NULL, 32, NULL, 'Trans', '', '2013-11-25 12:56:22'),
(666, 8, 4, NULL, '2013-11-25', '', 1, 2, 120, 41, 41, 1, 120, 120, 0, 'Income', 1, NULL, 13, NULL, 'Trans', '', '2013-11-25 12:56:51'),
(667, 8, 4, NULL, '2013-11-25', '', 1, 2, 20, 41, 41, 1, 20, 20, 0, 'Income', 1, NULL, 32, NULL, 'Trans', '', '2013-11-25 12:56:51'),
(668, 8, 4, NULL, '2013-11-26', '', 1, 2, 10, 41, 41, 1, 10, 10, 0, 'Income', 1, NULL, 13, NULL, 'Trans', '', '2013-11-26 05:31:00'),
(669, 8, 4, NULL, '2013-11-26', '', 1, 2, 10, 41, 41, 1, 10, 10, 0, 'Income', 1, NULL, 13, NULL, 'Trans', '', '2013-11-26 05:31:25'),
(695, 9, 3, NULL, '2013-11-26', '', 19, 20, 50, 4, 4, 1, 50, 50, 0, 'Income', 10, NULL, 30, NULL, 'Trans', '', '2013-11-26 08:02:01'),
(696, 9, 3, NULL, '2013-11-26', '', 19, 20, 50, 4, 4, 1, 50, 50, 0, 'Income', 10, NULL, 30, NULL, 'Trans', '', '2013-11-26 08:02:17'),
(699, 9, 3, NULL, '2013-11-26', '', 19, 20, 50.262181864689, 108, 4, 1.09466, 50.262181864689, 55.02, 0, 'Income', 10, NULL, 31, NULL, 'Trans', '', '2013-11-26 08:36:59'),
(700, 9, 3, NULL, '2013-11-26', '', 19, 20, 45.676283046791, 108, 4, 1.09466, 45.676283046791, 50, 0, 'Income', 10, NULL, 31, NULL, 'Trans', '', '2013-11-26 08:37:14'),
(701, 9, 3, NULL, '2013-11-26', '', 19, 20, 40.195129081176, 108, 4, 1.09466, 40.195129081176, 44, 0, 'Income', 10, NULL, 31, NULL, 'Trans', '', '2013-11-26 08:38:28'),
(703, 8, 4, NULL, '2013-11-26', '', 1, 2, 16.27299579332, 108, 41, 61.4515, 16.27299579332, 1000, 0, 'Income', 1, NULL, 4, NULL, 'Trans', '', '2013-11-26 10:17:01'),
(705, 8, 4, NULL, '2013-11-26', '', 1, 2, 895.01476863262, 108, 41, 61.4515, 895.01476863262, 55000, 0, 'Income', 1, NULL, 4, NULL, 'Trans', '', '2013-11-26 10:18:36'),
(706, 9, 3, NULL, '2013-11-26', '', 22, 20, 300.56, 4, 4, 1, 300.56, 300.56, 0, 'Expense', 110, NULL, NULL, 43, 'Trans', '', '2013-11-26 11:05:00'),
(710, 8, 4, NULL, '2013-11-26', '', 1, 52, 20.8, 41, 41, 1, 20.8, 20.8, 0, 'Income', 1, NULL, 32, NULL, 'Trans', '', '2013-11-26 12:35:38'),
(711, 9, 3, NULL, '2013-11-27', '', 19, 49, 232, 4, 4, 1, 232, 232, 0, 'Income', 10, NULL, 30, NULL, 'Trans', '', '2013-11-26 14:24:01'),
(712, 9, 3, NULL, '2013-11-26', '', 19, 20, 5.4811539656149, 108, 4, 1.09466, 5.4811539656149, 6, 0, 'Income', 10, NULL, 31, NULL, 'Trans', '', '2013-11-26 14:24:26'),
(713, 9, 3, NULL, '2013-11-27', '', 19, 20, 300, 4, 4, 1, 300, 300, 0, 'Income', 10, NULL, 30, NULL, 'Trans', '', '2013-11-27 05:10:19'),
(714, 9, 3, NULL, '2013-11-27', '', 19, 20, 45.676283046791, 108, 4, 1.09466, 45.676283046791, 50, 0, 'Income', 10, NULL, 31, NULL, 'Trans', '', '2013-11-27 05:10:19'),
(715, 9, 3, NULL, '2013-11-27', '', 19, 20, 45.676283046791, 108, 4, 1.09466, 45.676283046791, 50, 0, 'Income', 10, NULL, 31, NULL, 'Trans', '', '2013-11-27 05:10:38'),
(716, 9, 3, NULL, '2013-11-27', '', 19, 49, 100, 4, 4, 1, 100, 100, 0, 'Income', 10, NULL, 30, NULL, 'Trans', '', '2013-11-27 05:12:50'),
(717, 9, 3, NULL, '2013-11-27', '', 22, 20, 31.925468411052, 51, 51, 3.86306, 31.925468411052, 123.33, 0, 'Expense', 109, NULL, NULL, 44, 'Trans', '', '2013-11-27 05:35:42'),
(720, 9, 3, NULL, '2013-11-27', '', 19, 20, 22.381378692927, 108, 4, 1.09466, 22.381378692927, 24.5, 0, 'Income', 10, NULL, 31, NULL, 'Trans', '', '2013-11-27 05:47:39'),
(721, 9, 3, NULL, '2013-11-27', '', 19, 20, 22.838141523395, 108, 4, 1.09466, 22.838141523395, 25, 0, 'Income', 10, NULL, 31, NULL, 'Trans', '', '2013-11-27 05:48:36'),
(724, 9, 3, NULL, '2013-11-27', '', 19, 20, 27.405769828074, 108, 4, 1.09466, 27.405769828074, 30, 0, 'Income', 10, NULL, 31, NULL, 'Trans', '', '2013-11-27 06:21:46'),
(725, 9, 3, NULL, '2013-11-27', '', 19, 20, 0.45676283046791, 108, 4, 1.09466, 0.45676283046791, 0.5, 0, 'Income', 10, NULL, 31, NULL, 'Trans', '', '2013-11-27 06:22:26'),
(726, 9, 3, NULL, '2013-11-27', '', 22, 20, 0.90601750943553, 51, 51, 3.86306, 0.90601750943553, 3.5, 0, 'Expense', 109, NULL, NULL, 44, 'Trans', '', '2013-11-27 06:30:00'),
(727, 9, 3, NULL, '2013-11-27', '', 22, 20, 0.1294310727765, 51, 51, 3.86306, 0.1294310727765, 0.5, 0, 'Expense', 109, NULL, NULL, 44, 'Trans', '', '2013-11-27 06:30:34'),
(728, 9, 3, NULL, '2013-11-27', '', 22, 20, 15, 4, 4, 1, 15, 15, 0, 'Expense', 110, NULL, NULL, 43, 'Trans', '', '2013-11-27 06:32:41'),
(729, 9, 3, NULL, '2013-11-27', '', 25, 20, 50, 4, 4, 1, 50, 50, 50, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2013-11-27 07:27:24'),
(730, 9, 3, NULL, '2013-11-27', '', 25, 20, 0, 4, 4, 1, 0, 0, 0, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2013-11-27 07:31:59'),
(731, 8, 4, NULL, '2013-11-27', '', 7, 2, 50, 41, 41, 1, 50, 50, 50, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2013-11-27 07:49:42'),
(735, 8, 4, NULL, '2013-11-27', '', 1, 2, 8.1364978966602, 108, 41, 61.4515, 8.1364978966602, 500, 0, 'Income', 1, NULL, 4, NULL, 'Trans', '', '2013-11-27 10:04:31'),
(736, 8, 4, NULL, '2013-11-27', '', 1, 2, 500, 41, 41, 1, 500, 500, 0, 'Income', 1, NULL, 32, NULL, 'Trans', '', '2013-11-27 10:04:31'),
(737, 8, 4, NULL, '2013-11-27', '', 1, 2, 50000, 41, 41, 1, 50000, 50000, 0, 'Income', 1, NULL, 32, NULL, 'Trans', '', '2013-11-27 10:05:01'),
(745, 8, 4, NULL, '2013-11-26', 'Transfer to SBI', NULL, 52, 100, 41, 41, 1, 100, 100, 0, 'Expense', NULL, NULL, NULL, NULL, 'Transfer', '', '2013-11-27 13:09:27'),
(746, 8, 4, NULL, '2013-11-26', 'Transfer from ICICI', NULL, 88, 100, 41, 41, 1, 100, 100, 0, 'Income', NULL, NULL, NULL, NULL, 'Transfer', '', '2013-11-27 13:09:27'),
(747, 9, 3, NULL, '2013-11-27', 'Transfer to Cash on Hand', NULL, 49, 5000, 4, 4, 1, 5000, 5000, 0, 'Expense', NULL, NULL, NULL, NULL, 'Transfer', '', '2013-11-27 13:24:17'),
(748, 9, 3, NULL, '2013-11-27', 'Transfer from ICICI BANK', NULL, 20, 5000, 4, 4, 1, 5000, 5000, 0, 'Income', NULL, NULL, NULL, NULL, 'Transfer', '', '2013-11-27 13:24:17'),
(749, 9, 3, NULL, '2013-11-28', '', 19, 20, 63.946796265507, 108, 4, 1.09466, 63.946796265507, 70, 0, 'Income', 10, NULL, 31, NULL, 'Trans', '', '2013-11-28 09:34:55'),
(750, 9, 3, NULL, '2013-11-28', '', 19, 20, 27.384505846592, 108, 4, 1.09551, 27.384505846592, 30, 0, 'Income', 10, NULL, 33, NULL, 'Trans', '', '2013-11-28 09:34:55'),
(751, 9, 3, NULL, '2013-11-28', '', 19, 20, 4.6188533194585, 108, 4, 1.09551, 4.6188533194585, 5.06, 0, 'Income', 10, NULL, 33, NULL, 'Trans', '', '2013-11-28 09:35:13'),
(752, 9, 3, NULL, '2013-11-28', '', 25, 20, 500, 4, 4, 1, 500, 500, 500, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2013-11-28 09:35:28'),
(753, 9, 3, NULL, '2013-11-28', '', 19, 20, 100, 41, 4, 1, 100, 100, 0, 'Income', 73, NULL, 34, NULL, 'Trans', '', '2013-11-28 12:40:43'),
(754, 9, 3, NULL, '2013-11-28', '', 19, 20, 10, 41, 4, 1, 10, 10, 0, 'Income', 73, NULL, 34, NULL, 'Trans', '', '2013-11-28 12:42:10'),
(755, 9, 3, NULL, '2013-11-29', '', 19, 20, 890, 41, 4, 1, 890, 890, 0, 'Income', 73, NULL, 34, NULL, 'Trans', '', '2013-11-29 07:35:35'),
(757, 8, 4, NULL, '2013-11-29', '', 7, 2, 0, 41, 41, 1, 0, 0, 0, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2013-11-29 10:00:08'),
(758, 8, 4, NULL, '2013-11-29', '', 8, 2, 0, 41, 41, 1, 0, 0, 0, 'Expense', NULL, NULL, NULL, NULL, 'Trans', '', '2013-11-29 10:00:55'),
(759, 8, 4, NULL, '2013-11-29', '', 1, 2, 56.955485276621, 108, 41, 61.4515, 56.955485276621, 3500, 0, 'Income', 1, NULL, 4, NULL, 'Trans', '', '2013-11-29 10:26:23'),
(760, 11, 6, NULL, '2014-01-13', 'this is the description', 95, 93, 45000, 41, 41, 1, 45000, 45000, 0, 'Expense', NULL, 137, NULL, 45, 'Trans', '', '2014-01-13 12:06:19'),
(761, 11, 6, NULL, '2014-01-13', 'Transfer to GIC & Term Deposits ', NULL, 93, 0, 41, 41, 1, 0, 0, 0, 'Expense', NULL, NULL, NULL, NULL, 'Transfer', '', '2014-01-13 12:09:54'),
(762, 11, 6, NULL, '2014-01-13', 'Transfer from Cash on Hand', NULL, 107, 0, 41, 41, 1, 0, 0, 0, 'Income', NULL, NULL, NULL, NULL, 'Transfer', '', '2014-01-13 12:09:54'),
(763, 12, 7, NULL, '2014-03-04', '', 116, 111, 1000, 41, 41, 1, 1000, 1000, 1000, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2014-03-04 07:06:06'),
(764, 12, 7, NULL, '2014-03-04', '', 110, 111, 10, 41, 41, 1, 10, 10, 0, 'Income', 138, NULL, 38, NULL, 'Trans', '', '2014-03-04 07:06:18'),
(765, 12, 7, NULL, '2014-03-04', '', 116, 111, 50000, 41, 41, 1, 50000, 50000, 50000, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2014-03-04 07:14:47'),
(811, 16, 10, NULL, '2014-03-06', '', 163, 164, 3000, 108, 108, 1, 3000, 3000, 0, 'Income', 144, NULL, 43, NULL, 'Trans', '', '2014-03-06 12:17:24'),
(812, 16, 10, NULL, '2014-03-06', '', 163, 164, 1500, 108, 108, 1, 1500, 1500, 0, 'Income', 144, NULL, 44, NULL, 'Trans', '', '2014-03-06 12:17:24'),
(813, 16, 10, NULL, '2014-03-06', '', 169, 164, 200, 108, 108, 1, 200, 200, 200, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2014-03-06 12:18:11'),
(814, 16, 10, NULL, '2014-03-06', '', 169, 176, 500, 108, 108, 1, 500, 500, 500, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2014-03-06 12:18:19'),
(815, 16, 10, NULL, '2014-03-06', '', 166, 164, 200, 108, 108, 1, 200, 200, 0, 'Expense', 145, NULL, NULL, 50, 'Trans', '', '2014-03-06 12:19:40'),
(816, 16, 10, NULL, '2014-03-06', '', 170, 164, 100, 108, 108, 1, 100, 100, 100, 'Expense', NULL, NULL, NULL, NULL, 'Trans', '', '2014-03-06 12:20:12'),
(817, 16, 10, NULL, '2014-03-06', '', 172, 164, 200, 108, 108, 1, 200, 200, 200, 'Expense', NULL, NULL, NULL, NULL, 'Trans', '', '2014-03-06 12:20:19'),
(818, 16, 10, NULL, '2013-02-20', 'DCARDFEE3568MAR12-FEB13+ST', 169, 181, 111.24, 108, 108, 1, 111.24, 111.24, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(819, 16, 10, NULL, '2013-02-27', 'BIL/410053405/PAYTM MOBILE SOLUTIO/90554549_PAY', 169, 181, 50, 108, 108, 1, 50, 50, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(820, 16, 10, NULL, '2013-03-01', '601601513987:Int.Pd:01-09-2012 to 28-02-2013', 169, 181, 86, 108, 108, 1, 86, 86, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(821, 16, 10, NULL, '2013-03-04', 'VIN/PAYPAL *FAN/20130304233628/0', 169, 181, 157.41, 108, 108, 1, 157.41, 157.41, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(822, 16, 10, NULL, '2013-03-12', 'INF/000006364416/Sheik sal feb 2013', 169, 181, 8, 108, 108, 1, 8, 8, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(823, 16, 10, NULL, '2013-03-12', 'BIL/415922679/sheik/NSP', 169, 181, 8, 108, 108, 1, 8, 8, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(824, 16, 10, NULL, '2013-03-21', 'BIL/418872055/Bill Desk/AIRTELPREPAI_MI', 169, 181, 55, 108, 108, 1, 55, 55, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(825, 16, 10, NULL, '2013-03-25', 'VPS/PAYPAL *FRE/20130325053948/0', 169, 181, 303.76, 108, 108, 1, 303.76, 303.76, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(826, 16, 10, NULL, '2013-03-31', 'BIL/422336044/Bill Desk/AIRTELPREPAI_MI', 169, 181, 55, 108, 108, 1, 55, 55, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(827, 16, 10, NULL, '2013-04-01', 'BIL/422677627/Bill Desk/AIRTELPREPAI_MI', 169, 181, 235, 108, 108, 1, 235, 235, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(828, 16, 10, NULL, '2013-04-03', 'BIL/423840649/C.C.AVENUES/SHOPCLUES.COM-I', 169, 181, 38, 108, 108, 1, 38, 38, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(829, 16, 10, NULL, '2013-04-03', 'BIL/423983693/TATA SKY/TATASKY_MICI296', 169, 181, 350, 108, 108, 1, 350, 350, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(830, 16, 10, NULL, '2013-04-04', 'BIL/424213904/EBAY-PAISAPAY/33429158205', 169, 181, 19, 108, 108, 1, 19, 19, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(831, 16, 10, NULL, '2013-04-05', 'INF/000006509644/March 2013 Salary', 169, 181, 5, 108, 108, 1, 5, 5, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(832, 16, 10, NULL, '2013-04-05', 'BIL/424840654/sheik/NSP', 169, 181, 6, 108, 108, 1, 6, 6, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(833, 16, 10, NULL, '2013-04-09', 'BIL/426220390/REDIFF.COM/7934733', 169, 181, 185, 108, 108, 1, 185, 185, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(834, 16, 10, NULL, '2013-04-24', 'BIL/431284087/Bill Desk/AIRTELPREPAI_MI', 169, 181, 110, 108, 108, 1, 110, 110, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(835, 16, 10, NULL, '2013-04-26', 'DCARDFEE3568MAR13-FEB14ST12.24', 169, 181, 111.24, 108, 108, 1, 111.24, 111.24, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(836, 16, 10, NULL, '2013-05-10', 'INF/000006720251/April Salary 2013', 169, 181, 8, 108, 108, 1, 8, 8, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(837, 16, 10, NULL, '2013-05-12', 'BIL/438631012/sheik/NSP', 169, 181, 8, 108, 108, 1, 8, 8, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(838, 16, 10, NULL, '2013-05-31', 'BIL/445375091/0127/919894067178/919894067178', 169, 181, 50, 108, 108, 1, 50, 50, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(839, 16, 10, NULL, '2013-06-10', 'BIL/449630026/sudha/NSP', 169, 181, 1, 108, 108, 1, 1, 1, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(840, 16, 10, NULL, '2013-06-11', 'BIL/449850495/sudha/NSP', 169, 181, 2, 108, 108, 1, 2, 2, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(841, 16, 10, NULL, '2013-06-13', 'INF/000006934846/Shiek May salary', 169, 181, 8, 108, 108, 1, 8, 8, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(842, 16, 10, NULL, '2013-06-15', 'BIL/451511672/PayU India/29799661', 169, 181, 598, 108, 108, 1, 598, 598, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(843, 16, 10, NULL, '2013-06-17', 'BIL/452148003/sheik/NSP', 169, 181, 9, 108, 108, 1, 9, 9, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(844, 16, 10, NULL, '2013-06-23', 'BIL/453990759/0127/919894067178/919894067178', 169, 181, 50, 108, 108, 1, 50, 50, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(845, 16, 10, NULL, '2013-06-23', 'BIL/AUTORECON TID.000453990759/PREPAID MOBILE RECH', 169, 181, 50, 108, 108, 1, 50, 50, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(846, 16, 10, NULL, '2013-06-24', 'BIL/454411781/Bill Desk/AIRTELPREPAI_MI', 169, 181, 50, 108, 108, 1, 50, 50, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(847, 16, 10, NULL, '2013-06-24', 'BIL/AUTORECON TID.000454403358', 169, 181, 50, 108, 108, 1, 50, 50, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(848, 16, 10, NULL, '2013-06-24', 'BIL/AUTORECON TID.000454403358/Bill Desk', 169, 181, 50, 108, 108, 1, 50, 50, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(849, 16, 10, NULL, '2013-06-26', 'REV OF PMT ID 454411781', 169, 181, 50, 108, 108, 1, 50, 50, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(850, 16, 10, NULL, '2013-07-08', 'BIL/460444154/BILL JUNCTION PAYMEN/76581688', 169, 181, 827.95, 108, 108, 1, 827.95, 827.95, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(851, 16, 10, NULL, '2013-07-13', 'BIL/462381409/Bill Desk/AIRTELPREPAI_MI', 169, 181, 50, 108, 108, 1, 50, 50, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(852, 16, 10, NULL, '2013-07-16', 'INF/000007141623/Sheik jun 2013 sal', 169, 181, 8, 108, 108, 1, 8, 8, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(853, 16, 10, NULL, '2013-07-17', 'ATM/CASH WDL/17-07-13/19:13:17/0', 169, 181, 8, 108, 108, 1, 8, 8, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(854, 16, 10, NULL, '2013-08-03', 'REV OF PMT ID 451511672', 169, 181, 139, 108, 108, 1, 139, 139, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(855, 16, 10, NULL, '2013-08-08', 'INF/000007307898/Sheik July 2013salary', 169, 181, 8, 108, 108, 1, 8, 8, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(856, 16, 10, NULL, '2013-08-13', 'ATM/CASH WDL/13-08-13/19:21:25/0', 169, 181, 8, 108, 108, 1, 8, 8, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(857, 16, 10, NULL, '2013-09-02', '601601513987:Int.Pd:01-03-2013 to 31-08-2013', 169, 181, 83, 108, 108, 1, 83, 83, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-06 12:22:05'),
(858, 16, 10, NULL, '2014-03-06', '', 169, 164, 0, 108, 108, 1, 0, 0, 0, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2014-03-06 12:22:33'),
(859, 16, 10, NULL, '2014-03-06', '', 169, 164, 0, 108, 108, 1, 0, 0, 0, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2014-03-06 12:22:36'),
(860, 16, 10, NULL, '2014-03-06', '', 176, 164, 200, 108, 108, 1, 200, 200, 200, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2014-03-06 12:22:48'),
(861, 16, 10, NULL, '2014-03-06', '', 170, 164, 100, 108, 108, 1, 100, 100, 100, 'Expense', NULL, NULL, NULL, NULL, 'Trans', '', '2014-03-06 12:22:59'),
(862, 16, 10, NULL, '2014-03-06', 'Transfer to Certificate of Deposit ', NULL, 164, 300, 108, 108, 1, 300, 300, 0, 'Expense', NULL, NULL, NULL, NULL, 'Transfer', '', '2014-03-06 12:23:49'),
(863, 16, 10, NULL, '2014-03-06', 'Transfer from Cash on Hand', NULL, 180, 300, 108, 108, 1, 300, 300, 0, 'Income', NULL, NULL, NULL, NULL, 'Transfer', '', '2014-03-06 12:23:49'),
(864, 16, 10, NULL, '2014-03-07', '', 163, 164, 400, 108, 108, 1, 400, 400, 0, 'Income', 144, NULL, 43, NULL, 'Trans', '', '2014-03-07 04:53:43'),
(866, 16, 10, NULL, '2014-03-07', '', 176, 164, 100, 108, 108, 1, 100, 100, 100, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2014-03-07 05:43:20'),
(867, 16, 10, NULL, '2014-03-07', '', 176, 164, 200, 108, 108, 1, 200, 200, 200, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2014-03-07 05:43:43'),
(868, 16, 10, NULL, '2014-03-07', '', 166, 181, 20, 108, 108, 1, 20, 20, 0, 'Expense', 145, NULL, NULL, 50, 'Trans', '', '2014-03-07 05:44:30'),
(869, 16, 10, NULL, '2014-03-07', '', 166, 181, 80, 103, 108, 1, 80, 80, 0, 'Expense', 145, NULL, NULL, 51, 'Trans', '', '2014-03-07 05:44:30'),
(870, 16, 10, NULL, '2014-03-07', '', 173, 164, 200, 108, 108, 1, 200, 200, 200, 'Expense', NULL, NULL, NULL, NULL, 'Trans', '', '2014-03-07 05:45:06'),
(871, 16, 10, NULL, '2014-03-07', '', 170, 164, 0, 108, 108, 1, 0, 0, 0, 'Expense', NULL, NULL, NULL, NULL, 'Trans', '', '2014-03-07 05:45:36'),
(873, 16, 10, NULL, '2014-03-07', '', 169, 164, 0, 108, 108, 1, 0, 0, 0, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2014-03-07 06:37:44'),
(874, 19, 12, NULL, '2014-03-24', 'rent', 210, 205, 500, 108, 108, 1, 500, 500, 500, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2014-03-24 12:19:26'),
(875, 19, 12, NULL, '2014-03-24', '', 204, 205, 500, 108, 108, 1, 500, 500, 0, 'Income', 149, NULL, 49, NULL, 'Trans', '', '2014-03-24 12:19:39'),
(876, 19, 12, NULL, '2014-03-24', 'power', 211, 205, 500, 108, 108, 1, 500, 500, 500, 'Expense', NULL, NULL, NULL, NULL, 'Trans', '', '2014-03-24 12:21:00'),
(877, 19, 12, NULL, '2013-02-20', 'DCARDFEE3568MAR12-FEB13+ST', 210, 222, 111.24, 108, 108, 1, 111.24, 111.24, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(878, 19, 12, NULL, '2013-02-27', 'BIL/410053405/PAYTM MOBILE SOLUTIO/90554549_PAY', 210, 222, 50, 108, 108, 1, 50, 50, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(879, 19, 12, NULL, '2013-03-01', '601601513987:Int.Pd:01-09-2012 to 28-02-2013', 210, 222, 86, 108, 108, 1, 86, 86, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(880, 19, 12, NULL, '2013-03-04', 'VIN/PAYPAL *FAN/20130304233628/0', 210, 222, 157.41, 108, 108, 1, 157.41, 157.41, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(881, 19, 12, NULL, '2013-03-12', 'INF/000006364416/Sheik sal feb 2013                                             ', 210, 222, 8, 108, 108, 1, 8, 8, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(882, 19, 12, NULL, '2013-03-12', 'BIL/415922679/sheik/NSP                                                         ', 210, 222, 8, 108, 108, 1, 8, 8, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(883, 19, 12, NULL, '2013-03-21', 'BIL/418872055/Bill Desk/AIRTELPREPAI_MI                                         ', 210, 222, 55, 108, 108, 1, 55, 55, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(884, 19, 12, NULL, '2013-03-25', 'VPS/PAYPAL *FRE/20130325053948/0                                                ', 210, 222, 303.76, 108, 108, 1, 303.76, 303.76, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(885, 19, 12, NULL, '2013-03-31', 'BIL/422336044/Bill Desk/AIRTELPREPAI_MI                                         ', 210, 222, 55, 108, 108, 1, 55, 55, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(886, 19, 12, NULL, '2013-04-01', 'BIL/422677627/Bill Desk/AIRTELPREPAI_MI                                         ', 210, 222, 235, 108, 108, 1, 235, 235, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(887, 19, 12, NULL, '2013-04-03', 'BIL/423840649/C.C.AVENUES/SHOPCLUES.COM-I                                       ', 210, 222, 38, 108, 108, 1, 38, 38, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(888, 19, 12, NULL, '2013-04-03', 'BIL/423983693/TATA SKY/TATASKY_MICI296                                          ', 210, 222, 350, 108, 108, 1, 350, 350, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(889, 19, 12, NULL, '2013-04-04', 'BIL/424213904/EBAY-PAISAPAY/33429158205                                         ', 210, 222, 19, 108, 108, 1, 19, 19, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(890, 19, 12, NULL, '2013-04-05', 'INF/000006509644/March 2013 Salary                                              ', 210, 222, 5, 108, 108, 1, 5, 5, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(891, 19, 12, NULL, '2013-04-05', 'BIL/424840654/sheik/NSP                                                         ', 210, 222, 6, 108, 108, 1, 6, 6, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(892, 19, 12, NULL, '2013-04-09', 'BIL/426220390/REDIFF.COM/7934733                                                ', 210, 222, 185, 108, 108, 1, 185, 185, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(893, 19, 12, NULL, '2013-04-24', 'BIL/431284087/Bill Desk/AIRTELPREPAI_MI                                         ', 210, 222, 110, 108, 108, 1, 110, 110, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(894, 19, 12, NULL, '2013-04-26', 'DCARDFEE3568MAR13-FEB14ST12.24                                                  ', 210, 222, 111.24, 108, 108, 1, 111.24, 111.24, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(895, 19, 12, NULL, '2013-05-10', 'INF/000006720251/April Salary 2013                                              ', 210, 222, 8, 108, 108, 1, 8, 8, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(896, 19, 12, NULL, '2013-05-12', 'BIL/438631012/sheik/NSP                                                         ', 210, 222, 8, 108, 108, 1, 8, 8, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(897, 19, 12, NULL, '2013-05-31', 'BIL/445375091/0127/919894067178/919894067178                                    ', 210, 222, 50, 108, 108, 1, 50, 50, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(898, 19, 12, NULL, '2013-06-10', 'BIL/449630026/sudha/NSP                                                         ', 210, 222, 1, 108, 108, 1, 1, 1, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(899, 19, 12, NULL, '2013-06-11', 'BIL/449850495/sudha/NSP                                                         ', 210, 222, 2, 108, 108, 1, 2, 2, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(900, 19, 12, NULL, '2013-06-13', 'INF/000006934846/Shiek May salary                                               ', 210, 222, 8, 108, 108, 1, 8, 8, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(901, 19, 12, NULL, '2013-06-15', 'BIL/451511672/PayU India/29799661                                               ', 210, 222, 598, 108, 108, 1, 598, 598, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(902, 19, 12, NULL, '2013-06-17', 'BIL/452148003/sheik/NSP                                                         ', 210, 222, 9, 108, 108, 1, 9, 9, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(903, 19, 12, NULL, '2013-06-23', 'BIL/453990759/0127/919894067178/919894067178                                    ', 210, 222, 50, 108, 108, 1, 50, 50, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(904, 19, 12, NULL, '2013-06-23', 'BIL/AUTORECON TID.000453990759/PREPAID MOBILE RECH                              ', 210, 222, 50, 108, 108, 1, 50, 50, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(905, 19, 12, NULL, '2013-06-24', 'BIL/454411781/Bill Desk/AIRTELPREPAI_MI                                         ', 210, 222, 50, 108, 108, 1, 50, 50, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(906, 19, 12, NULL, '2013-06-24', 'BIL/AUTORECON TID.000454403358                                                  ', 210, 222, 50, 108, 108, 1, 50, 50, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(907, 19, 12, NULL, '2013-06-24', 'BIL/AUTORECON TID.000454403358/Bill Desk                                        ', 210, 222, 50, 108, 108, 1, 50, 50, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(908, 19, 12, NULL, '2013-06-26', 'REV OF PMT ID 454411781                                                         ', 210, 222, 50, 108, 108, 1, 50, 50, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(909, 19, 12, NULL, '2013-07-08', 'BIL/460444154/BILL JUNCTION PAYMEN/76581688                                     ', 210, 222, 827.95, 108, 108, 1, 827.95, 827.95, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(910, 19, 12, NULL, '2013-07-13', 'BIL/462381409/Bill Desk/AIRTELPREPAI_MI                                         ', 210, 222, 50, 108, 108, 1, 50, 50, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(911, 19, 12, NULL, '2013-07-16', 'INF/000007141623/Sheik jun 2013 sal                                             ', 210, 222, 8, 108, 108, 1, 8, 8, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(912, 19, 12, NULL, '2013-07-17', 'ATM/CASH WDL/17-07-13/19:13:17/0                                                ', 210, 222, 8, 108, 108, 1, 8, 8, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(913, 19, 12, NULL, '2013-08-03', 'REV OF PMT ID 451511672                                                         ', 210, 222, 139, 108, 108, 1, 139, 139, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(914, 19, 12, NULL, '2013-08-08', 'INF/000007307898/Sheik July 2013salary                                          ', 210, 222, 8, 108, 108, 1, 8, 8, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(915, 19, 12, NULL, '2013-08-13', 'ATM/CASH WDL/13-08-13/19:21:25/0                                                ', 210, 222, 8, 108, 108, 1, 8, 8, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(916, 19, 12, NULL, '2013-09-02', '601601513987:Int.Pd:01-03-2013 to 31-08-2013                                    ', 210, 222, 83, 108, 108, 1, 83, 83, 0, 'Income', NULL, NULL, NULL, NULL, 'Statement', '', '2014-03-24 12:23:28'),
(917, 8, 4, NULL, '2014-10-11', '', 8, 2, 0, 41, 41, 1, 0, 0, 0, 'Expense', NULL, NULL, NULL, NULL, 'Trans', '', '2014-10-10 13:14:24'),
(919, 8, 4, NULL, '2014-12-05', '', 7, 2, 0, 41, 41, 1, 0, 0, 0, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2014-12-05 14:30:30'),
(920, 8, 4, NULL, '2014-12-11', '', 8, 2, 0, 41, 41, 1, 0, 0, 0, 'Expense', NULL, NULL, NULL, NULL, 'Trans', '', '2014-12-11 18:54:15'),
(921, 8, 4, NULL, '2015-03-08', '', 7, 2, 0, 41, 41, 1, 0, 0, 0, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2015-03-08 18:15:25'),
(922, 8, 4, NULL, '2015-03-08', '', 7, 2, 0, 41, 41, 1, 0, 0, 0, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2015-03-08 18:16:20'),
(924, 8, 4, NULL, '2015-03-08', '', 8, 2, 0, 41, 41, 1, 0, 0, 0, 'Expense', NULL, NULL, NULL, NULL, 'Trans', '', '2015-03-08 18:20:55'),
(930, 24, 17, NULL, '2016-08-20', '', 289, 284, 0, 41, 41, 1, 0, 0, 0, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2016-08-20 07:32:32'),
(962, 26, 18, NULL, '2019-06-04', 'Paid by fantron', 317, 332, 345000, 117, 117, 1, 345000, 345000, 0, 'Expense', NULL, 161, NULL, 58, 'Trans', '', '2019-06-04 18:02:18'),
(963, 26, 18, NULL, '2019-06-04', '', 314, 332, 277725, 117, 117, 1, 277725, 277725, 0, 'Income', 160, NULL, 59, NULL, 'Trans', '', '2019-06-04 18:10:46'),
(964, 26, 18, NULL, '2019-06-04', '', 321, 315, 0, 117, 117, 1, 0, 0, 0, 'Expense', NULL, NULL, NULL, NULL, 'Trans', '', '2019-06-04 18:18:52'),
(965, 27, 19, NULL, '2019-06-11', '', 336, 355, 2300, 117, 117, 1, 2300, 2300, 0, 'Income', 164, NULL, 60, NULL, 'Trans', '', '2019-06-11 20:03:41'),
(966, 27, 19, NULL, '2019-06-11', '', 339, 355, 230000, 117, 117, 1, 230000, 230000, 0, 'Expense', NULL, 162, NULL, 60, 'Trans', '', '2019-06-11 20:12:41'),
(967, 27, 19, NULL, '2019-06-12', '', 336, 355, 220800, 117, 117, 1, 220800, 220800, 0, 'Income', 164, NULL, 61, NULL, 'Trans', '', '2019-06-12 03:49:02'),
(976, 13, 8, NULL, '2019-07-22', 'Website Development', 135, 130, 400000, 108, 108, 1, 400000, 400000, 0, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2019-07-22 12:51:09'),
(977, 29, 21, NULL, '2020-06-01', '', 379, 374, 0, 80, 80, 1, 0, 0, 0, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2020-01-06 16:25:07'),
(978, 13, 8, NULL, '2020-02-03', '', 135, 130, 0, 108, 108, 1, 0, 0, 0, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2020-02-03 03:39:36'),
(979, 13, 8, NULL, '2020-02-03', '', 136, 130, 0, 108, 108, 1, 0, 0, 0, 'Expense', NULL, NULL, NULL, NULL, 'Trans', '', '2020-02-03 03:41:40'),
(980, 13, 8, NULL, '2020-02-03', '', 136, 130, 0, 108, 108, 1, 0, 0, 0, 'Expense', NULL, NULL, NULL, NULL, 'Trans', '', '2020-02-03 05:05:57'),
(981, 13, 8, NULL, '2020-02-03', '', 135, 130, 0, 108, 108, 1, 0, 0, 0, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2020-02-03 05:11:37'),
(982, 13, 8, NULL, '2020-03-16', '', 135, 130, 0, 108, 108, 1, 0, 0, 0, 'Income', NULL, NULL, NULL, NULL, 'Trans', '', '2020-03-16 16:27:40'),
(983, 13, 8, NULL, '2020-04-03', '', 132, 130, 44, 105, 108, 1, 44, 44, 0, 'Expense', NULL, 157, NULL, 61, 'Trans', '', '2020-04-03 10:15:33'),
(984, 13, 8, NULL, '2020-06-04', '', 136, 130, 0, 108, 108, 1, 0, 0, 0, 'Expense', NULL, NULL, NULL, NULL, 'Trans', '', '2020-06-04 08:37:30');

-- --------------------------------------------------------

--
-- Table structure for table `fin_transactiontaxes`
--

CREATE TABLE `fin_transactiontaxes` (
  `taid` int(11) NOT NULL,
  `tid` int(11) NOT NULL COMMENT 'transaction id',
  `tamt` double NOT NULL COMMENT 'percentage or amount',
  `taxid` int(11) NOT NULL,
  `taxamount` float NOT NULL COMMENT 'Percentage Or Amount',
  `taxtype` enum('Percentage','Amount') COLLATE utf8_unicode_ci NOT NULL,
  `currency` int(11) NOT NULL,
  `primarycurrency` int(11) NOT NULL,
  `exchange_rate` float NOT NULL,
  `totalamount` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_transactiontaxes`
--

INSERT INTO `fin_transactiontaxes` (`taid`, `tid`, `tamt`, `taxid`, `taxamount`, `taxtype`, `currency`, `primarycurrency`, `exchange_rate`, `totalamount`) VALUES
(1, 547, 12, 13, 96, 'Percentage', 4, 4, 1, 96),
(2, 547, 10, 14, 80, 'Percentage', 4, 4, 1, 80),
(3, 547, 3, 19, 24, 'Percentage', 4, 4, 1, 24),
(17, 647, 12, 13, 10.43, 'Percentage', 108, 4, 1.1, 11.48),
(18, 647, 3, 19, 2.61, 'Percentage', 108, 4, 1.1, 2.87);

-- --------------------------------------------------------

--
-- Table structure for table `fin_userdefaults`
--

CREATE TABLE `fin_userdefaults` (
  `udid` int(11) NOT NULL,
  `uid` int(11) NOT NULL COMMENT 'userid',
  `bid` int(11) NOT NULL COMMENT 'business id',
  `agid` int(11) NOT NULL COMMENT 'account group id',
  `aiid` int(11) DEFAULT NULL COMMENT 'account item id',
  `bankid` int(11) DEFAULT NULL,
  `taxid` int(11) DEFAULT NULL,
  `systemaccount` tinyint(4) NOT NULL,
  `accountname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `edit` tinyint(4) NOT NULL,
  `delete` tinyint(4) NOT NULL,
  `status` enum('Active','Trash') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_userdefaults`
--

INSERT INTO `fin_userdefaults` (`udid`, `uid`, `bid`, `agid`, `aiid`, `bankid`, `taxid`, `systemaccount`, `accountname`, `edit`, `delete`, `status`) VALUES
(1, 8, 4, 1, 364, NULL, NULL, 1, 'Accounts Receivable', 0, 0, 'Active'),
(2, 8, 4, 1, 366, NULL, NULL, 1, 'Cash on Hand', 0, 0, 'Active'),
(3, 8, 4, 1, 363, NULL, NULL, 1, 'Unrealized Gain on Foreign Currency', 0, 0, 'Active'),
(4, 8, 4, 2, 362, NULL, NULL, 1, 'Accounts Payable', 0, 0, 'Active'),
(5, 8, 4, 2, 361, NULL, NULL, 1, 'Unrealized Loss on Foreign Currency', 0, 0, 'Active'),
(6, 8, 4, 3, 360, NULL, NULL, 1, 'Gain on Foreign Exchange', 0, 0, 'Active'),
(7, 8, 4, 3, 365, NULL, NULL, 0, 'Sales', 0, 1, 'Active'),
(8, 8, 4, 4, 335, NULL, NULL, 0, 'Accounting Fees', 0, 1, 'Active'),
(9, 8, 4, 4, 359, NULL, NULL, 1, 'Loss on Foreign Exchange', 0, 0, 'Active'),
(10, 8, 4, 4, 346, NULL, NULL, 0, 'Office Supplies', 0, 1, 'Active'),
(11, 8, 4, 4, 351, NULL, NULL, 0, 'Rent Expense', 0, 1, 'Active'),
(12, 8, 4, 4, 162, NULL, NULL, 0, 'Telephone – Land Line ', 0, 1, 'Active'),
(13, 8, 4, 4, 355, NULL, NULL, 0, 'Utilities', 0, 1, 'Trash'),
(14, 8, 4, 5, 91, NULL, NULL, 0, 'Owner Investment / Drawings ', 1, 0, 'Active'),
(15, 8, 4, 5, 358, NULL, NULL, 1, 'Owner\'s Equity', 0, 0, 'Active'),
(16, 8, 4, 1, 1, NULL, NULL, 0, 'ICICI', 1, 1, 'Trash'),
(17, 8, 4, 2, 25, NULL, NULL, 0, 'Bank Overdraft lia', 1, 1, 'Active'),
(18, 8, 4, 2, 25, NULL, NULL, 0, 'Bank Overdraft  2', 1, 1, 'Active'),
(19, 9, 3, 1, 364, NULL, NULL, 1, 'Accounts Receivable', 0, 0, 'Active'),
(20, 9, 3, 1, 366, NULL, NULL, 1, 'Cash on Hand', 0, 0, 'Active'),
(21, 9, 3, 1, 363, NULL, NULL, 1, 'Unrealized Gain on Foreign Currency', 0, 0, 'Active'),
(22, 9, 3, 2, 362, NULL, NULL, 1, 'Accounts Payable', 0, 0, 'Active'),
(23, 9, 3, 2, 361, NULL, NULL, 1, 'Unrealized Loss on Foreign Currency', 0, 0, 'Active'),
(24, 9, 3, 3, 360, NULL, NULL, 1, 'Gain on Foreign Exchange', 0, 0, 'Active'),
(25, 9, 3, 3, 365, NULL, NULL, 0, 'Sales', 0, 1, 'Active'),
(26, 9, 3, 4, 335, NULL, NULL, 0, 'Accounting Fees', 0, 1, 'Trash'),
(27, 9, 3, 4, 359, NULL, NULL, 1, 'Loss on Foreign Exchange', 0, 0, 'Active'),
(28, 9, 3, 4, 346, NULL, NULL, 0, 'Office Supplies', 0, 1, 'Active'),
(29, 9, 3, 4, 351, NULL, NULL, 0, 'Rent Expense', 0, 1, 'Active'),
(30, 9, 3, 4, 162, NULL, NULL, 0, 'Telephone – Land Line ', 0, 1, 'Active'),
(31, 9, 3, 4, 355, NULL, NULL, 0, 'Utilities', 0, 1, 'Active'),
(32, 9, 3, 5, 91, NULL, NULL, 0, 'Owner Investment / Drawings ', 1, 0, 'Active'),
(33, 9, 3, 5, 358, NULL, NULL, 1, 'Owner\'s Equity', 0, 0, 'Active'),
(34, 9, 3, 1, NULL, NULL, 13, 1, 'VAT Receivable', 0, 0, 'Active'),
(35, 9, 3, 2, NULL, NULL, 13, 1, 'VAT Payable', 0, 0, 'Active'),
(36, 8, 4, 3, 49, NULL, NULL, 0, 'Crop Sales ', 0, 1, 'Trash'),
(37, 8, 4, 5, 91, NULL, NULL, 0, 'Owner Investment / Drawings 123', 1, 1, 'Active'),
(38, 9, 3, 1, NULL, NULL, 14, 1, 'SAT Receivable', 0, 0, 'Active'),
(39, 9, 3, 2, NULL, NULL, 14, 1, 'SAT Payable', 0, 0, 'Active'),
(40, 9, 3, 1, NULL, NULL, 15, 1, 'GTax Receivable', 0, 0, 'Active'),
(41, 9, 3, 2, NULL, NULL, 15, 1, 'GTax Payable', 0, 0, 'Active'),
(43, 9, 3, 2, 25, NULL, NULL, 0, 'Bank Overdraft ', 1, 1, 'Active'),
(44, 8, 4, 1, NULL, NULL, 16, 1, 'VAT Receivable', 0, 0, 'Active'),
(45, 8, 4, 2, NULL, NULL, 16, 1, 'VAT Payable', 0, 0, 'Active'),
(49, 9, 3, 1, 1, 2, NULL, 0, 'ICICI BANK', 0, 0, 'Active'),
(51, 9, 3, 1, 1, 4, NULL, 0, 'CANRA BANK', 0, 0, 'Active'),
(52, 8, 4, 1, 1, 5, NULL, 0, 'ICICI', 0, 0, 'Active'),
(53, 8, 4, 1, 7, NULL, NULL, 0, 'Certificate of Deposit ', 1, 1, 'Active'),
(54, 8, 4, 1, 9, NULL, NULL, 0, 'Brokerage Account ', 1, 1, 'Active'),
(55, 8, 4, 4, 106, NULL, NULL, 0, 'Merchant Account Fees ', 0, 1, 'Active'),
(56, 9, 3, 4, 106, NULL, NULL, 0, 'Merchant Account Fees ', 0, 1, 'Active'),
(57, 8, 4, 1, 289, NULL, NULL, 0, 'Land ', 1, 1, 'Active'),
(58, 10, 5, 1, 364, NULL, NULL, 1, 'Accounts Receivable', 0, 0, 'Active'),
(59, 10, 5, 1, 366, NULL, NULL, 1, 'Cash on Hand', 0, 0, 'Active'),
(60, 10, 5, 1, 363, NULL, NULL, 1, 'Unrealized Gain on Foreign Currency', 0, 0, 'Active'),
(61, 10, 5, 2, 362, NULL, NULL, 1, 'Accounts Payable', 0, 0, 'Active'),
(62, 10, 5, 2, 361, NULL, NULL, 1, 'Unrealized Loss on Foreign Currency', 0, 0, 'Active'),
(63, 10, 5, 3, 360, NULL, NULL, 1, 'Gain on Foreign Exchange', 0, 0, 'Active'),
(64, 10, 5, 3, 365, NULL, NULL, 0, 'Sales', 0, 1, 'Active'),
(65, 10, 5, 4, 335, NULL, NULL, 0, 'Accounting Fees', 0, 1, 'Active'),
(66, 10, 5, 4, 359, NULL, NULL, 1, 'Loss on Foreign Exchange', 0, 0, 'Active'),
(67, 10, 5, 4, 346, NULL, NULL, 0, 'Office Supplies', 0, 1, 'Active'),
(68, 10, 5, 4, 351, NULL, NULL, 0, 'Rent Expense', 0, 1, 'Active'),
(69, 10, 5, 4, 162, NULL, NULL, 0, 'Telephone – Land Line ', 0, 1, 'Active'),
(70, 10, 5, 4, 355, NULL, NULL, 0, 'Utilities', 0, 1, 'Active'),
(71, 10, 5, 5, 91, NULL, NULL, 0, 'Owner Investment / Drawings ', 1, 0, 'Active'),
(72, 10, 5, 5, 358, NULL, NULL, 1, 'Owner\'s Equity', 0, 0, 'Active'),
(73, 10, 5, 1, NULL, NULL, 17, 1, 'VAT Receivable', 0, 0, 'Active'),
(74, 10, 5, 2, NULL, NULL, 17, 1, 'VAT Payable', 0, 0, 'Active'),
(75, 10, 5, 1, NULL, NULL, 18, 1, 'SAT Receivable', 0, 0, 'Active'),
(76, 10, 5, 2, NULL, NULL, 18, 1, 'SAT Payable', 0, 0, 'Active'),
(77, 10, 5, 1, 1, 6, NULL, 0, 'ICICI Bank', 0, 0, 'Active'),
(78, 10, 5, 1, 2, 7, NULL, 0, 'Canara Bank', 0, 0, 'Active'),
(79, 9, 3, 1, NULL, NULL, 19, 1, 'CTAXt Receivable', 0, 0, 'Active'),
(80, 9, 3, 2, NULL, NULL, 19, 1, 'CTAXt Payable', 0, 0, 'Active'),
(81, 10, 5, 4, 106, NULL, NULL, 0, 'Merchant Account Fees ', 0, 1, 'Active'),
(82, 8, 4, 1, NULL, NULL, 20, 1, 'VAT(R) Receivable', 0, 0, 'Active'),
(83, 8, 4, 2, NULL, NULL, 20, 1, 'VAT(R) Payable', 0, 0, 'Active'),
(84, 9, 3, 4, 107, NULL, NULL, 0, 'Subcontracted Services ', 0, 1, 'Active'),
(85, 9, 3, 3, 45, NULL, NULL, 0, 'Agricultural Program Payments ', 0, 1, 'Trash'),
(86, 9, 3, 1, 6, NULL, NULL, 0, 'GIC & Term Deposits ', 1, 1, 'Active'),
(87, 9, 3, 4, 111, NULL, NULL, 0, 'Fuel ', 0, 1, 'Active'),
(88, 8, 4, 1, 1, 8, NULL, 0, 'SBI', 0, 0, 'Active'),
(89, 8, 4, 1, 1, 9, NULL, 0, 'Credit card', 0, 0, 'Active'),
(90, 9, 3, 1, 2, 10, NULL, 0, 'Check account', 0, 0, 'Active'),
(91, 9, 3, 1, 2, 11, NULL, 0, 'ICICI check', 0, 0, 'Active'),
(92, 11, 6, 1, 364, NULL, NULL, 1, 'Accounts Receivable', 0, 0, 'Active'),
(93, 11, 6, 1, 366, NULL, NULL, 1, 'Cash on Hand', 0, 0, 'Active'),
(94, 11, 6, 1, 363, NULL, NULL, 1, 'Unrealized Gain on Foreign Currency', 0, 0, 'Active'),
(95, 11, 6, 2, 362, NULL, NULL, 1, 'Accounts Payable', 0, 0, 'Active'),
(96, 11, 6, 2, 361, NULL, NULL, 1, 'Unrealized Loss on Foreign Currency', 0, 0, 'Active'),
(97, 11, 6, 3, 360, NULL, NULL, 1, 'Gain on Foreign Exchange', 0, 0, 'Active'),
(98, 11, 6, 3, 365, NULL, NULL, 0, 'Sales', 0, 1, 'Active'),
(99, 11, 6, 4, 335, NULL, NULL, 0, 'Accounting Fees', 0, 1, 'Active'),
(100, 11, 6, 4, 359, NULL, NULL, 1, 'Loss on Foreign Exchange', 0, 0, 'Active'),
(101, 11, 6, 4, 346, NULL, NULL, 0, 'Office Supplies', 0, 1, 'Active'),
(102, 11, 6, 4, 351, NULL, NULL, 0, 'Rent Expense', 0, 1, 'Active'),
(103, 11, 6, 4, 162, NULL, NULL, 0, 'Telephone – Land Line ', 0, 1, 'Active'),
(104, 11, 6, 4, 355, NULL, NULL, 0, 'Utilities', 0, 1, 'Active'),
(105, 11, 6, 5, 91, NULL, NULL, 0, 'Owner Investment / Drawings ', 1, 0, 'Active'),
(106, 11, 6, 5, 358, NULL, NULL, 1, 'Owner\'s Equity', 0, 0, 'Active'),
(107, 11, 6, 1, 6, NULL, NULL, 0, 'GIC & Term Deposits ', 1, 1, 'Active'),
(108, 11, 6, 3, 46, NULL, NULL, 0, 'Commodity Credit Loans ', 0, 1, 'Active'),
(109, 11, 6, 4, 107, NULL, NULL, 0, 'Subcontracted Services ', 0, 1, 'Active'),
(110, 12, 7, 1, 364, NULL, NULL, 1, 'Accounts Receivable', 0, 0, 'Active'),
(111, 12, 7, 1, 366, NULL, NULL, 1, 'Cash on Hand', 0, 0, 'Active'),
(112, 12, 7, 1, 363, NULL, NULL, 1, 'Unrealized Gain on Foreign Currency', 0, 0, 'Active'),
(113, 12, 7, 2, 362, NULL, NULL, 1, 'Accounts Payable', 0, 0, 'Active'),
(114, 12, 7, 2, 361, NULL, NULL, 1, 'Unrealized Loss on Foreign Currency', 0, 0, 'Active'),
(115, 12, 7, 3, 360, NULL, NULL, 1, 'Gain on Foreign Exchange', 0, 0, 'Active'),
(116, 12, 7, 3, 365, NULL, NULL, 0, 'Sales', 0, 1, 'Active'),
(117, 12, 7, 4, 335, NULL, NULL, 0, 'Accounting Fees', 0, 1, 'Active'),
(118, 12, 7, 4, 359, NULL, NULL, 1, 'Loss on Foreign Exchange', 0, 0, 'Active'),
(119, 12, 7, 4, 346, NULL, NULL, 0, 'Office Supplies', 0, 1, 'Active'),
(120, 12, 7, 4, 351, NULL, NULL, 0, 'Rent Expense', 0, 1, 'Active'),
(121, 12, 7, 4, 162, NULL, NULL, 0, 'Telephone – Land Line ', 0, 1, 'Active'),
(122, 12, 7, 4, 355, NULL, NULL, 0, 'Utilities', 0, 1, 'Active'),
(123, 12, 7, 5, 91, NULL, NULL, 0, 'Owner Investment / Drawings ', 1, 0, 'Active'),
(124, 12, 7, 5, 358, NULL, NULL, 1, 'Owner\'s Equity', 0, 0, 'Active'),
(125, 12, 7, 1, NULL, NULL, 21, 1, 'Tax Receivable', 0, 0, 'Active'),
(126, 12, 7, 2, NULL, NULL, 21, 1, 'Tax Payable', 0, 0, 'Active'),
(127, 12, 7, 1, 1, 12, NULL, 0, 'icici', 0, 0, 'Active'),
(128, 12, 7, 1, 2, 13, NULL, 0, 'icici', 0, 0, 'Active'),
(129, 13, 8, 1, 364, NULL, NULL, 1, 'Accounts Receivable', 0, 0, 'Active'),
(130, 13, 8, 1, 366, NULL, NULL, 1, 'Cash on Hand', 0, 0, 'Active'),
(131, 13, 8, 1, 363, NULL, NULL, 1, 'Unrealized Gain on Foreign Currency', 0, 0, 'Active'),
(132, 13, 8, 2, 362, NULL, NULL, 1, 'Accounts Payable', 0, 0, 'Active'),
(133, 13, 8, 2, 361, NULL, NULL, 1, 'Unrealized Loss on Foreign Currency', 0, 0, 'Active'),
(134, 13, 8, 3, 360, NULL, NULL, 1, 'Gain on Foreign Exchange', 0, 0, 'Active'),
(135, 13, 8, 3, 365, NULL, NULL, 0, 'Sales', 0, 1, 'Active'),
(136, 13, 8, 4, 335, NULL, NULL, 0, 'Accounting Fees', 0, 1, 'Active'),
(137, 13, 8, 4, 359, NULL, NULL, 1, 'Loss on Foreign Exchange', 0, 0, 'Active'),
(138, 13, 8, 4, 346, NULL, NULL, 0, 'Office Supplies', 0, 1, 'Active'),
(139, 13, 8, 4, 351, NULL, NULL, 0, 'Rent Expense', 0, 1, 'Active'),
(140, 13, 8, 4, 162, NULL, NULL, 0, 'Telephone – Land Line ', 0, 1, 'Active'),
(141, 13, 8, 4, 355, NULL, NULL, 0, 'Utilities', 0, 1, 'Active'),
(142, 13, 8, 5, 91, NULL, NULL, 0, 'Owner Investment / Drawings ', 1, 0, 'Active'),
(143, 13, 8, 5, 358, NULL, NULL, 1, 'Owner\'s Equity', 0, 0, 'Active'),
(144, 13, 8, 1, NULL, NULL, 22, 1, 'Tax Receivable', 0, 0, 'Active'),
(145, 13, 8, 2, NULL, NULL, 22, 1, 'Tax Payable', 0, 0, 'Active'),
(146, 13, 8, 1, 18, NULL, NULL, 0, 'Inventory ', 1, 1, 'Active'),
(147, 13, 8, 1, 2, 14, NULL, 0, 'CRDB Plc', 0, 0, 'Active'),
(148, 15, 9, 1, 364, NULL, NULL, 1, 'Accounts Receivable', 0, 0, 'Active'),
(149, 15, 9, 1, 366, NULL, NULL, 1, 'Cash on Hand', 0, 0, 'Active'),
(150, 15, 9, 1, 363, NULL, NULL, 1, 'Unrealized Gain on Foreign Currency', 0, 0, 'Active'),
(151, 15, 9, 2, 362, NULL, NULL, 1, 'Accounts Payable', 0, 0, 'Active'),
(152, 15, 9, 2, 361, NULL, NULL, 1, 'Unrealized Loss on Foreign Currency', 0, 0, 'Active'),
(153, 15, 9, 3, 360, NULL, NULL, 1, 'Gain on Foreign Exchange', 0, 0, 'Active'),
(154, 15, 9, 3, 365, NULL, NULL, 0, 'Sales', 0, 1, 'Active'),
(155, 15, 9, 4, 335, NULL, NULL, 0, 'Accounting Fees', 0, 1, 'Active'),
(156, 15, 9, 4, 359, NULL, NULL, 1, 'Loss on Foreign Exchange', 0, 0, 'Active'),
(157, 15, 9, 4, 346, NULL, NULL, 0, 'Office Supplies', 0, 1, 'Active'),
(158, 15, 9, 4, 351, NULL, NULL, 0, 'Rent Expense', 0, 1, 'Active'),
(159, 15, 9, 4, 162, NULL, NULL, 0, 'Telephone – Land Line ', 0, 1, 'Active'),
(160, 15, 9, 4, 355, NULL, NULL, 0, 'Utilities', 0, 1, 'Active'),
(161, 15, 9, 5, 91, NULL, NULL, 0, 'Owner Investment / Drawings ', 1, 0, 'Active'),
(162, 15, 9, 5, 358, NULL, NULL, 1, 'Owner\'s Equity', 0, 0, 'Active'),
(163, 16, 10, 1, 364, NULL, NULL, 1, 'Accounts Receivable', 0, 0, 'Active'),
(164, 16, 10, 1, 366, NULL, NULL, 1, 'Cash on Hand', 0, 0, 'Active'),
(165, 16, 10, 1, 363, NULL, NULL, 1, 'Unrealized Gain on Foreign Currency', 0, 0, 'Active'),
(166, 16, 10, 2, 362, NULL, NULL, 1, 'Accounts Payable', 0, 0, 'Active'),
(167, 16, 10, 2, 361, NULL, NULL, 1, 'Unrealized Loss on Foreign Currency', 0, 0, 'Active'),
(168, 16, 10, 3, 360, NULL, NULL, 1, 'Gain on Foreign Exchange', 0, 0, 'Active'),
(169, 16, 10, 3, 365, NULL, NULL, 0, 'Sales', 0, 1, 'Active'),
(170, 16, 10, 4, 335, NULL, NULL, 0, 'Accounting Fees', 0, 1, 'Active'),
(171, 16, 10, 4, 359, NULL, NULL, 1, 'Loss on Foreign Exchange', 0, 0, 'Active'),
(172, 16, 10, 4, 346, NULL, NULL, 0, 'Office Supplies', 0, 1, 'Active'),
(173, 16, 10, 4, 351, NULL, NULL, 0, 'Rent Expense', 0, 1, 'Active'),
(174, 16, 10, 4, 162, NULL, NULL, 0, 'Telephone – Land Line ', 0, 1, 'Active'),
(175, 16, 10, 4, 355, NULL, NULL, 0, 'Utilities', 0, 1, 'Active'),
(176, 16, 10, 5, 91, NULL, NULL, 0, 'Owner Investment / Drawings ', 1, 0, 'Active'),
(177, 16, 10, 5, 358, NULL, NULL, 1, 'Owner\'s Equity', 0, 0, 'Active'),
(178, 16, 10, 1, NULL, NULL, 23, 1, 'VAT Receivable', 0, 0, 'Active'),
(179, 16, 10, 2, NULL, NULL, 23, 1, 'VAT Payable', 0, 0, 'Active'),
(180, 16, 10, 1, 7, NULL, NULL, 0, 'Certificate of Deposit ', 1, 1, 'Active'),
(181, 16, 10, 1, 1, 15, NULL, 0, 'icici', 0, 0, 'Active'),
(182, 16, 10, 2, 27, NULL, NULL, 0, 'Billing Accounts ', 1, 1, 'Active'),
(183, 16, 10, 1, 6, NULL, NULL, 0, 'GIC & Term Deposits ', 1, 1, 'Active'),
(184, 16, 10, 3, 60, NULL, NULL, 0, 'Fuel Surcharge ', 0, 1, 'Active'),
(185, 16, 10, 1, NULL, NULL, 24, 1, 'service Receivable', 0, 0, 'Active'),
(186, 16, 10, 2, NULL, NULL, 24, 1, 'service Payable', 0, 0, 'Active'),
(188, 16, 10, 2, 25, NULL, NULL, 0, 'Bank Overdraft ', 1, 1, 'Active'),
(189, 17, 11, 1, 364, NULL, NULL, 1, 'Accounts Receivable', 0, 0, 'Active'),
(190, 17, 11, 1, 366, NULL, NULL, 1, 'Cash on Hand', 0, 0, 'Active'),
(191, 17, 11, 1, 363, NULL, NULL, 1, 'Unrealized Gain on Foreign Currency', 0, 0, 'Active'),
(192, 17, 11, 2, 362, NULL, NULL, 1, 'Accounts Payable', 0, 0, 'Active'),
(193, 17, 11, 2, 361, NULL, NULL, 1, 'Unrealized Loss on Foreign Currency', 0, 0, 'Active'),
(194, 17, 11, 3, 360, NULL, NULL, 1, 'Gain on Foreign Exchange', 0, 0, 'Active'),
(195, 17, 11, 3, 365, NULL, NULL, 0, 'Sales', 0, 1, 'Active'),
(196, 17, 11, 4, 335, NULL, NULL, 0, 'Accounting Fees', 0, 1, 'Active'),
(197, 17, 11, 4, 359, NULL, NULL, 1, 'Loss on Foreign Exchange', 0, 0, 'Active'),
(198, 17, 11, 4, 346, NULL, NULL, 0, 'Office Supplies', 0, 1, 'Active'),
(199, 17, 11, 4, 351, NULL, NULL, 0, 'Rent Expense', 0, 1, 'Active'),
(200, 17, 11, 4, 162, NULL, NULL, 0, 'Telephone – Land Line ', 0, 1, 'Active'),
(201, 17, 11, 4, 355, NULL, NULL, 0, 'Utilities', 0, 1, 'Active'),
(202, 17, 11, 5, 91, NULL, NULL, 0, 'Owner Investment / Drawings ', 1, 0, 'Active'),
(203, 17, 11, 5, 358, NULL, NULL, 1, 'Owner\'s Equity', 0, 0, 'Active'),
(204, 19, 12, 1, 364, NULL, NULL, 1, 'Accounts Receivable', 0, 0, 'Active'),
(205, 19, 12, 1, 366, NULL, NULL, 1, 'Cash on Hand', 0, 0, 'Active'),
(206, 19, 12, 1, 363, NULL, NULL, 1, 'Unrealized Gain on Foreign Currency', 0, 0, 'Active'),
(207, 19, 12, 2, 362, NULL, NULL, 1, 'Accounts Payable', 0, 0, 'Active'),
(208, 19, 12, 2, 361, NULL, NULL, 1, 'Unrealized Loss on Foreign Currency', 0, 0, 'Active'),
(209, 19, 12, 3, 360, NULL, NULL, 1, 'Gain on Foreign Exchange', 0, 0, 'Active'),
(210, 19, 12, 3, 365, NULL, NULL, 0, 'Sales', 0, 1, 'Active'),
(211, 19, 12, 4, 335, NULL, NULL, 0, 'Accounting Fees', 0, 1, 'Active'),
(212, 19, 12, 4, 359, NULL, NULL, 1, 'Loss on Foreign Exchange', 0, 0, 'Active'),
(213, 19, 12, 4, 346, NULL, NULL, 0, 'Office Supplies', 0, 1, 'Active'),
(214, 19, 12, 4, 351, NULL, NULL, 0, 'Rent Expense', 0, 1, 'Active'),
(215, 19, 12, 4, 162, NULL, NULL, 0, 'Telephone – Land Line ', 0, 1, 'Active'),
(216, 19, 12, 4, 355, NULL, NULL, 0, 'Utilities', 0, 1, 'Active'),
(217, 19, 12, 5, 91, NULL, NULL, 0, 'Owner Investment / Drawings ', 1, 0, 'Active'),
(218, 19, 12, 5, 358, NULL, NULL, 1, 'Owner\'s Equity', 0, 0, 'Active'),
(219, 19, 12, 1, NULL, NULL, 25, 1, 'VAT Receivable', 0, 0, 'Active'),
(220, 19, 12, 2, NULL, NULL, 25, 1, 'VAT Payable', 0, 0, 'Active'),
(221, 19, 12, 1, 8, NULL, NULL, 0, 'Other Asset Account ', 1, 1, 'Active'),
(222, 19, 12, 1, 1, 17, NULL, 0, 'icici', 0, 0, 'Active'),
(223, 20, 13, 1, 364, NULL, NULL, 1, 'Accounts Receivable', 0, 0, 'Active'),
(224, 20, 13, 1, 366, NULL, NULL, 1, 'Cash on Hand', 0, 0, 'Active'),
(225, 20, 13, 1, 363, NULL, NULL, 1, 'Unrealized Gain on Foreign Currency', 0, 0, 'Active'),
(226, 20, 13, 2, 362, NULL, NULL, 1, 'Accounts Payable', 0, 0, 'Active'),
(227, 20, 13, 2, 361, NULL, NULL, 1, 'Unrealized Loss on Foreign Currency', 0, 0, 'Active'),
(228, 20, 13, 3, 360, NULL, NULL, 1, 'Gain on Foreign Exchange', 0, 0, 'Active'),
(229, 20, 13, 3, 365, NULL, NULL, 0, 'Sales', 0, 1, 'Active'),
(230, 20, 13, 4, 335, NULL, NULL, 0, 'Accounting Fees', 0, 1, 'Active'),
(231, 20, 13, 4, 359, NULL, NULL, 1, 'Loss on Foreign Exchange', 0, 0, 'Active'),
(232, 20, 13, 4, 346, NULL, NULL, 0, 'Office Supplies', 0, 1, 'Active'),
(233, 20, 13, 4, 351, NULL, NULL, 0, 'Rent Expense', 0, 1, 'Active'),
(234, 20, 13, 4, 162, NULL, NULL, 0, 'Telephone – Land Line ', 0, 1, 'Active'),
(235, 20, 13, 4, 355, NULL, NULL, 0, 'Utilities', 0, 1, 'Active'),
(236, 20, 13, 5, 91, NULL, NULL, 0, 'Owner Investment / Drawings ', 1, 0, 'Active'),
(237, 20, 13, 5, 358, NULL, NULL, 1, 'Owner\'s Equity', 0, 0, 'Active'),
(238, 21, 14, 1, 364, NULL, NULL, 1, 'Accounts Receivable', 0, 0, 'Active'),
(239, 21, 14, 1, 366, NULL, NULL, 1, 'Cash on Hand', 0, 0, 'Active'),
(240, 21, 14, 1, 363, NULL, NULL, 1, 'Unrealized Gain on Foreign Currency', 0, 0, 'Active'),
(241, 21, 14, 2, 362, NULL, NULL, 1, 'Accounts Payable', 0, 0, 'Active'),
(242, 21, 14, 2, 361, NULL, NULL, 1, 'Unrealized Loss on Foreign Currency', 0, 0, 'Active'),
(243, 21, 14, 3, 360, NULL, NULL, 1, 'Gain on Foreign Exchange', 0, 0, 'Active'),
(244, 21, 14, 3, 365, NULL, NULL, 0, 'Sales', 0, 1, 'Active'),
(245, 21, 14, 4, 335, NULL, NULL, 0, 'Accounting Fees', 0, 1, 'Active'),
(246, 21, 14, 4, 359, NULL, NULL, 1, 'Loss on Foreign Exchange', 0, 0, 'Active'),
(247, 21, 14, 4, 346, NULL, NULL, 0, 'Office Supplies', 0, 1, 'Active'),
(248, 21, 14, 4, 351, NULL, NULL, 0, 'Rent Expense', 0, 1, 'Active'),
(249, 21, 14, 4, 162, NULL, NULL, 0, 'Telephone – Land Line ', 0, 1, 'Active'),
(250, 21, 14, 4, 355, NULL, NULL, 0, 'Utilities', 0, 1, 'Active'),
(251, 21, 14, 5, 91, NULL, NULL, 0, 'Owner Investment / Drawings ', 1, 0, 'Active'),
(252, 21, 14, 5, 358, NULL, NULL, 1, 'Owner\'s Equity', 0, 0, 'Active'),
(253, 22, 15, 1, 364, NULL, NULL, 1, 'Accounts Receivable', 0, 0, 'Active'),
(254, 22, 15, 1, 366, NULL, NULL, 1, 'Cash on Hand', 0, 0, 'Active'),
(255, 22, 15, 1, 363, NULL, NULL, 1, 'Unrealized Gain on Foreign Currency', 0, 0, 'Active'),
(256, 22, 15, 2, 362, NULL, NULL, 1, 'Accounts Payable', 0, 0, 'Active'),
(257, 22, 15, 2, 361, NULL, NULL, 1, 'Unrealized Loss on Foreign Currency', 0, 0, 'Active'),
(258, 22, 15, 3, 360, NULL, NULL, 1, 'Gain on Foreign Exchange', 0, 0, 'Active'),
(259, 22, 15, 3, 365, NULL, NULL, 0, 'Sales', 0, 1, 'Active'),
(260, 22, 15, 4, 335, NULL, NULL, 0, 'Accounting Fees', 0, 1, 'Active'),
(261, 22, 15, 4, 359, NULL, NULL, 1, 'Loss on Foreign Exchange', 0, 0, 'Active'),
(262, 22, 15, 4, 346, NULL, NULL, 0, 'Office Supplies', 0, 1, 'Active'),
(263, 22, 15, 4, 351, NULL, NULL, 0, 'Rent Expense', 0, 1, 'Active'),
(264, 22, 15, 4, 162, NULL, NULL, 0, 'Telephone – Land Line ', 0, 1, 'Active'),
(265, 22, 15, 4, 355, NULL, NULL, 0, 'Utilities', 0, 1, 'Active'),
(266, 22, 15, 5, 91, NULL, NULL, 0, 'Owner Investment / Drawings ', 1, 0, 'Active'),
(267, 22, 15, 5, 358, NULL, NULL, 1, 'Owner\'s Equity', 0, 0, 'Active'),
(268, 23, 16, 1, 364, NULL, NULL, 1, 'Accounts Receivable', 0, 0, 'Active'),
(269, 23, 16, 1, 366, NULL, NULL, 1, 'Cash on Hand', 0, 0, 'Active'),
(270, 23, 16, 1, 363, NULL, NULL, 1, 'Unrealized Gain on Foreign Currency', 0, 0, 'Active'),
(271, 23, 16, 2, 362, NULL, NULL, 1, 'Accounts Payable', 0, 0, 'Active'),
(272, 23, 16, 2, 361, NULL, NULL, 1, 'Unrealized Loss on Foreign Currency', 0, 0, 'Active'),
(273, 23, 16, 3, 360, NULL, NULL, 1, 'Gain on Foreign Exchange', 0, 0, 'Active'),
(274, 23, 16, 3, 365, NULL, NULL, 0, 'Sales', 0, 1, 'Active'),
(275, 23, 16, 4, 335, NULL, NULL, 0, 'Accounting Fees', 0, 1, 'Active'),
(276, 23, 16, 4, 359, NULL, NULL, 1, 'Loss on Foreign Exchange', 0, 0, 'Active'),
(277, 23, 16, 4, 346, NULL, NULL, 0, 'Office Supplies', 0, 1, 'Active'),
(278, 23, 16, 4, 351, NULL, NULL, 0, 'Rent Expense', 0, 1, 'Active'),
(279, 23, 16, 4, 162, NULL, NULL, 0, 'Telephone – Land Line ', 0, 1, 'Active'),
(280, 23, 16, 4, 355, NULL, NULL, 0, 'Utilities', 0, 1, 'Active'),
(281, 23, 16, 5, 91, NULL, NULL, 0, 'Owner Investment / Drawings ', 1, 0, 'Active'),
(282, 23, 16, 5, 358, NULL, NULL, 1, 'Owner\'s Equity', 0, 0, 'Active'),
(283, 24, 17, 1, 364, NULL, NULL, 1, 'Accounts Receivable', 0, 0, 'Active'),
(284, 24, 17, 1, 366, NULL, NULL, 1, 'Cash on Hand', 0, 0, 'Active'),
(285, 24, 17, 1, 363, NULL, NULL, 1, 'Unrealized Gain on Foreign Currency', 0, 0, 'Active'),
(286, 24, 17, 2, 362, NULL, NULL, 1, 'Accounts Payable', 0, 0, 'Active'),
(287, 24, 17, 2, 361, NULL, NULL, 1, 'Unrealized Loss on Foreign Currency', 0, 0, 'Active'),
(288, 24, 17, 3, 360, NULL, NULL, 1, 'Gain on Foreign Exchange', 0, 0, 'Active'),
(289, 24, 17, 3, 365, NULL, NULL, 0, 'Sales', 0, 1, 'Active'),
(290, 24, 17, 4, 335, NULL, NULL, 0, 'Accounting Fees', 0, 1, 'Active'),
(291, 24, 17, 4, 359, NULL, NULL, 1, 'Loss on Foreign Exchange', 0, 0, 'Active'),
(292, 24, 17, 4, 346, NULL, NULL, 0, 'Office Supplies', 0, 1, 'Active'),
(293, 24, 17, 4, 351, NULL, NULL, 0, 'Rent Expense', 0, 1, 'Active'),
(294, 24, 17, 4, 162, NULL, NULL, 0, 'Telephone – Land Line ', 0, 1, 'Active'),
(295, 24, 17, 4, 355, NULL, NULL, 0, 'Utilities', 0, 1, 'Active'),
(296, 24, 17, 5, 91, NULL, NULL, 0, 'Owner Investment / Drawings ', 1, 0, 'Active'),
(297, 24, 17, 5, 358, NULL, NULL, 1, 'Owner\'s Equity', 0, 0, 'Active'),
(298, 13, 8, 1, 6, NULL, NULL, 0, 'GIC & Term Deposits ', 1, 1, 'Trash'),
(299, 13, 8, 3, 54, NULL, NULL, 0, 'Commission Income ', 0, 1, 'Active'),
(301, 13, 8, 1, NULL, NULL, 26, 1, 'VAT Receivable', 0, 0, 'Active'),
(302, 13, 8, 2, NULL, NULL, 26, 1, 'VAT Payable', 0, 0, 'Active'),
(303, 13, 8, 4, 110, NULL, NULL, 0, 'Equipment Rental for Jobs ', 0, 1, 'Active'),
(304, 13, 8, 4, 356, NULL, NULL, 0, 'Vehicle – Fuel', 0, 1, 'Active'),
(305, 13, 8, 5, 104, NULL, NULL, 0, 'Currency Adjustments ', 1, 1, 'Active'),
(310, 13, 8, 4, 111, NULL, NULL, 0, 'Fuel ', 0, 1, 'Active'),
(311, 13, 8, 3, 266, NULL, NULL, 0, 'Sales – Retail Products ', 0, 1, 'Active'),
(312, 13, 8, 1, 2, 25, NULL, 0, 'AKIBA Commercial Bank', 0, 0, 'Active'),
(313, 13, 8, 1, 290, NULL, NULL, 0, 'Land Improvements ', 1, 1, 'Active'),
(314, 26, 18, 1, 364, NULL, NULL, 1, 'Accounts Receivable', 0, 0, 'Active'),
(315, 26, 18, 1, 366, NULL, NULL, 1, 'Cash on Hand', 0, 0, 'Active'),
(316, 26, 18, 1, 363, NULL, NULL, 1, 'Unrealized Gain on Foreign Currency', 0, 0, 'Active'),
(317, 26, 18, 2, 362, NULL, NULL, 1, 'Accounts Payable', 0, 0, 'Active'),
(318, 26, 18, 2, 361, NULL, NULL, 1, 'Unrealized Loss on Foreign Currency', 0, 0, 'Active'),
(319, 26, 18, 3, 360, NULL, NULL, 1, 'Gain on Foreign Exchange', 0, 0, 'Active'),
(320, 26, 18, 3, 365, NULL, NULL, 0, 'Sales', 0, 1, 'Active'),
(321, 26, 18, 4, 335, NULL, NULL, 0, 'Accounting Fees', 0, 1, 'Active'),
(322, 26, 18, 4, 359, NULL, NULL, 1, 'Loss on Foreign Exchange', 0, 0, 'Active'),
(323, 26, 18, 4, 346, NULL, NULL, 0, 'Office Supplies', 0, 1, 'Active'),
(324, 26, 18, 4, 351, NULL, NULL, 0, 'Rent Expense', 0, 1, 'Active'),
(325, 26, 18, 4, 162, NULL, NULL, 0, 'Telephone – Land Line ', 0, 1, 'Active'),
(326, 26, 18, 4, 355, NULL, NULL, 0, 'Utilities', 0, 1, 'Active'),
(327, 26, 18, 5, 91, NULL, NULL, 0, 'Owner Investment / Drawings ', 1, 0, 'Active'),
(328, 26, 18, 5, 358, NULL, NULL, 1, 'Owner\'s Equity', 0, 0, 'Active'),
(329, 26, 18, 4, 336, NULL, NULL, 0, 'Advertising & Promotion', 0, 1, 'Active'),
(330, 26, 18, 4, 129, NULL, NULL, 0, 'Purchases – Resale Items ', 0, 1, 'Active'),
(331, 26, 18, 1, 18, NULL, NULL, 0, 'Inventory ', 1, 1, 'Active'),
(332, 26, 18, 1, 2, 26, NULL, 0, 'ABSA', 0, 0, 'Active'),
(333, 26, 18, 1, NULL, NULL, 27, 1, 'Standard VAT Receivable', 0, 0, 'Active'),
(334, 26, 18, 2, NULL, NULL, 27, 1, 'Standard VAT Payable', 0, 0, 'Active'),
(335, 26, 18, 4, 154, NULL, NULL, 0, 'Other Expenses ', 0, 1, 'Active'),
(336, 27, 19, 1, 364, NULL, NULL, 1, 'Accounts Receivable', 0, 0, 'Active'),
(337, 27, 19, 1, 366, NULL, NULL, 1, 'Cash on Hand', 0, 0, 'Active'),
(338, 27, 19, 1, 363, NULL, NULL, 1, 'Unrealized Gain on Foreign Currency', 0, 0, 'Active'),
(339, 27, 19, 2, 362, NULL, NULL, 1, 'Accounts Payable', 0, 0, 'Active'),
(340, 27, 19, 2, 361, NULL, NULL, 1, 'Unrealized Loss on Foreign Currency', 0, 0, 'Active'),
(341, 27, 19, 3, 360, NULL, NULL, 1, 'Gain on Foreign Exchange', 0, 0, 'Active'),
(342, 27, 19, 3, 365, NULL, NULL, 0, 'Sales', 0, 1, 'Active'),
(343, 27, 19, 4, 335, NULL, NULL, 0, 'Accounting Fees', 0, 1, 'Active'),
(344, 27, 19, 4, 359, NULL, NULL, 1, 'Loss on Foreign Exchange', 0, 0, 'Active'),
(345, 27, 19, 4, 346, NULL, NULL, 0, 'Office Supplies', 0, 1, 'Active'),
(346, 27, 19, 4, 351, NULL, NULL, 0, 'Rent Expense', 0, 1, 'Active'),
(347, 27, 19, 4, 162, NULL, NULL, 0, 'Telephone – Land Line ', 0, 1, 'Active'),
(348, 27, 19, 4, 355, NULL, NULL, 0, 'Utilities', 0, 1, 'Active'),
(349, 27, 19, 5, 91, NULL, NULL, 0, 'Owner Investment / Drawings ', 1, 0, 'Active'),
(350, 27, 19, 5, 358, NULL, NULL, 1, 'Owner\'s Equity', 0, 0, 'Active'),
(351, 27, 19, 3, 251, NULL, NULL, 0, 'Other Income', 0, 1, 'Active'),
(352, 27, 19, 1, NULL, NULL, 28, 1, 'Standard VAT Receivable', 0, 0, 'Active'),
(353, 27, 19, 2, NULL, NULL, 28, 1, 'Standard VAT Payable', 0, 0, 'Active'),
(354, 27, 19, 4, 129, NULL, NULL, 0, 'Purchases – Resale Items ', 0, 1, 'Active'),
(355, 27, 19, 1, 2, 27, NULL, 0, 'ABSA', 0, 0, 'Active'),
(356, 13, 8, 1, 11, NULL, NULL, 0, 'asdasd', 1, 1, 'Active'),
(357, 13, 8, 4, 128, NULL, NULL, 0, 'Purchases – Parts & Materials ', 0, 1, 'Active'),
(358, 28, 20, 1, 364, NULL, NULL, 1, 'Accounts Receivable', 0, 0, 'Active'),
(359, 28, 20, 1, 366, NULL, NULL, 1, 'Cash on Hand', 0, 0, 'Active'),
(360, 28, 20, 1, 363, NULL, NULL, 1, 'Unrealized Gain on Foreign Currency', 0, 0, 'Active'),
(361, 28, 20, 2, 362, NULL, NULL, 1, 'Accounts Payable', 0, 0, 'Active'),
(362, 28, 20, 2, 361, NULL, NULL, 1, 'Unrealized Loss on Foreign Currency', 0, 0, 'Active'),
(363, 28, 20, 3, 360, NULL, NULL, 1, 'Gain on Foreign Exchange', 0, 0, 'Active'),
(364, 28, 20, 3, 365, NULL, NULL, 0, 'Sales', 0, 1, 'Active'),
(365, 28, 20, 4, 335, NULL, NULL, 0, 'Accounting Fees', 0, 1, 'Active'),
(366, 28, 20, 4, 359, NULL, NULL, 1, 'Loss on Foreign Exchange', 0, 0, 'Active'),
(367, 28, 20, 4, 346, NULL, NULL, 0, 'Office Supplies', 0, 1, 'Active'),
(368, 28, 20, 4, 351, NULL, NULL, 0, 'Rent Expense', 0, 1, 'Active'),
(369, 28, 20, 4, 162, NULL, NULL, 0, 'Telephone – Land Line ', 0, 1, 'Active'),
(370, 28, 20, 4, 355, NULL, NULL, 0, 'Utilities', 0, 1, 'Active'),
(371, 28, 20, 5, 91, NULL, NULL, 0, 'Owner Investment / Drawings ', 1, 0, 'Active'),
(372, 28, 20, 5, 358, NULL, NULL, 1, 'Owner\'s Equity', 0, 0, 'Active'),
(373, 29, 21, 1, 364, NULL, NULL, 1, 'Accounts Receivable', 0, 0, 'Active'),
(374, 29, 21, 1, 366, NULL, NULL, 1, 'Cash on Hand', 0, 0, 'Active'),
(375, 29, 21, 1, 363, NULL, NULL, 1, 'Unrealized Gain on Foreign Currency', 0, 0, 'Active'),
(376, 29, 21, 2, 362, NULL, NULL, 1, 'Accounts Payable', 0, 0, 'Active'),
(377, 29, 21, 2, 361, NULL, NULL, 1, 'Unrealized Loss on Foreign Currency', 0, 0, 'Active'),
(378, 29, 21, 3, 360, NULL, NULL, 1, 'Gain on Foreign Exchange', 0, 0, 'Active'),
(379, 29, 21, 3, 365, NULL, NULL, 0, 'Sales', 0, 1, 'Active'),
(380, 29, 21, 4, 335, NULL, NULL, 0, 'Accounting Fees', 0, 1, 'Active'),
(381, 29, 21, 4, 359, NULL, NULL, 1, 'Loss on Foreign Exchange', 0, 0, 'Active'),
(382, 29, 21, 4, 346, NULL, NULL, 0, 'Office Supplies', 0, 1, 'Active'),
(383, 29, 21, 4, 351, NULL, NULL, 0, 'Rent Expense', 0, 1, 'Active'),
(384, 29, 21, 4, 162, NULL, NULL, 0, 'Telephone – Land Line ', 0, 1, 'Active'),
(385, 29, 21, 4, 355, NULL, NULL, 0, 'Utilities', 0, 1, 'Active'),
(386, 29, 21, 5, 91, NULL, NULL, 0, 'Owner Investment / Drawings ', 1, 0, 'Active'),
(387, 29, 21, 5, 358, NULL, NULL, 1, 'Owner\'s Equity', 0, 0, 'Active'),
(388, 29, 21, 1, 1, 28, NULL, 0, 'yedy', 0, 0, 'Active'),
(389, 13, 8, 1, 11, NULL, NULL, 0, 'Shares ', 1, 1, 'Active'),
(390, 31, 22, 1, 364, NULL, NULL, 1, 'Accounts Receivable', 0, 0, 'Active'),
(391, 31, 22, 1, 366, NULL, NULL, 1, 'Cash on Hand', 0, 0, 'Active'),
(392, 31, 22, 1, 363, NULL, NULL, 1, 'Unrealized Gain on Foreign Currency', 0, 0, 'Active'),
(393, 31, 22, 2, 362, NULL, NULL, 1, 'Accounts Payable', 0, 0, 'Active'),
(394, 31, 22, 2, 361, NULL, NULL, 1, 'Unrealized Loss on Foreign Currency', 0, 0, 'Active'),
(395, 31, 22, 3, 360, NULL, NULL, 1, 'Gain on Foreign Exchange', 0, 0, 'Active'),
(396, 31, 22, 3, 365, NULL, NULL, 0, 'Sales', 0, 1, 'Active'),
(397, 31, 22, 4, 335, NULL, NULL, 0, 'Accounting Fees', 0, 1, 'Active'),
(398, 31, 22, 4, 359, NULL, NULL, 1, 'Loss on Foreign Exchange', 0, 0, 'Active'),
(399, 31, 22, 4, 346, NULL, NULL, 0, 'Office Supplies', 0, 1, 'Active'),
(400, 31, 22, 4, 351, NULL, NULL, 0, 'Rent Expense', 0, 1, 'Active'),
(401, 31, 22, 4, 162, NULL, NULL, 0, 'Telephone – Land Line ', 0, 1, 'Active'),
(402, 31, 22, 4, 355, NULL, NULL, 0, 'Utilities', 0, 1, 'Active'),
(403, 31, 22, 5, 91, NULL, NULL, 0, 'Owner Investment / Drawings ', 1, 0, 'Active'),
(404, 31, 22, 5, 358, NULL, NULL, 1, 'Owner\'s Equity', 0, 0, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `fin_users`
--

CREATE TABLE `fin_users` (
  `uid` int(11) NOT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `verify` enum('0','1') COLLATE utf8_unicode_ci NOT NULL COMMENT 'email verification set as 1',
  `currency` int(11) NOT NULL,
  `ip` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `createddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fkey` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Forgot password key',
  `status` enum('Active','Inactive','Trash','Account Setup','Partial') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_users`
--

INSERT INTO `fin_users` (`uid`, `firstname`, `lastname`, `email`, `password`, `link`, `address1`, `address2`, `city`, `state`, `country`, `zip`, `key`, `verify`, `currency`, `ip`, `createddate`, `fkey`, `status`) VALUES
(8, 'John', 'Smith', 'john@phpscriptsonline.com', 'f865b53623b121fd34ee5426c792e5c33af8c227', 'shanmugam1380716524', '', '', 'madurai', '', '97', NULL, 'c7omwthrzu8byt2k', '0', 0, '', '2013-10-02 12:22:04', '', 'Active'),
(9, 'vignesh', 'k', 'vignesh@phpscriptsonline.com.com', 'cbdbe4936ce8be63184d9f2e13fc249234371b9a', 'vignesh1380716715', NULL, NULL, NULL, NULL, NULL, NULL, 'uwg1g3opm60f37hi', '0', 0, '', '2013-10-02 12:25:15', 'ozqnp4d4h07u8yvpahvnjvnnivdklb', 'Active'),
(10, 'shunmugam', 'sundaram', 'krish@phpscriptsonline.com.com', 'cbdbe4936ce8be63184d9f2e13fc249234371b9a', 'shunmugam1380776561', NULL, NULL, NULL, NULL, NULL, NULL, 'f4128jbo353ii14i', '0', 0, '', '2013-10-03 05:02:41', '', 'Active'),
(11, 'vignesh', 'waran', 'senthil@phpscriptsonline.com.com', '6a0e3ee49aab85d2354db44c0cb759c462e48cdb', 'vignesh1389612669', NULL, NULL, NULL, NULL, NULL, NULL, 'h8ib9pktr7mvqopm', '0', 0, '', '2014-01-13 11:31:09', '', 'Active'),
(12, 'karthi', 'krishna', 'testk777@gmail.com', '7c941ff0d2d3c683462f3a2cdd1ac5e315b70a78', 'karthi1393914092', NULL, NULL, NULL, NULL, NULL, NULL, 'vv48nzd5piwld0mb', '0', 0, '', '2014-03-04 06:21:32', '', 'Active'),
(13, 'Dev Raj', 'Pokhrel', 'testuserlt1@gmail.com', '7288edd0fc3ffcbe93a0cf06e3568e28521687bc', 'test1394080214', 'Butwal - 12, Rupandehi', '', 'Butwal', 'Lumbini', '143', NULL, '1t1ag19yfaka0t27', '0', 0, '', '2014-03-06 04:30:14', '', 'Active'),
(14, 'vignesh', 'krishna', 'fgfdg@gmail.com', '7c941ff0d2d3c683462f3a2cdd1ac5e315b70a78', 'vignesh1394103239', NULL, NULL, NULL, NULL, NULL, NULL, 'v42p11p4g6061qzn', '0', 0, '', '2014-03-06 10:53:59', '', 'Account Setup'),
(15, 'test', 'test', 'sadfasd@gmail.com', 'cdbe0f406c5fb3da3d9df310958d266d3a9ffabc', 'test1394103367', NULL, NULL, NULL, NULL, NULL, NULL, 'uk5ih2aos6ww7ccl', '0', 0, '', '2014-03-06 10:56:07', '', 'Active'),
(16, 'john', 'peter', 'testuserlt2@gmail.com', '7c941ff0d2d3c683462f3a2cdd1ac5e315b70a78', 'john1394107626', NULL, NULL, NULL, NULL, NULL, NULL, 'vy7qhqqtrld1334n', '0', 0, '', '2014-03-06 12:07:06', '', 'Active'),
(17, 'Karthi', 'Karthi', 'Karthi@gmail.com', '43901b57bef8092b85bcabdbbd8ad4be5eb77ae6', 'karthi1395643634', NULL, NULL, NULL, NULL, NULL, NULL, 'wqhqnhvc4knb14pd', '0', 0, '', '2014-03-24 06:47:14', '', 'Active'),
(18, 'Jack', 'had', 'testuserlt4@gmail.com', '7c941ff0d2d3c683462f3a2cdd1ac5e315b70a78', 'jack1395653277', NULL, NULL, NULL, NULL, NULL, NULL, '9f3hltonu8nrgjzn', '0', 0, '', '2014-03-24 09:27:57', '', 'Account Setup'),
(19, 'Jack', 'had', 'testuserlt3@gmail.com', '7c941ff0d2d3c683462f3a2cdd1ac5e315b70a78', 'jack1395653753', NULL, NULL, NULL, NULL, NULL, NULL, '2zq9z8e3nimtko5s', '0', 0, '', '2014-03-24 09:35:53', '', 'Active'),
(20, 'Sudhakar', 'Rajamanickam', 'sudhakarba.phpscriptsonline@gmail.com', 'b536da50724070f213bfaeafe63780e5f831fcca', 'sudhakar1432282844', NULL, NULL, NULL, NULL, NULL, NULL, 'kiizm6339e7gbzk9', '0', 0, '', '2015-05-22 08:20:44', '', 'Active'),
(21, 'MUNIKISHORE', 'MALLU', 'mallu.munikishore@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'munikishore1435482814', NULL, NULL, NULL, NULL, NULL, NULL, '19peww3y5yr7uj8c', '0', 0, '', '2015-06-28 09:13:34', '', 'Active'),
(22, 'sreenivas', 'Reddy', 'ysreddy400@gmail.com', '0a08d86960c3b4b07dec61907cf49ef8ae3dbb58', 'sreenivas1435640866', NULL, NULL, NULL, NULL, NULL, NULL, 'n4nfkedq50ltrwpd', '0', 0, '', '2015-06-30 05:07:46', '', 'Active'),
(23, 'Bollapally', 'srinath', 'naanys@yahoo.com', '4354dc61658ce8d9b45bf314231624e8bed61b7f', 'bollapally1435981837', NULL, NULL, NULL, NULL, NULL, NULL, 'ho8om0jsv5aoo10p', '0', 0, '', '2015-07-04 03:50:37', '', 'Active'),
(24, 'Balamurugan', 'Nagarajan', 'balamurugan.phpscriptsonline@gmail.com', '7288edd0fc3ffcbe93a0cf06e3568e28521687bc', 'balamurugan1471674999', NULL, NULL, NULL, NULL, NULL, NULL, 'a2ql2gnhmhzv49gq', '0', 0, '', '2016-08-20 06:36:39', '', 'Active'),
(25, 'Nitesh', 'cnjs', 'ag03327@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'nitesh1506406193', NULL, NULL, NULL, NULL, NULL, NULL, 'adcrbyc9zf22aebe', '0', 0, '', '2017-09-26 06:09:53', '', 'Account Setup'),
(26, 'malesela', 'banda', 'admin@bandabookkeepers.com', '9bc34549d565d9505b287de0cd20ac77be1d3f2c', 'malesela1559655634', NULL, NULL, NULL, NULL, NULL, NULL, 'w1qk48ptrq1ya0c1', '0', 0, '', '2019-06-04 13:40:35', '', 'Active'),
(27, 'malesela', 'banda', 'bandabookkeepers@gmail.com', '340c9a409bb620038b0cb6c1dbc832c049327b0a', 'malesela1560235842', NULL, NULL, NULL, NULL, NULL, NULL, 'stzwvniamiqe6195', '0', 0, '', '2019-06-11 06:50:41', '', 'Active'),
(28, 'Manan', 'Patel', 'ttechmanan@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'manan1577428116', NULL, NULL, NULL, NULL, NULL, NULL, 'qffcm5176w6difsj', '0', 0, '', '2019-12-27 06:28:36', '', 'Active'),
(29, 'Hassan', 'Naeem', 'muhammadhassannaeem@gmail.com', '25f817aefcf3b4c70e0b11c230785f940c5f4ccf', 'hassan1578327670', NULL, NULL, NULL, NULL, NULL, NULL, '70vkfrq49zvv5sf4', '0', 0, '', '2020-01-06 16:21:10', '', 'Active'),
(30, '111', '1111', '111@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', '1111581226104', NULL, NULL, NULL, NULL, NULL, NULL, 'kpndey7dg17lrws7', '0', 0, '', '2020-02-09 05:28:24', '', 'Account Setup'),
(31, 'sam', 'dare', 'daresam7@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'sam1581226448', NULL, NULL, NULL, NULL, NULL, NULL, '4bbjf1fzy26ow0ns', '0', 0, '', '2020-02-09 05:34:08', '', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `fin_usersettings`
--

CREATE TABLE `fin_usersettings` (
  `usid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `bid` int(11) NOT NULL,
  `companylogo` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `display` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL,
  `producttitle` varchar(150) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Invoice and billing titles',
  `paymentterms` int(11) NOT NULL COMMENT 'payment due dates',
  `invoice_title` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `invoice_subtitle` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `invoice_footer` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `invoice_memo` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `estimation_title` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `estimation_subtitle` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `estimation_footer` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `estimation_memo` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `createddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modifydate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fin_usersettings`
--

INSERT INTO `fin_usersettings` (`usid`, `uid`, `bid`, `companylogo`, `display`, `producttitle`, `paymentterms`, `invoice_title`, `invoice_subtitle`, `invoice_footer`, `invoice_memo`, `estimation_title`, `estimation_subtitle`, `estimation_footer`, `estimation_memo`, `createddate`, `modifydate`) VALUES
(1, 9, 3, '524d0b21-e824-49ad-82fc-06fcc0a80228.png', 'Yes', 'Product', 0, 'Invoice', 'Sub heading', 'Footer', 'Memo', 'Estimate', 'estimate subheading', 'estimate footer', 'Estimate memo', '2013-11-15 12:16:26', NULL),
(2, 8, 4, '52567f82-8488-40c7-9dad-0a80c0a80228.png', 'Yes', 'Product/Service', 15, 'Invoice Title', 'Sub heading for invoice', 'Footer ivoice', 'Memo Invoice', 'Estimate', 'estimate subheading', 'estimate footer', 'Memo', '2013-11-18 07:24:35', NULL),
(3, 10, 5, '', 'Yes', 'Product', 0, 'Invoice', '', '', '', 'Estimate', '', '', '', '2013-10-03 05:08:26', NULL),
(4, 11, 6, '', 'Yes', 'Product', 0, 'Invoice', '', '', '', 'Estimate', '', '', '', '2014-01-13 11:37:48', NULL),
(5, 12, 7, '531571a0-b554-4a77-bab4-4628c0bad285.png', 'Yes', 'Product', 15, 'Invoice', 'Bangalore Karnataka india', 'Bangalore Karnataka india', 'Bangalore Karnataka india\r\n', 'Estimate', 'Bangalore Karnataka india', 'Bangalore Karnataka india', 'Bangalore Karnataka india\r\n', '2014-03-04 06:24:41', NULL),
(6, 13, 8, '5ba1291b-e878-4772-8a25-294e42ba1305.jpg', 'No', 'Product/Service', 15, 'Invoice', 'Sample Invoice', 'Sample Invoice', 'Sample Invoice', 'Estimate', 'Sample estimate', 'Sample estimate', 'Sample estimate', '2019-07-21 09:59:04', NULL),
(7, 15, 9, '', 'Yes', 'Product', 0, 'Invoice', '', '', '', 'Estimate', '', '', '', '2014-03-06 10:59:49', NULL),
(8, 16, 10, '531865b2-bc8c-45e0-9483-43a4c0bad285.png', 'Yes', 'Product', 0, 'Invoice', 'Thank you ', 'Thank you for your business', 'If you have question about this invoice , Please contact E-mail', 'Estimate', 'Thank you ', 'Thank you for your business', 'If you have question about this invoice , Please contact E-mail', '2014-03-06 12:10:26', NULL),
(9, 17, 11, '', 'Yes', 'Product', 0, 'Invoice', '', '', '', 'Estimate', '', '', '', '2014-03-24 06:49:59', NULL),
(10, 19, 12, '532ffe1b-ca68-4f26-b4f2-4409c0bad285.png', 'Yes', 'Product/Service', 0, 'Invoice', 'Thank you for your business', 'Thank you for your business', 'Thank you for your business. We do expect payment within 21 days, so please process this invoice within that time. There will be a 1.5% interest charg', 'Estimate', 'Thank you for your business', 'Thank you for your business', 'Thank you for your business', '2014-03-24 09:43:14', NULL),
(11, 20, 13, '555ee8d0-52b0-4b8d-beec-32a3c0bad285.png', 'Yes', 'Service', 0, 'Invoice', '', '', '', 'Estimate', '', '', '', '2015-05-22 08:29:04', NULL),
(12, 21, 14, '', 'Yes', 'Product', 0, 'Invoice', '', '', '', 'Estimate', '', '', '', '2015-06-28 09:17:16', NULL),
(13, 22, 15, '', 'Yes', 'Product', 0, 'Invoice', '', '', '', 'Estimate', '', '', '', '2015-06-30 05:13:30', NULL),
(14, 23, 16, '', 'Yes', 'Product', 0, 'Invoice', '', '', '', 'Estimate', '', '', '', '2015-07-04 03:54:42', NULL),
(15, 24, 17, '', 'Yes', 'Product', 0, 'Invoice', '', '', '', 'Estimate', '', '', '', '2016-08-20 06:38:41', NULL),
(16, 26, 18, '', 'Yes', 'Product/Service', 15, 'Invoice', '', '', '', 'Quotation', '', '', '', '2019-06-04 13:52:31', NULL),
(17, 27, 19, '5d0006e2-3c60-46e4-89ea-0e1742ba1305.png', 'Yes', 'Product/Service', 0, 'Invoice', 'Tax Invoice', 'Thinkmybiz Limited', 'Welcome and Keep supporting', 'Estimate', '', '', '', '2019-06-11 19:55:10', NULL),
(18, 28, 20, '', 'Yes', 'Product', 0, 'Invoice', '', '', '', 'Estimate', '', '', '', '2019-12-27 06:31:14', NULL),
(19, 29, 21, '', 'Yes', 'Product', 0, 'Invoice', '', '', '', 'Estimate', '', '', '', '2020-01-06 16:21:51', NULL),
(20, 31, 22, '', 'Yes', 'Product', 0, 'Invoice', '', '', '', 'Estimate', '', '', '', '2020-02-09 05:35:20', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fin_accountdefaults`
--
ALTER TABLE `fin_accountdefaults`
  ADD PRIMARY KEY (`adid`),
  ADD KEY `agid` (`agid`),
  ADD KEY `aiid` (`aiid`);

--
-- Indexes for table `fin_accountgroupcontents`
--
ALTER TABLE `fin_accountgroupcontents`
  ADD PRIMARY KEY (`acid`),
  ADD KEY `agid` (`agid`);

--
-- Indexes for table `fin_accountgroups`
--
ALTER TABLE `fin_accountgroups`
  ADD PRIMARY KEY (`agid`);

--
-- Indexes for table `fin_adminusers`
--
ALTER TABLE `fin_adminusers`
  ADD PRIMARY KEY (`aid`);

--
-- Indexes for table `fin_agcategories`
--
ALTER TABLE `fin_agcategories`
  ADD PRIMARY KEY (`agcid`),
  ADD KEY `agid` (`agid`);

--
-- Indexes for table `fin_agcategorycontents`
--
ALTER TABLE `fin_agcategorycontents`
  ADD PRIMARY KEY (`agccid`),
  ADD KEY `agcid` (`agcid`);

--
-- Indexes for table `fin_agitemcontents`
--
ALTER TABLE `fin_agitemcontents`
  ADD PRIMARY KEY (`agicid`),
  ADD KEY `agiid` (`agiid`);

--
-- Indexes for table `fin_agitems`
--
ALTER TABLE `fin_agitems`
  ADD PRIMARY KEY (`agiid`),
  ADD KEY `agid` (`agid`),
  ADD KEY `agcid` (`agcid`),
  ADD KEY `agsid` (`agsid`);

--
-- Indexes for table `fin_agsubcategories`
--
ALTER TABLE `fin_agsubcategories`
  ADD PRIMARY KEY (`agsid`),
  ADD KEY `agid` (`agid`),
  ADD KEY `agcid` (`agcid`);

--
-- Indexes for table `fin_agsubcategorycontents`
--
ALTER TABLE `fin_agsubcategorycontents`
  ADD PRIMARY KEY (`agscid`),
  ADD KEY `agsid` (`agsid`);

--
-- Indexes for table `fin_banks`
--
ALTER TABLE `fin_banks`
  ADD PRIMARY KEY (`bankid`),
  ADD KEY `uid` (`uid`),
  ADD KEY `bid` (`bid`),
  ADD KEY `agcid` (`agcid`);

--
-- Indexes for table `fin_banktransfers`
--
ALTER TABLE `fin_banktransfers`
  ADD PRIMARY KEY (`btid`),
  ADD KEY `uid` (`uid`),
  ADD KEY `bid` (`bid`),
  ADD KEY `tobankid` (`tobankid`),
  ADD KEY `frombankid` (`frombankid`);

--
-- Indexes for table `fin_billproducts`
--
ALTER TABLE `fin_billproducts`
  ADD PRIMARY KEY (`bpid`),
  ADD KEY `billid` (`billid`),
  ADD KEY `pid` (`pid`),
  ADD KEY `exp_cat` (`exp_cat`);

--
-- Indexes for table `fin_bills`
--
ALTER TABLE `fin_bills`
  ADD PRIMARY KEY (`billid`),
  ADD KEY `uid` (`uid`),
  ADD KEY `bid` (`bid`),
  ADD KEY `currency` (`currency`),
  ADD KEY `cid` (`cid`),
  ADD KEY `primary_currency` (`primary_currency`);

--
-- Indexes for table `fin_billtaxes`
--
ALTER TABLE `fin_billtaxes`
  ADD PRIMARY KEY (`btid`),
  ADD KEY `billid` (`billid`),
  ADD KEY `bpid` (`bpid`),
  ADD KEY `taxid` (`taxid`),
  ADD KEY `currency` (`currency`),
  ADD KEY `primarycurrency` (`primarycurrency`);

--
-- Indexes for table `fin_blogcates`
--
ALTER TABLE `fin_blogcates`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `fin_blogs`
--
ALTER TABLE `fin_blogs`
  ADD PRIMARY KEY (`bid`);

--
-- Indexes for table `fin_businesses`
--
ALTER TABLE `fin_businesses`
  ADD PRIMARY KEY (`bid`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `fin_businesstypecontents`
--
ALTER TABLE `fin_businesstypecontents`
  ADD PRIMARY KEY (`btcid`),
  ADD KEY `btid` (`btid`);

--
-- Indexes for table `fin_businesstypes`
--
ALTER TABLE `fin_businesstypes`
  ADD PRIMARY KEY (`btid`);

--
-- Indexes for table `fin_checks`
--
ALTER TABLE `fin_checks`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `fin_collaberaters`
--
ALTER TABLE `fin_collaberaters`
  ADD PRIMARY KEY (`cid`),
  ADD KEY `uid` (`uid`),
  ADD KEY `bid` (`bid`);

--
-- Indexes for table `fin_countries`
--
ALTER TABLE `fin_countries`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `fin_currencies`
--
ALTER TABLE `fin_currencies`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `fin_customers`
--
ALTER TABLE `fin_customers`
  ADD PRIMARY KEY (`cid`),
  ADD KEY `uid` (`uid`),
  ADD KEY `bid` (`bid`);

--
-- Indexes for table `fin_dformates`
--
ALTER TABLE `fin_dformates`
  ADD PRIMARY KEY (`did`);

--
-- Indexes for table `fin_emailcontents`
--
ALTER TABLE `fin_emailcontents`
  ADD PRIMARY KEY (`ecid`);

--
-- Indexes for table `fin_emaillists`
--
ALTER TABLE `fin_emaillists`
  ADD PRIMARY KEY (`eid`);

--
-- Indexes for table `fin_enquiries`
--
ALTER TABLE `fin_enquiries`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `fin_estimateproducts`
--
ALTER TABLE `fin_estimateproducts`
  ADD PRIMARY KEY (`epid`),
  ADD KEY `esid` (`esid`),
  ADD KEY `pid` (`pid`);

--
-- Indexes for table `fin_estimates`
--
ALTER TABLE `fin_estimates`
  ADD PRIMARY KEY (`eid`),
  ADD KEY `uid` (`uid`),
  ADD KEY `bid` (`bid`),
  ADD KEY `cid` (`cid`),
  ADD KEY `currency` (`currency`),
  ADD KEY `primary_currency` (`primary_currency`);

--
-- Indexes for table `fin_estimatetaxes`
--
ALTER TABLE `fin_estimatetaxes`
  ADD PRIMARY KEY (`estid`),
  ADD KEY `esid` (`esid`),
  ADD KEY `espid` (`espid`),
  ADD KEY `taxid` (`taxid`),
  ADD KEY `currency` (`currency`),
  ADD KEY `primarycurrency` (`primarycurrency`);

--
-- Indexes for table `fin_featurecontents`
--
ALTER TABLE `fin_featurecontents`
  ADD PRIMARY KEY (`fcid`),
  ADD KEY `fid` (`fid`);

--
-- Indexes for table `fin_features`
--
ALTER TABLE `fin_features`
  ADD PRIMARY KEY (`fid`);

--
-- Indexes for table `fin_invoiceproducts`
--
ALTER TABLE `fin_invoiceproducts`
  ADD PRIMARY KEY (`ipid`),
  ADD KEY `ivid` (`ivid`),
  ADD KEY `pid` (`pid`);

--
-- Indexes for table `fin_invoices`
--
ALTER TABLE `fin_invoices`
  ADD PRIMARY KEY (`ivid`),
  ADD KEY `uid` (`uid`),
  ADD KEY `bid` (`bid`),
  ADD KEY `cid` (`cid`),
  ADD KEY `currency` (`currency`),
  ADD KEY `primary_currency` (`primary_currency`);

--
-- Indexes for table `fin_invoicetaxes`
--
ALTER TABLE `fin_invoicetaxes`
  ADD PRIMARY KEY (`itid`),
  ADD KEY `taxid` (`taxid`),
  ADD KEY `ivid` (`ivid`),
  ADD KEY `ipid` (`ipid`),
  ADD KEY `primarycurrency` (`primarycurrency`),
  ADD KEY `currency` (`currency`);

--
-- Indexes for table `fin_journalentries`
--
ALTER TABLE `fin_journalentries`
  ADD PRIMARY KEY (`jeid`),
  ADD KEY `uid` (`uid`),
  ADD KEY `bid` (`bid`),
  ADD KEY `jid` (`jid`),
  ADD KEY `udid` (`udid`),
  ADD KEY `agid` (`agid`);

--
-- Indexes for table `fin_journals`
--
ALTER TABLE `fin_journals`
  ADD PRIMARY KEY (`jid`),
  ADD KEY `uid` (`uid`),
  ADD KEY `bid` (`bid`);

--
-- Indexes for table `fin_languages`
--
ALTER TABLE `fin_languages`
  ADD PRIMARY KEY (`lid`);

--
-- Indexes for table `fin_products`
--
ALTER TABLE `fin_products`
  ADD PRIMARY KEY (`pid`),
  ADD KEY `bid` (`bid`),
  ADD KEY `uid` (`uid`),
  ADD KEY `sell_cat` (`sell_cat`),
  ADD KEY `buy_cat` (`buy_cat`);

--
-- Indexes for table `fin_slidercontents`
--
ALTER TABLE `fin_slidercontents`
  ADD PRIMARY KEY (`scid`),
  ADD KEY `sid` (`sid`);

--
-- Indexes for table `fin_sliders`
--
ALTER TABLE `fin_sliders`
  ADD PRIMARY KEY (`sid`);

--
-- Indexes for table `fin_staffs`
--
ALTER TABLE `fin_staffs`
  ADD PRIMARY KEY (`sid`),
  ADD KEY `uid` (`uid`),
  ADD KEY `bid` (`bid`);

--
-- Indexes for table `fin_staticpagecontents`
--
ALTER TABLE `fin_staticpagecontents`
  ADD PRIMARY KEY (`scid`);

--
-- Indexes for table `fin_staticpages`
--
ALTER TABLE `fin_staticpages`
  ADD PRIMARY KEY (`sid`);

--
-- Indexes for table `fin_taxes`
--
ALTER TABLE `fin_taxes`
  ADD PRIMARY KEY (`tid`),
  ADD KEY `bid` (`bid`),
  ADD KEY `uid_2` (`uid`);

--
-- Indexes for table `fin_testimonialcontents`
--
ALTER TABLE `fin_testimonialcontents`
  ADD PRIMARY KEY (`tcid`),
  ADD KEY `tid` (`tid`);

--
-- Indexes for table `fin_testimonials`
--
ALTER TABLE `fin_testimonials`
  ADD PRIMARY KEY (`tid`);

--
-- Indexes for table `fin_timetracks`
--
ALTER TABLE `fin_timetracks`
  ADD PRIMARY KEY (`trid`);

--
-- Indexes for table `fin_transactions`
--
ALTER TABLE `fin_transactions`
  ADD PRIMARY KEY (`tid`),
  ADD KEY `uid` (`uid`),
  ADD KEY `bid` (`bid`),
  ADD KEY `category` (`category`),
  ADD KEY `account` (`account`),
  ADD KEY `currency` (`currency`),
  ADD KEY `primarycurrency` (`primarycurrency`),
  ADD KEY `customer` (`customer`),
  ADD KEY `vendor` (`vendor`),
  ADD KEY `invoice` (`invoice`),
  ADD KEY `bill` (`bill`),
  ADD KEY `bankid` (`bankid`);

--
-- Indexes for table `fin_transactiontaxes`
--
ALTER TABLE `fin_transactiontaxes`
  ADD PRIMARY KEY (`taid`),
  ADD KEY `taxid` (`taxid`),
  ADD KEY `currency` (`currency`),
  ADD KEY `primarycurrency` (`primarycurrency`),
  ADD KEY `taxid_2` (`taxid`),
  ADD KEY `tid` (`tid`);

--
-- Indexes for table `fin_userdefaults`
--
ALTER TABLE `fin_userdefaults`
  ADD PRIMARY KEY (`udid`),
  ADD KEY `uid` (`uid`),
  ADD KEY `bid` (`bid`),
  ADD KEY `agid` (`agid`),
  ADD KEY `aiid` (`aiid`),
  ADD KEY `systemaccount` (`systemaccount`),
  ADD KEY `taxid` (`taxid`),
  ADD KEY `bankid` (`bankid`);

--
-- Indexes for table `fin_users`
--
ALTER TABLE `fin_users`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `fin_usersettings`
--
ALTER TABLE `fin_usersettings`
  ADD PRIMARY KEY (`usid`),
  ADD KEY `uid` (`uid`),
  ADD KEY `bid` (`bid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fin_accountdefaults`
--
ALTER TABLE `fin_accountdefaults`
  MODIFY `adid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `fin_accountgroupcontents`
--
ALTER TABLE `fin_accountgroupcontents`
  MODIFY `acid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `fin_accountgroups`
--
ALTER TABLE `fin_accountgroups`
  MODIFY `agid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `fin_adminusers`
--
ALTER TABLE `fin_adminusers`
  MODIFY `aid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fin_agcategories`
--
ALTER TABLE `fin_agcategories`
  MODIFY `agcid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `fin_agcategorycontents`
--
ALTER TABLE `fin_agcategorycontents`
  MODIFY `agccid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `fin_agitemcontents`
--
ALTER TABLE `fin_agitemcontents`
  MODIFY `agicid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=369;

--
-- AUTO_INCREMENT for table `fin_agitems`
--
ALTER TABLE `fin_agitems`
  MODIFY `agiid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=368;

--
-- AUTO_INCREMENT for table `fin_agsubcategories`
--
ALTER TABLE `fin_agsubcategories`
  MODIFY `agsid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `fin_agsubcategorycontents`
--
ALTER TABLE `fin_agsubcategorycontents`
  MODIFY `agscid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `fin_banks`
--
ALTER TABLE `fin_banks`
  MODIFY `bankid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `fin_banktransfers`
--
ALTER TABLE `fin_banktransfers`
  MODIFY `btid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fin_billproducts`
--
ALTER TABLE `fin_billproducts`
  MODIFY `bpid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=779;

--
-- AUTO_INCREMENT for table `fin_bills`
--
ALTER TABLE `fin_bills`
  MODIFY `billid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `fin_billtaxes`
--
ALTER TABLE `fin_billtaxes`
  MODIFY `btid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=259;

--
-- AUTO_INCREMENT for table `fin_blogcates`
--
ALTER TABLE `fin_blogcates`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fin_blogs`
--
ALTER TABLE `fin_blogs`
  MODIFY `bid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fin_businesses`
--
ALTER TABLE `fin_businesses`
  MODIFY `bid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `fin_businesstypecontents`
--
ALTER TABLE `fin_businesstypecontents`
  MODIFY `btcid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `fin_businesstypes`
--
ALTER TABLE `fin_businesstypes`
  MODIFY `btid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `fin_checks`
--
ALTER TABLE `fin_checks`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fin_collaberaters`
--
ALTER TABLE `fin_collaberaters`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fin_countries`
--
ALTER TABLE `fin_countries`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=229;

--
-- AUTO_INCREMENT for table `fin_currencies`
--
ALTER TABLE `fin_currencies`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `fin_customers`
--
ALTER TABLE `fin_customers`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;

--
-- AUTO_INCREMENT for table `fin_dformates`
--
ALTER TABLE `fin_dformates`
  MODIFY `did` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `fin_emailcontents`
--
ALTER TABLE `fin_emailcontents`
  MODIFY `ecid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `fin_emaillists`
--
ALTER TABLE `fin_emaillists`
  MODIFY `eid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fin_enquiries`
--
ALTER TABLE `fin_enquiries`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fin_estimateproducts`
--
ALTER TABLE `fin_estimateproducts`
  MODIFY `epid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `fin_estimates`
--
ALTER TABLE `fin_estimates`
  MODIFY `eid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `fin_estimatetaxes`
--
ALTER TABLE `fin_estimatetaxes`
  MODIFY `estid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `fin_featurecontents`
--
ALTER TABLE `fin_featurecontents`
  MODIFY `fcid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `fin_features`
--
ALTER TABLE `fin_features`
  MODIFY `fid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `fin_invoiceproducts`
--
ALTER TABLE `fin_invoiceproducts`
  MODIFY `ipid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=497;

--
-- AUTO_INCREMENT for table `fin_invoices`
--
ALTER TABLE `fin_invoices`
  MODIFY `ivid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `fin_invoicetaxes`
--
ALTER TABLE `fin_invoicetaxes`
  MODIFY `itid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=202;

--
-- AUTO_INCREMENT for table `fin_journalentries`
--
ALTER TABLE `fin_journalentries`
  MODIFY `jeid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=641;

--
-- AUTO_INCREMENT for table `fin_journals`
--
ALTER TABLE `fin_journals`
  MODIFY `jid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `fin_languages`
--
ALTER TABLE `fin_languages`
  MODIFY `lid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fin_products`
--
ALTER TABLE `fin_products`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT for table `fin_slidercontents`
--
ALTER TABLE `fin_slidercontents`
  MODIFY `scid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `fin_sliders`
--
ALTER TABLE `fin_sliders`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `fin_staffs`
--
ALTER TABLE `fin_staffs`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fin_staticpagecontents`
--
ALTER TABLE `fin_staticpagecontents`
  MODIFY `scid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `fin_staticpages`
--
ALTER TABLE `fin_staticpages`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `fin_taxes`
--
ALTER TABLE `fin_taxes`
  MODIFY `tid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `fin_testimonialcontents`
--
ALTER TABLE `fin_testimonialcontents`
  MODIFY `tcid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `fin_testimonials`
--
ALTER TABLE `fin_testimonials`
  MODIFY `tid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `fin_timetracks`
--
ALTER TABLE `fin_timetracks`
  MODIFY `trid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fin_transactions`
--
ALTER TABLE `fin_transactions`
  MODIFY `tid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=985;

--
-- AUTO_INCREMENT for table `fin_transactiontaxes`
--
ALTER TABLE `fin_transactiontaxes`
  MODIFY `taid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `fin_userdefaults`
--
ALTER TABLE `fin_userdefaults`
  MODIFY `udid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=405;

--
-- AUTO_INCREMENT for table `fin_users`
--
ALTER TABLE `fin_users`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `fin_usersettings`
--
ALTER TABLE `fin_usersettings`
  MODIFY `usid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `fin_accountdefaults`
--
ALTER TABLE `fin_accountdefaults`
  ADD CONSTRAINT `fin_accountdefaults_ibfk_1` FOREIGN KEY (`agid`) REFERENCES `fin_accountgroups` (`agid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_accountdefaults_ibfk_2` FOREIGN KEY (`aiid`) REFERENCES `fin_agitems` (`agiid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_accountgroupcontents`
--
ALTER TABLE `fin_accountgroupcontents`
  ADD CONSTRAINT `fin_accountgroupcontents_ibfk_1` FOREIGN KEY (`agid`) REFERENCES `fin_accountgroups` (`agid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_agcategories`
--
ALTER TABLE `fin_agcategories`
  ADD CONSTRAINT `fin_agcategories_ibfk_1` FOREIGN KEY (`agid`) REFERENCES `fin_accountgroups` (`agid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_agcategorycontents`
--
ALTER TABLE `fin_agcategorycontents`
  ADD CONSTRAINT `fin_agcategorycontents_ibfk_1` FOREIGN KEY (`agcid`) REFERENCES `fin_agcategories` (`agcid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_agitemcontents`
--
ALTER TABLE `fin_agitemcontents`
  ADD CONSTRAINT `fin_agitemcontents_ibfk_1` FOREIGN KEY (`agiid`) REFERENCES `fin_agitems` (`agiid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_agitems`
--
ALTER TABLE `fin_agitems`
  ADD CONSTRAINT `fin_agitems_ibfk_1` FOREIGN KEY (`agid`) REFERENCES `fin_accountgroups` (`agid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_agitems_ibfk_2` FOREIGN KEY (`agcid`) REFERENCES `fin_agcategories` (`agcid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_agitems_ibfk_3` FOREIGN KEY (`agsid`) REFERENCES `fin_agsubcategories` (`agsid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_agsubcategories`
--
ALTER TABLE `fin_agsubcategories`
  ADD CONSTRAINT `fin_agsubcategories_ibfk_1` FOREIGN KEY (`agid`) REFERENCES `fin_accountgroups` (`agid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_agsubcategories_ibfk_2` FOREIGN KEY (`agcid`) REFERENCES `fin_agcategories` (`agcid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_agsubcategorycontents`
--
ALTER TABLE `fin_agsubcategorycontents`
  ADD CONSTRAINT `fin_agsubcategorycontents_ibfk_1` FOREIGN KEY (`agsid`) REFERENCES `fin_agsubcategories` (`agsid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_banks`
--
ALTER TABLE `fin_banks`
  ADD CONSTRAINT `fin_banks_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `fin_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_banks_ibfk_2` FOREIGN KEY (`bid`) REFERENCES `fin_businesses` (`bid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_banks_ibfk_3` FOREIGN KEY (`agcid`) REFERENCES `fin_agcategories` (`agid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_banktransfers`
--
ALTER TABLE `fin_banktransfers`
  ADD CONSTRAINT `fin_banktransfers_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `fin_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_banktransfers_ibfk_2` FOREIGN KEY (`bid`) REFERENCES `fin_businesses` (`bid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_banktransfers_ibfk_3` FOREIGN KEY (`frombankid`) REFERENCES `fin_banks` (`bankid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_banktransfers_ibfk_4` FOREIGN KEY (`tobankid`) REFERENCES `fin_banks` (`bankid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_billproducts`
--
ALTER TABLE `fin_billproducts`
  ADD CONSTRAINT `fin_billproducts_ibfk_1` FOREIGN KEY (`billid`) REFERENCES `fin_bills` (`billid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_billproducts_ibfk_2` FOREIGN KEY (`pid`) REFERENCES `fin_products` (`pid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_billproducts_ibfk_3` FOREIGN KEY (`exp_cat`) REFERENCES `fin_userdefaults` (`udid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_bills`
--
ALTER TABLE `fin_bills`
  ADD CONSTRAINT `fin_bills_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `fin_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_bills_ibfk_2` FOREIGN KEY (`bid`) REFERENCES `fin_businesses` (`bid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_bills_ibfk_3` FOREIGN KEY (`currency`) REFERENCES `fin_currencies` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_bills_ibfk_4` FOREIGN KEY (`cid`) REFERENCES `fin_customers` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_bills_ibfk_5` FOREIGN KEY (`primary_currency`) REFERENCES `fin_currencies` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_billtaxes`
--
ALTER TABLE `fin_billtaxes`
  ADD CONSTRAINT `fin_billtaxes_ibfk_1` FOREIGN KEY (`billid`) REFERENCES `fin_bills` (`billid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_billtaxes_ibfk_2` FOREIGN KEY (`bpid`) REFERENCES `fin_billproducts` (`bpid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_billtaxes_ibfk_3` FOREIGN KEY (`taxid`) REFERENCES `fin_taxes` (`tid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_businesses`
--
ALTER TABLE `fin_businesses`
  ADD CONSTRAINT `fin_businesses_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `fin_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_businesstypecontents`
--
ALTER TABLE `fin_businesstypecontents`
  ADD CONSTRAINT `fin_businesstypecontents_ibfk_1` FOREIGN KEY (`btid`) REFERENCES `fin_businesstypes` (`btid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_collaberaters`
--
ALTER TABLE `fin_collaberaters`
  ADD CONSTRAINT `fin_collaberaters_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `fin_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_collaberaters_ibfk_2` FOREIGN KEY (`bid`) REFERENCES `fin_businesses` (`bid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_customers`
--
ALTER TABLE `fin_customers`
  ADD CONSTRAINT `fin_customers_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `fin_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_customers_ibfk_2` FOREIGN KEY (`bid`) REFERENCES `fin_businesses` (`bid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_estimateproducts`
--
ALTER TABLE `fin_estimateproducts`
  ADD CONSTRAINT `fin_estimateproducts_ibfk_1` FOREIGN KEY (`esid`) REFERENCES `fin_estimates` (`eid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_estimateproducts_ibfk_2` FOREIGN KEY (`pid`) REFERENCES `fin_products` (`pid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_estimates`
--
ALTER TABLE `fin_estimates`
  ADD CONSTRAINT `fin_estimates_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `fin_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_estimates_ibfk_2` FOREIGN KEY (`bid`) REFERENCES `fin_businesses` (`bid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_estimates_ibfk_3` FOREIGN KEY (`cid`) REFERENCES `fin_customers` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_estimates_ibfk_4` FOREIGN KEY (`currency`) REFERENCES `fin_currencies` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_estimates_ibfk_5` FOREIGN KEY (`primary_currency`) REFERENCES `fin_currencies` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_estimatetaxes`
--
ALTER TABLE `fin_estimatetaxes`
  ADD CONSTRAINT `fin_estimatetaxes_ibfk_1` FOREIGN KEY (`esid`) REFERENCES `fin_estimates` (`eid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_estimatetaxes_ibfk_2` FOREIGN KEY (`espid`) REFERENCES `fin_estimateproducts` (`epid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_estimatetaxes_ibfk_3` FOREIGN KEY (`taxid`) REFERENCES `fin_taxes` (`tid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_estimatetaxes_ibfk_4` FOREIGN KEY (`currency`) REFERENCES `fin_currencies` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_estimatetaxes_ibfk_5` FOREIGN KEY (`primarycurrency`) REFERENCES `fin_currencies` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_featurecontents`
--
ALTER TABLE `fin_featurecontents`
  ADD CONSTRAINT `fin_featurecontents_ibfk_1` FOREIGN KEY (`fid`) REFERENCES `fin_features` (`fid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_invoiceproducts`
--
ALTER TABLE `fin_invoiceproducts`
  ADD CONSTRAINT `fin_invoiceproducts_ibfk_1` FOREIGN KEY (`ivid`) REFERENCES `fin_invoices` (`ivid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_invoiceproducts_ibfk_2` FOREIGN KEY (`pid`) REFERENCES `fin_products` (`pid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_invoices`
--
ALTER TABLE `fin_invoices`
  ADD CONSTRAINT `fin_invoices_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `fin_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_invoices_ibfk_2` FOREIGN KEY (`bid`) REFERENCES `fin_businesses` (`bid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_invoices_ibfk_3` FOREIGN KEY (`cid`) REFERENCES `fin_customers` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_invoices_ibfk_4` FOREIGN KEY (`currency`) REFERENCES `fin_currencies` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_invoices_ibfk_5` FOREIGN KEY (`primary_currency`) REFERENCES `fin_currencies` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_invoicetaxes`
--
ALTER TABLE `fin_invoicetaxes`
  ADD CONSTRAINT `fin_invoicetaxes_ibfk_1` FOREIGN KEY (`ivid`) REFERENCES `fin_invoices` (`ivid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_invoicetaxes_ibfk_2` FOREIGN KEY (`taxid`) REFERENCES `fin_taxes` (`tid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_invoicetaxes_ibfk_3` FOREIGN KEY (`ipid`) REFERENCES `fin_invoiceproducts` (`ipid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_journalentries`
--
ALTER TABLE `fin_journalentries`
  ADD CONSTRAINT `fin_journalentries_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `fin_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_journalentries_ibfk_2` FOREIGN KEY (`bid`) REFERENCES `fin_businesses` (`bid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_journalentries_ibfk_3` FOREIGN KEY (`jid`) REFERENCES `fin_journals` (`jid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_journalentries_ibfk_4` FOREIGN KEY (`udid`) REFERENCES `fin_userdefaults` (`udid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_journalentries_ibfk_5` FOREIGN KEY (`agid`) REFERENCES `fin_accountgroups` (`agid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_journals`
--
ALTER TABLE `fin_journals`
  ADD CONSTRAINT `fin_journals_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `fin_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_journals_ibfk_2` FOREIGN KEY (`bid`) REFERENCES `fin_businesses` (`bid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_products`
--
ALTER TABLE `fin_products`
  ADD CONSTRAINT `fin_products_ibfk_1` FOREIGN KEY (`bid`) REFERENCES `fin_businesses` (`bid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_products_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `fin_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_slidercontents`
--
ALTER TABLE `fin_slidercontents`
  ADD CONSTRAINT `fin_slidercontents_ibfk_1` FOREIGN KEY (`sid`) REFERENCES `fin_sliders` (`sid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_staffs`
--
ALTER TABLE `fin_staffs`
  ADD CONSTRAINT `fin_staffs_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `fin_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_staffs_ibfk_2` FOREIGN KEY (`bid`) REFERENCES `fin_businesses` (`bid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_taxes`
--
ALTER TABLE `fin_taxes`
  ADD CONSTRAINT `fin_taxes_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `fin_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_taxes_ibfk_2` FOREIGN KEY (`bid`) REFERENCES `fin_businesses` (`bid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_testimonialcontents`
--
ALTER TABLE `fin_testimonialcontents`
  ADD CONSTRAINT `fin_testimonialcontents_ibfk_1` FOREIGN KEY (`tid`) REFERENCES `fin_testimonials` (`tid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_transactions`
--
ALTER TABLE `fin_transactions`
  ADD CONSTRAINT `fin_transactions_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `fin_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_transactions_ibfk_10` FOREIGN KEY (`bill`) REFERENCES `fin_bills` (`billid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_transactions_ibfk_12` FOREIGN KEY (`bankid`) REFERENCES `fin_banks` (`bankid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_transactions_ibfk_2` FOREIGN KEY (`bid`) REFERENCES `fin_businesses` (`bid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_transactions_ibfk_3` FOREIGN KEY (`category`) REFERENCES `fin_userdefaults` (`udid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_transactions_ibfk_4` FOREIGN KEY (`account`) REFERENCES `fin_userdefaults` (`udid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_transactions_ibfk_5` FOREIGN KEY (`currency`) REFERENCES `fin_currencies` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_transactions_ibfk_6` FOREIGN KEY (`primarycurrency`) REFERENCES `fin_currencies` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_transactions_ibfk_7` FOREIGN KEY (`customer`) REFERENCES `fin_customers` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_transactions_ibfk_8` FOREIGN KEY (`vendor`) REFERENCES `fin_customers` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_transactions_ibfk_9` FOREIGN KEY (`invoice`) REFERENCES `fin_invoices` (`ivid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_transactiontaxes`
--
ALTER TABLE `fin_transactiontaxes`
  ADD CONSTRAINT `fin_transactiontaxes_ibfk_1` FOREIGN KEY (`tid`) REFERENCES `fin_transactions` (`tid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_transactiontaxes_ibfk_2` FOREIGN KEY (`taxid`) REFERENCES `fin_taxes` (`tid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_transactiontaxes_ibfk_3` FOREIGN KEY (`currency`) REFERENCES `fin_currencies` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_transactiontaxes_ibfk_4` FOREIGN KEY (`primarycurrency`) REFERENCES `fin_currencies` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_userdefaults`
--
ALTER TABLE `fin_userdefaults`
  ADD CONSTRAINT `fin_userdefaults_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `fin_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_userdefaults_ibfk_2` FOREIGN KEY (`bid`) REFERENCES `fin_businesses` (`bid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_userdefaults_ibfk_3` FOREIGN KEY (`agid`) REFERENCES `fin_accountgroups` (`agid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_userdefaults_ibfk_4` FOREIGN KEY (`aiid`) REFERENCES `fin_agitems` (`agiid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_userdefaults_ibfk_5` FOREIGN KEY (`taxid`) REFERENCES `fin_taxes` (`tid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_userdefaults_ibfk_6` FOREIGN KEY (`bankid`) REFERENCES `fin_banks` (`bankid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fin_usersettings`
--
ALTER TABLE `fin_usersettings`
  ADD CONSTRAINT `fin_usersettings_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `fin_users` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fin_usersettings_ibfk_2` FOREIGN KEY (`bid`) REFERENCES `fin_businesses` (`bid`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
